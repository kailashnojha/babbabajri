'use strict';
/* jshint node: true ,esversion:6*/
var loopback = require('loopback');
var boot = require('loopback-boot');

var app = module.exports = loopback();
var config = require("./config.json");
 
var session = require('express-session');
var cookieParser = require('cookie-parser');
// Passport configurators..
var loopbackPassport = require('loopback-component-passport');
var PassportConfigurator = loopbackPassport.PassportConfigurator;
var passportConfigurator = new PassportConfigurator(app);
var flash      = require('express-flash');
var path = require('path');

app.use(loopback.static(path.resolve(__dirname, '../client/adminbababajri/admin'))); 
// app.use(loopback.static(path.resolve(__dirname, '../client/multi-ln-babbabazri/bababazri-hindi-version')));



// attempt to build the providers/passport config
var configPassport = {};
try {
  configPassport = require('../providers.json');
} catch (err) {
  console.trace(err);
  process.exit(1); // fatal
}

if(process.argv[2] === "test"){
  config.redirectUrl =  "http://localhost:"+config.port;
}else{
  config.redirectUrl =  "http://139.59.71.150:"+config.port;
}


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// boot scripts mount components like REST API
boot(app, __dirname);

app.middleware('session:before', cookieParser(app.get('cookieSecret')));
app.middleware('session', session({
  secret: 'kitty',
  saveUninitialized: true,
  resave: true,
}));

passportConfigurator.init();

// We need flash messages to see passport errors
app.use(flash());

passportConfigurator.setupModels({
  userModel: app.models.People,
  userIdentityModel: app.models.UserIdentity,
  userCredentialModel: app.models.UserCredential,
});
for (var s in configPassport) {
  var c = configPassport[s];
  c.session = c.session !== false;
  passportConfigurator.configureProvider(s, c);
}


app.get('/robots.txt', function (req, res) {
    res.type('text/plain');
    res.send("User-agent: *\nDisallow: /");
});

app.get("/error",function(req,res,next){
  res.render("default-error.ejs");
})


app.all('/*', function(req, res, next) {
  if( !req.url.startsWith('/api') && !req.url.startsWith('/explorer') && !req.url.startsWith('/auth/facebook')){
      // res.sendFile('index.html', { root: path.resolve(__dirname, '../', 'client/babbabazri/last-build') });
      res.sendFile('index.html', { root: path.resolve(__dirname, '../', 'client/adminbababajri/admin') });
      // res.sendFile('index.html', { root: path.resolve(__dirname, '../', 'client/multi-ln-babbabazri/bababazri-hindi-version') });
  }
  else
    next();
});

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
      let a = process.argv[2] == "test"? "Local":"Production";
      console.log("Loopback running environment => ",a);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
/*boot(app, __dirname, function(err) {
  if (err) throw err;
*/
  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
// });
