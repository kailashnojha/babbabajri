'use strict';
/* jshint node: true ,esversion:6*/
module.exports = function(app) {
  	/*
   		
   		Min Ton formula = tyre * 1.5
   		Max Ton formula = tyre * 3.5
   		1 Ton = 1000kg
   		25 CUF = 1 Ton
   		1 Bag = 50 Kg
   		1 Bricks = 3 Kg

		// For 6 Tyres calculation demo
   		"unitTONmax": 6*3.5,
        "unitCUFmax": 6*3.5*25,
        "unitBRICKmax": (6*3.5*1000)/3,
        "unitBAGmax": (6*3.5*1000)/50

   	*/	

  let data = [
      {
        "name": {
        	en: "Truck",
        	hi: "ट्रक"
        },
        "image": "/api/Containers/VehicleTypeImg/download/truck.png",
        "clickImage":"/api/Containers/VehicleTypeImg/download/truck_click.png",
        "driveImage": "/api/Containers/VehicleTypeImg/download/truck-drive.png",
        "updatedAt": new Date(),
        "loadValue": [
            {
	            "tyres"           : 6,
	            "loading"         : 0,
	            "unloading"       : 0,
	            "loadKM"          : 25,
	            "unloadKM"        : 24,
	            "driverSalary"    : 325,
	            "driverDailyExp"  : 100,
	            "helper"          : 250,
	            "gst"             : 0,
	            "royalty"         : 0,
	            "tollTax"         : 0,
	            "driverSaving"    : 0,
	            "nightStay"       : 0,
	            "daily"           : 0,
	            "minCharge"       : 0,
	            "unitTONmin"      : 9,
	            "unitTONmax"      : 25,
	            "unitCUFmin"      : 225,
	            "unitCUFmax"      : 625,
	            "unitBRICKmin"    : 4500,
	            "unitBRICKmax"    : 9375,
	            "unitBAGmin"      : (9*1000)/50,
	            "unitBAGmax"      : (25*1000)/50
            },
            {
	            "tyres"           : 10,
	            "loading"         : 0,
	            "unloading"       : 0,
	            "loadKM"          : 25,
	            "unloadKM"        : 24,
	            "driverSalary"    : 325,
	            "driverDailyExp"  : 100,
	            "helper"          : 250,
	            "gst"             : 0,
	            "royalty"         : 0,
	            "tollTax"         : 0,
	            "driverSaving"    : 0,
	            "nightStay"       : 0,
	            "daily"           : 0,
	            "minCharge"       : 0,
	            "unitTONmin"      : 18,
	            "unitTONmax"      : 30,
	            "unitCUFmin"      : 450,
	            "unitCUFmax"      : 750,
	            "unitBRICKmin"    : 9000,
	            "unitBRICKmax"    : 11250,
	            "unitBAGmin"      : (18*1000)/50,
	            "unitBAGmax"      : (30*1000)/50
            },
            {
	            "tyres"           : 12,
	            "loading"         : 0,
	            "unloading"       : 0,
	            "loadKM"          : 25,
	            "unloadKM"        : 24,
	            "driverSalary"    : 325,
	            "driverDailyExp"  : 100,
	            "helper"          : 250,
	            "gst"             : 0,
	            "royalty"         : 0,
	            "tollTax"         : 0,
	            "driverSaving"    : 0,
	            "nightStay"       : 0,
	            "daily"           : 0,
	            "minCharge"       : 0,
	            "unitTONmin"      : 21,
	            "unitTONmax"      : 35,
	            "unitCUFmin"      : 525,
	            "unitCUFmax"      : 875,
	            "unitBRICKmin"    : 10500,
	            "unitBRICKmax"    : 13125,
	            "unitBAGmin"      : (21*1000)/50,
	            "unitBAGmax"      : (35*1000)/50
            },
            {
	            "tyres"           : 14,
	            "loading"         : 0,
	            "unloading"       : 0,
	            "loadKM"          : 26,
	            "unloadKM"        : 25,
	            "driverSalary"    : 400,
	            "driverDailyExp"  : 100,
	            "helper"          : 250,
	            "gst"             : 0,
	            "royalty"         : 0,
	            "tollTax"         : 0,
	            "driverSaving"    : 0,
	            "nightStay"       : 0,
	            "daily"           : 0,
	            "minCharge"       : 0,
	            "unitTONmin"      : 21,
	            "unitTONmax"      : 40,
	            "unitCUFmin"      : 525,
	            "unitCUFmax"      : 1000,
	            "unitBRICKmin"    : 10500,
	            "unitBRICKmax"    : 15000,
	            "unitBAGmin"      : (21*1000)/50,
	            "unitBAGmax"      : (40*1000)/50
            },
            {
	            "tyres"           : 16,
	            "loading"         : 0,
	            "unloading"       : 0,
	            "loadKM"          : 26,
	            "unloadKM"        : 25,
	            "driverSalary"    : 400,
	            "driverDailyExp"  : 100,
	            "helper"          : 250,
	            "gst"             : 0,
	            "royalty"         : 0,
	            "tollTax"         : 0,
	            "driverSaving"    : 0,
	            "nightStay"       : 0,
	            "daily"           : 0,
	            "minCharge"       : 0,
	            "unitTONmin"      : 21,
	            "unitTONmax"      : 40,
	            "unitCUFmin"      : 525,
	            "unitCUFmax"      : 1000,
	            "unitBRICKmin"    : 10500,
	            "unitBRICKmax"    : 15000,
	            "unitBAGmin"      : (21*1000)/50,
	            "unitBAGmax"      : (40*1000)/50
            },
            {
	            "tyres"           : 18,
	            "loading"         : 0,
	            "unloading"       : 0,
	            "loadKM"          : 26,
	            "unloadKM"        : 25,
	            "driverSalary"    : 400,
	            "driverDailyExp"  : 100,
	            "helper"          : 250,
	            "gst"             : 0,
	            "royalty"         : 0,
	            "tollTax"         : 0,
	            "driverSaving"    : 0,
	            "nightStay"       : 0,
	            "daily"           : 0,
	            "minCharge"       : 0,
	            "unitTONmin"      : 21,
	            "unitTONmax"      : 40,
	            "unitCUFmin"      : 525,
	            "unitCUFmax"      : 1000,
	            "unitBRICKmin"    : 10500,
	            "unitBRICKmax"    : 15000,
	            "unitBAGmin"      : (21*1000)/50,
	            "unitBAGmax"      : (40*1000)/50
            }

        ],
        "createdAt": new Date()
      },  	
      {
        "name": {
        	en: "Dumper",
        	hi: "डम्पर"
        },
        "image": "/api/Containers/VehicleTypeImg/download/dumper.png",
        "clickImage":"/api/Containers/VehicleTypeImg/download/dumper_click.png",
        "driveImage": "/api/Containers/VehicleTypeImg/download/dumper-drive.png",
        "updatedAt": new Date(),
        "loadValue": [
            {
	            "tyres"           : 6,
	            "loading"         : 0,
	            "unloading"       : 0,
	            "loadKM"          : 24.33,
	            "unloadKM"        : 22,
	            "driverSalary"    : 325,
	            "driverDailyExp"  : 100,
	            "helper"          : 0,
	            "gst"             : 0,
	            "royalty"         : 0,
	            "tollTax"         : 0,
	            "driverSaving"    : 0,
	            "nightStay"       : 0,
	            "daily"           : 0,
	            "minCharge"       : 0,
	            "unitTONmin"      : 9,
	            "unitTONmax"      : 18,
	            "unitCUFmin"      : 225,
	            "unitCUFmax"      : 450,
	            "unitBRICKmin"    : 4500,
	            "unitBRICKmax"    : 6750,
	            "unitBAGmin"      : (9*1000)/50,
	            "unitBAGmax"      : (18*1000)/50
            },
            {
	            "tyres"           : 10,
	            "loading"         : 0,
	            "unloading"       : 0,
	            "loadKM"          : 25,
	            "unloadKM"        : 23,
	            "driverSalary"    : 325,
	            "driverDailyExp"  : 100,
	            "helper"          : 0,
	            "gst"             : 0,
	            "royalty"         : 0,
	            "tollTax"         : 0,
	            "driverSaving"    : 0,
	            "nightStay"       : 0,
	            "daily"           : 0,
	            "minCharge"       : 0,
	            "unitTONmin"      : 18,
	            "unitTONmax"      : 30,
	            "unitCUFmin"      : 450,
	            "unitCUFmax"      : 750,
	            "unitBRICKmin"    : 9000,
	            "unitBRICKmax"    : 11250,
	            "unitBAGmin"      : (18*1000)/50,
	            "unitBAGmax"      : (30*1000)/50
            },
            {
	            "tyres"           : 12,
	            "loading"         : 0,
	            "unloading"       : 0,
	            "loadKM"          : 25,
	            "unloadKM"        : 23,
	            "driverSalary"    : 325,
	            "driverDailyExp"  : 100,
	            "helper"          : 0,
	            "gst"             : 0,
	            "royalty"         : 0,
	            "tollTax"         : 0,
	            "driverSaving"    : 0,
	            "nightStay"       : 0,
	            "daily"           : 0,
	            "minCharge"       : 0,
	            "unitTONmin"      : 21,
	            "unitTONmax"      : 40,
	            "unitCUFmin"      : 525,
	            "unitCUFmax"      : 1000,
	            "unitBRICKmin"    : 10500,
	            "unitBRICKmax"    : 15000,
	            "unitBAGmin"      : (21*1000)/50,
	            "unitBAGmax"      : (40*1000)/50
            },
            {
	            "tyres"           : 16,
	            "loading"         : 0,
	            "unloading"       : 0,
	            "loadKM"          : 25,
	            "unloadKM"        : 24,
	            "driverSalary"    : 325,
	            "driverDailyExp"  : 100,
	            "helper"          : 0,
	            "gst"             : 0,
	            "royalty"         : 0,
	            "tollTax"         : 0,
	            "driverSaving"    : 0,
	            "nightStay"       : 0,
	            "daily"           : 0,
	            "minCharge"       : 0,
	            "unitTONmin"      : 27,
	            "unitTONmax"      : 50,
	            "unitCUFmin"      : 675,
	            "unitCUFmax"      : 1250,
	            "unitBRICKmin"    : 13500,
	            "unitBRICKmax"    : 18750,
	            "unitBAGmin"      : (27*1000)/50,
	            "unitBAGmax"      : (50*1000)/50
            }                                                
        ],
        "createdAt": new Date()
      },  	
      {
        "name": {
        	en: "Trolla",
        	hi: "ट्रोला"
        },
        "image": "/api/Containers/VehicleTypeImg/download/trolla.png",
        "clickImage":"/api/Containers/VehicleTypeImg/download/trolla_click.png",
        "driveImage": "/api/Containers/VehicleTypeImg/download/trolla-drive.png",
        "updatedAt": new Date(),
        "loadValue": [
            {
	            "tyres"           : 16,
	            "loading"         : 0,
	            "unloading"       : 0,
	            "loadKM"          : 26,
	            "unloadKM"        : 24,
	            "driverSalary"    : 400,
	            "driverDailyExp"  : 100,
	            "helper"          : 250,
	            "gst"             : 0,
	            "royalty"         : 0,
	            "tollTax"         : 0,
	            "driverSaving"    : 0,
	            "nightStay"       : 0,
	            "daily"           : 0,
	            "minCharge"       : 0,
	            "unitTONmin"      : 21,
	            "unitTONmax"      : 40,
	            "unitCUFmin"      : 525,
	            "unitCUFmax"      : 1000,
	            "unitBRICKmin"    : 10500,
	            "unitBRICKmax"    : 15000,
	            "unitBAGmin"      : (21*1000)/50,
	            "unitBAGmax"      : (40*1000)/50
            },
            {
	            "tyres"           : 20,
	            "loading"         : 0,
	            "unloading"       : 0,
	            "loadKM"          : 26,
	            "unloadKM"        : 24,
	            "driverSalary"    : 400,
	            "driverDailyExp"  : 100,
	            "helper"          : 250,
	            "gst"             : 0,
	            "royalty"         : 0,
	            "tollTax"         : 0,
	            "driverSaving"    : 0,
	            "nightStay"       : 0,
	            "daily"           : 0,
	            "minCharge"       : 0,
	            "unitTONmin"      : 27,
	            "unitTONmax"      : 45,
	            "unitCUFmin"      : 675,
	            "unitCUFmax"      : 1125,
	            "unitBRICKmin"    : 13500,
	            "unitBRICKmax"    : 16875,
	            "unitBAGmin"      : (27*1000)/50,
	            "unitBAGmax"      : (45*1000)/50
            }            
        ],
        "createdAt": new Date()
      },  	
      {
        "name": {
        	en: "Pick Up",
        	hi: "पिक अप"
        },
        "image": "/api/Containers/VehicleTypeImg/download/pickup.png",
        "clickImage":"/api/Containers/VehicleTypeImg/download/pickup_click.png",
        "driveImage": "/api/Containers/VehicleTypeImg/download/pickup-drive.png",
        "updatedAt": new Date(),
        "loadValue": [
            {
	            "tyres"           : 4,
	            "loading"         : 0,
	            "unloading"       : 0,
	            "loadKM"          : 7.2,
	            "unloadKM"        : 6,
	            "driverSalary"    : 325,
	            "driverDailyExp"  : 100,
	            "helper"          : 0,
	            "gst"             : 0,
	            "royalty"         : 0,
	            "tollTax"         : 0,
	            "driverSaving"    : 0,
	            "nightStay"       : 0,
	            "daily"           : 0,
	            "minCharge"       : 0,
	            "unitTONmin"      : 2,
	            "unitTONmax"      : 3,
	            "unitCUFmin"      : 50,
	            "unitCUFmax"      : 75,
	            "unitBRICKmin"    : 1000,
	            "unitBRICKmax"    : 1125,
	            "unitBAGmin"      : (2*1000)/50,
	            "unitBAGmax"      : (3*1000)/50
            }
        ],
        "createdAt": new Date()
      },  	
      {
        "name": {
        	en: "Tractor Trolly",
        	hi: "ट्रेक्टर ट्रॉली"
        },
        "image": "/api/Containers/VehicleTypeImg/download/troli.png",
        "clickImage":"/api/Containers/VehicleTypeImg/download/troli_click.png",
        "driveImage": "/api/Containers/VehicleTypeImg/download/troli-drive.png",
        "updatedAt": new Date(),
        "loadValue": [
            {
	            "tyres"           : 6,
	            "loading"         : 0,
	            "unloading"       : 0,
	            "loadKM"          : 12,
	            "unloadKM"        : 10,
	            "driverSalary"    : 325,
	            "driverDailyExp"  : 100,
	            "helper"          : 0,
	            "gst"             : 0,
	            "royalty"         : 0,
	            "tollTax"         : 0,
	            "driverSaving"    : 0,
	            "nightStay"       : 0,
	            "daily"           : 0,
	            "minCharge"       : 0,
	            "unitTONmin"      : 4,
	            "unitTONmax"      : 8,
	            "unitCUFmin"      : 100,
	            "unitCUFmax"      : 200,
	            "unitBRICKmin"    : 2000,
	            "unitBRICKmax"    : 3000,
	            "unitBAGmin"      : (4*1000)/50,
	            "unitBAGmax"      : (8*1000)/50
            }
        ],
        "createdAt": new Date()
      },  	
  ];

  var VehicleType = app.models.VehicleType;
  function createVehicleType(){
    VehicleType.count(function(error,count){
        if(error) throw error;
        if(count == 0){
	    	VehicleType.create(data,function(){
	    		console.log("default vehicle type created");
	    	});   	
        }
    });
  }
  // createVehicleType();

 /* function changeName(){
  	data.forEach(function(value){
  		// console.log(value);
  		updateValue(value);
  	});
  }

  function updateValue(value){
  	VehicleType.findOne({where:{clickImage:value.clickImage}},function(err,success){
		// console.log(err);
		console.log(success);
		if(success){
			console.log(success);
			success.name = value.name;
			success.save(function(){});
		}
	});
  }

  changeName();*/

};