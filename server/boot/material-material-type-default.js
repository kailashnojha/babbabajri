'use strict';
/* jshint node: true ,esversion:6*/
module.exports = function(app) {
	var Material = app.models.Material;
	var MaterialType = app.models.MaterialType;
	var SubType = app.models.SubType;
	let data = [];

	data.push({
	    "name": {
	    	en: "Bazri",
	    	hi: "बजरी"
	    },
	    "createdAt": new Date(),
	    "updatedAt": new Date(),
	    "units": ["CUF","TON","BRICKS","BAG"],
	    // "subTypes": ["Grain","Fine","Coarse"],
	    materialTypes : [
	    	{
			    "name" : {
			    	en: "Banas",
			    	hi: "बनास"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/bazri-banas.jpg",
		        "subTypes" : ["grain","fine","coarse"]
			},
			{
			    "name" : {
			    	en: "Morel",
			    	hi: "मोरेल"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/bazri-morel.jpg",
		        "subTypes" : ["grain","fine","coarse"]
			},
			{
			    "name" : {
			    	en: "Banganga",
			    	hi: "बनगंगा"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/bazri-banganga.jpg",
		        "subTypes" : ["grain","fine","coarse"]
			},
			{
			    "name" : {
			    	en: "Sabi",
			    	hi: "साबी"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/bazri-sabi.jpg",
		        "subTypes" : ["grain","fine","coarse"]
			},
			{
			    "name" : {
			    	en: "Chambal",
			    	hi: "चम्बल"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/bazri-chambal.jpg",
		        "subTypes" : ["grain","fine","coarse"]
			},
			{
			    "name" : {
			    	en: "Mahi",
			    	hi: "माहि"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/bazri-mahi.jpg",
		        "subTypes" : ["grain","fine","coarse"]
			},
			{
			    "name" : {
			    	en: "Luni",
			    	hi: "लूनी"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/bazri-luni.jpg",
		        "subTypes" : ["grain","fine","coarse"]
			},
			{
			    "name" : {
			    	en: "Ghambhiri",
			    	hi: "घम्भीरी"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/bazri-ghambhiri.jpg",
		        "subTypes" : ["grain","fine","coarse"]
			},
			{
			    "name" : {
			    	en: "Kalisind",
			    	hi: "कलिसिंद"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/bazri-kalisind.jpg",
		        "subTypes" : ["grain","fine","coarse"]
			},
			{
			    "name" : {
			    	en: "Parwati",
			    	hi: "पार्वती"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/bazri-parwati.jpg",
		        "subTypes" : ["grain","fine","coarse"]
			},
			{
			    "name" : {
			    	en: "Jawai",
			    	hi: "जवाई"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/bazri-jawai.jpg",
		        "subTypes" : ["grain","fine","coarse"]
			},
			{
			    "name" : {
			    	en: "Beduch",
			    	hi: "बेडूक"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/bazri-beduch.jpg",
		        "subTypes" : ["grain","fine","coarse"]
			}
	    ]
  	});

	data.push({
		"name": {
			en: "Gitti/Rodi",
			hi: "गिट्टी/रोड़ी"
		},
	    "createdAt": new Date(),
	    "updatedAt": new Date(),
	    "units": ["CUF","TON","BRICKS","BAG"],
	    // "subTypes": ['wmm','10mm','20mm','40mm','60mm'],
	    materialTypes : [
	    	{
			    "name" : {
			    	en: "Blue",
			    	hi: "ब्लू"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/gitti-blue.jpg",
		        "subTypes" : ['wmm','10mm','20mm','40mm','60mm']
			},
			{
			    "name" : {
			    	en: "Reddish",
			    	hi: "रेड्डिश"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/gitti-reddish.jpg",
		        "subTypes" : ['wmm','10mm','20mm','40mm','60mm']
			}
	    ]
	});

	data.push({
		"name": {
			en: "Dust",
			hi: "डस्ट"
		},
	    "createdAt": new Date(),
	    "updatedAt": new Date(),
	    "units": ["CUF","TON","BRICKS","BAG"],
	    // "subTypes": [],
	    materialTypes : [
	    	{
			    "name" : {
			    	en: "Feldspar",
			    	hi: "फेल्डस्पार"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/dust-feldspar.jpg",
		        "subTypes" : []
			},
			{
			    "name" : {
			    	en: "Masonry",
			    	hi: "मसनरी"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/dust-masonry.jpg",
		        "subTypes" : []
			}
	    ]
	});

	data.push({
		"name": {
			en: "Cement",
			hi: "सीमेंट"
		},
	    "createdAt": new Date(),
	    "updatedAt": new Date(),
	    "units": ["CUF","TON","BRICKS","BAG"],
	    // "subTypes": [],
	    materialTypes : [
	    	{
			    "name" : {
			    	en: "Shree",
			    	hi: "श्री"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/cement-shree.jpg",
		        "subTypes" : []
			},
			{
			    "name" : {
			    	en: "Bangur",
			    	hi: "बांगर"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/cement-bangur.jpg",
		        "subTypes" : []
			},
			{
			    "name" : {
			    	en: "J K",
			    	hi: "जे के"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/cement-j-k.jpg",
		        "subTypes" : []
			},
			{
			    "name" : {
			    	en: "J K Laxmi",
			    	hi: "जे के लक्मी"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/cement-j-k-laxmi.jpg",
		        "subTypes" : []
			},
			{
			    "name" : {
			    	en: "Altratech",
			    	hi: "अल्ट्राटेक"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/cement-altratech.jpg",
		        "subTypes" : []
			},
			{
			    "name" : {
			    	en: "Ambuja",
			    	hi: "अम्बुजा"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/cement-ambuja.jpg",
		        "subTypes" : []
			},
			{
			    "name" : {
			    	en: "Binani",
			    	hi: "बिनानी"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/cement-binani.jpg",
		        "subTypes" : []
			},
			{
			    "name" : {
			    	en: "Acc",
			    	hi: "ऐ सी सी"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/cement-acc.jpg",
		        "subTypes" : []
			},
			{
			    "name" : {
			    	en: "Birla",
			    	hi: "बिरला"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/cement-birla.jpg",
		        "subTypes" : []
			},
			{
			    "name" : {
			    	en: "Lafarge",
			    	hi: "लाफार्ज"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/cement-lafarge.jpg",
		        "subTypes" : []
			},
			{
			    "name" : {
			    	en: "Wonder",
			    	hi: "वंडर"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/cement-wonder.jpg",
		        "subTypes" : []
			},
			{
			    "name" : {
			    	en: "Nirmex",
			    	hi: "निर्मेक्स"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/cement-nirmex.jpg",
		        "subTypes" : []
			},
			{
			    "name" : {
			    	en: "Rock Strong",
			    	hi: "रॉक स्ट्रांग"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/cement-rock-strong.jpg",
		        "subTypes" : []
			}
	    ]	
	});    	

	data.push({
		"name": {
			en: "Bricks",
			hi: "ब्रिक्स"
		},
	    "createdAt": new Date(),
	    "updatedAt": new Date(),
	    "units": ["CUF","TON","BRICKS","BAG"],
	    // "subTypes": ["1no", "2no", "3no", "Chatka"],
	    materialTypes : [
	    	{
			    "name" : {
			    	en: "Red Clay",
			    	hi: "रेड क्ले"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/bricks-red-clay.jpg",
		        "subTypes" : ["1no", "2no", "3no", "chatka"]
			},
			{
			    "name" : {
			    	en: "Cement",
			    	hi: "सीमेंट"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/bricks-cement.jpg",
		        "subTypes" : []
			},
			{
			    "name" : {
			    	en: "Fly Ash",
			    	hi: "फ्लाई ऐश"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/bricks-fly-ash.jpg",
		        "subTypes" : []
			}
	    ]
	});    

	data.push({
		"name": {
			en: "Water Tanker",
			hi: "वाटर टैंकर"
		},
	    "createdAt": new Date(),
	    "updatedAt": new Date(),
	    "units": ["CUF","TON","BRICKS","BAG"],
	    // "subTypes": [],
	    materialTypes : [
	    	{
			    "name" : {
			    	en: "Tractor Attach",
			    	hi: "ट्रेक्टर अटैच"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/water-tanker-tractor-attach.jpg",
		        "subTypes" : []
			},
			{
			    "name" : {
			    	en: "Truck Attach",
			    	hi: "ट्रक अटैच"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/water-tanker-truck-attach.jpg",
		        "subTypes" : []
			},
			{
			    "name" : {
			    	en: "Pickup Attach",
			    	hi: "पिक अप अटैच"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/water-tanker-pickup-attach.jpg",
		        "subTypes" : []
			}		
	    ]
	});   

	data.push({
		"name": {
			en: "Cheja Pathar/Khanda Pathar",
			hi: "चेजा पत्थर/खंडा पत्थर"
		},
	    "createdAt": new Date(),
	    "updatedAt": new Date(),
	    "units": ["CUF","TON","BRICKS","BAG"],
	    // "subTypes": ['Bolder','Small'],
	    materialTypes : [
	    	{
			    "name" : {
			    	en: "Sand Stone",
			    	hi: "सैंड स्टोन"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/cheja-pathar-sand-stone.jpg",
		        "subTypes" : ['bolder','small']
			},
			{
			    "name" : {
			    	en: "Masonry Stone",
			    	hi: "मसनरी स्टोन"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/cheja-pathar-masonry-stone.jpg",
		        "subTypes" : ['bolder','small']
			},
			{
			    "name" : {
			    	en: "Lime Stone",
			    	hi: "लाइम स्टोन"
			    },
		        "createdAt" : new Date(),
		        "updatedAt" : new Date(),
		        "image" : "/api/Containers/material-type/download/cheja-pathar-lime-stone.jpg",
		        "subTypes" : ['bolder','small']
			}
	    ]
	});

	function createData(){
		Material.count(function(err,count){
			if(count == 0){
				data.forEach(function(obj){
					createSingleRecord(obj);	
				});		
			}
		});
	}

	function createSingleRecord(obj){
		Material.create({
			/*"name"       : {
				en:obj.name,
				hi:obj.name
			},*/
			"name"       : obj.name,
		    "createdAt"  : new Date(),
		    "updatedAt"  : new Date(),
		    "units"      : obj.units,
		    "subTypes"   : obj.subTypes
		},function(err,material){
			if(material){
				obj.materialTypes.forEach(function(data){
					data.materialId = material.id;
					/*data.name = {
						en:data.name,
						hi:data.name
					}*/
					MaterialType.create(data,function(err,success){
						// console.log(success);
					});
				});
			}
		});
	}

	// createData();

	let subTypeData = [
		{
			name : {
				en: "grain",
				hi: "ग्रेन"
			},
			deleted:false,
			createdAt : new Date(),
			updatedAt : new Date()
		},
		{
			name : {
				en: "fine",
				hi: "फाइन"
			},
			deleted:false,
			createdAt : new Date(),
			updatedAt : new Date()
		},
		{
			name : {
				en: "coarse",
				hi: "कोर्स"
			},
			deleted:false,
			createdAt : new Date(),
			updatedAt : new Date()
		},
		{
			name : {
				en: "wmm",
				hi: "डब्ल्यू एम एम"
			},
			deleted:false,
			createdAt : new Date(),
			updatedAt : new Date()
		},
		{
			name : {
				en: "10mm",
				hi: "10 एम एम"
			},
			deleted:false,
			createdAt : new Date(),
			updatedAt : new Date()
		},
		{
			name : {
				en: "20mm",
				hi: "20 एम एम"
			},
			deleted:false,
			createdAt : new Date(),
			updatedAt : new Date()
		},
		{
			name : {
				en: "40mm",
				hi: "40 एम एम"
			},
			deleted:false,
			createdAt : new Date(),
			updatedAt : new Date()
		},
		{
			name : {
				en: "60mm",
				hi: "60 एम एम"
			},
			deleted:false,
			createdAt : new Date(),
			updatedAt : new Date()
		},
		{
			name : {
				en: "1no",
				hi: "1 नंबर"
			},
			deleted:false,
			createdAt : new Date(),
			updatedAt : new Date()
		},
		{
			name : {
				en: "2no",
				hi: "2 नंबर"
			},
			deleted:false,
			createdAt : new Date(),
			updatedAt : new Date()
		},
		{
			name : {
				en: "3no",
				hi: "3 नंबर"
			},
			deleted:false,
			createdAt : new Date(),
			updatedAt : new Date()
		},
		{
			name : {
				en: "chatka",
				hi: "चटका"
			},
			deleted:false,
			createdAt : new Date(),
			updatedAt : new Date()
		},
		{
			name : {
				en: "bolder",
				hi: "बोल्डर"
			},
			deleted:false,
			createdAt : new Date(),
			updatedAt : new Date()
		},
		{
			name : {
				en: "small",
				hi: "स्माल"
			},
			deleted:false,
			createdAt : new Date(),
			updatedAt : new Date()
		}
	];


	function createSubType(){
		SubType.count(function(err,count){
			if(err) throw err;
			if(count == 0){
				SubType.create(subTypeData,function(){});
			}
		});
	}

	// createSubType();



	function updateMaterialType(){
		data.forEach(function(value){

			Material.findOne({where:{"name.en" : value.name.en}},function(err,success){
				if(success){
					updateMaterialSubType(success.id,value.materialTypes);
					/*success.updateAttributes({
						name : value.name
					},function(){});*/
				} 
			});

		});	
	}

	
/*	updateMaterialType();

	function updateMaterialSubType(materialId, subTypeArr){

		subTypeArr.forEach(function(value){
			MaterialType.findOne({where:{materialId:materialId,name: value.name.en}},function(err,success){
				if(success){
					success.updateAttributes({
						name:value.name,
						subTypes:value.subTypes
					});	
				}
				
			});
		});
		
	}

*/

};

