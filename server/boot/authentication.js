'use strict';
/* jshint node: true ,esversion:6*/
module.exports = function enableAuthentication(server) {
  // enable authentication
  server.enableAuth();
};
