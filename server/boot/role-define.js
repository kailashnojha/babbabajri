'use strict';
/* jshint node: true ,esversion:6*/
module.exports = function(app) {

  var People = app.models.People;
  var WebUser = app.models.WebUser;
  var Role = app.models.Role;
  var RoleMapping = app.models.RoleMapping;

  var roleOptions = People.settings.rahOptions.roles || {};
  RoleMapping.belongsTo(People);
  RoleMapping.belongsTo(WebUser);
  People.hasMany(RoleMapping, {foreignKey: 'principalId'});
  Role.hasMany(People, {through: RoleMapping, foreignKey: 'roleId'});

  function createRoles(){
    Role.count(function(error,count){
        if(error) throw error;
        if(count == 0){
	       	var roles = roleOptions.roles || [];
	       	if(roles.length){
	       		let arrObj = [];
	       		roles.forEach(function(value){
	       			arrObj.push({name:value});
	       		});
	       	    Role.create(arrObj,function(err,success){
		            if(err) throw err;
		            createAdmin();
		        });
		    }else{
		    	createAdmin();
		    }    
        }else{
            createAdmin();
        }
    });
  }

  if(roleOptions.isDefine){
  	createRoles();
  }
  // createRoles();
  function createAdmin(){
  	var defatulRole = roleOptions.defaultUser;
  	if(defatulRole){
	    People.count({realm:defatulRole},function(error,count){
	      if(error) throw error;
	      if(count == 0){

	      	var adminData = {
	          fullName:defatulRole,
	          realm:defatulRole,
	          email: "info@babbabazri.com", 
	          mobile:'8696800499', 
	          address : 'jaipur',
	          password: defatulRole+'@123',
	          emailVerified:true,
	          mobileVerified:true,
	          adminApproval: "approved"
	        };

	        People.create(adminData, function(err, user) {
	          if (err) throw err;
	          console.log("admin created");
	          Role.findOne({
	          	where:{
	            	name: 'admin'
	            }
	          }, function(err, role) {
	            if (err) throw err;
	            role.principals.create({
	              principalType: RoleMapping.USER,
	              principalId: user.id
	            }, function(err, principal) {
	              if (err) throw err;
	            });
	          });
	        });
	      }
	    });
	}    
  }


  /*function updateData(){
  	People.findOne({
  		where:{
  			mobile:"8696800499"
  		}
  	},function(error, userInst){
  		if(userInst){
  			userInst.email = "info@babbabazri.com";
  			userInst.mobile = "8696800499";
  			userInst.mobileVerified = true;
  			userInst.save(function(){});
  			console.log(userInst);
  		}
  	});
  }

  updateData();*/

};