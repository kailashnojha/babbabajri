module.exports = function(msg,statusCode){
	var error = new Error(msg);
	error.statusCode = statusCode || 400;
	return error;
}