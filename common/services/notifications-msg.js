'use strict'
/* jshint node: true ,esversion:6*/
module.exports = {
	orderPlace : function(job,sendTo,ln){
        job.customer  = job.customer || {};
        job.material = job.material || {};
    	ln = ln || "en";
    	return {
            title              : {
                                    "en" : "New Request",
                                    "hi" : "नया अनुरोध"
                                 },
            body               : {
                                    "en" : job.customer.firstName+ " " +job.customer.lastName+" requested for "+job.material.name.en,
                                    "hi" : job.customer.firstName+ " " +job.customer.lastName+" "+job.material.name.hi+" के लिए अनुरोध किया गया"
                                 },
            "type"             : "order_place",
            sendTo             : sendTo,
            "orderRequestId"   : job.id.toString(),
            "sound"            : "default"
        };
	},
    confirmOrder : function(job,sendTo,ln){
        job.owner  = job.owner || {};
        ln = ln || "en";
        return {
            title              : {
                                    "en" : "Order Confirm",
                                    "hi" : "ऑर्डर पुष्टि करें"
                                },
            body               : {
                                    "en" : job.owner.firstName+ " " +job.owner.lastName+" confirm your request ",
                                    "hi" : job.owner.firstName+ " " +job.owner.lastName+" आपके अनुरोध की पुष्टि करें"
                                 },
            "type"             : "order_confirm",
            sendTo             : sendTo,
            "orderRequestId"   : job.id.toString(),
            "sound"            : "default"
        };
    },
    driverAssign : function(job,sendTo,ln){
        job.owner  = job.owner || {};
        ln = ln || "en";
        return {
            title              : {
                                    "en" : "New Job",
                                    "hi" : "नया काम"
                                 },
            body               : {
                                    "en" : job.owner.firstName+ " " +job.owner.lastName+" assigned you a new job",
                                    "hi" : job.owner.firstName+ " " +job.owner.lastName+" ने आपको एक नया काम सौंपा"
                                 },
            "type"             : "driver_assigned",
            sendTo             : sendTo,
            "orderRequestId"   : job.id.toString(),
            "sound"            : "default"
        };
    },    
    orderCancel : function(job,sendTo,ln){
        job.owner  = job.owner || {};
        ln = ln || "en";
        return {
            title              : {
                                    "en" : "Order Cancelled",
                                    "hi" : "आदेश रद्द कर दिया गया"
                                 },
            body               : {
                                    "en" : job.owner.firstName+ " " +job.owner.lastName+" cancelled your order for "+job.material.name.en,
                                    "hi" : job.owner.firstName+ " " +job.owner.lastName+" ने "+job.material.name.hi+" के लिए अपना ऑर्डर रद्द कर दिया"
                                 },
            "type"             : "order_cancelled",
            sendTo             : sendTo,
            "orderRequestId"   : job.id.toString(),
            "sound"            : "default"
        };
    },        
    agentVerifyRequest : function(job,sendTo,ln){

        console.log(job);
        job.customer  = job.customer || {};
        job.owner  = job.owner || {};
        job.driver  = job.driver || {};
        job.material = job.material || {};
        ln = ln || "en";
        return {
            title              : {
                                    "en" : "New Request",
                                    "hi" : "नया अनुरोध"
                                 },
            body               : {
                                    "en" : "Verification required for "+job.material.name.en+". Driver is "+job.driver.firstName + " "+job.driver.lastName+". Vehicle number is "+job.vehicle.truckNumber,
                                    "hi" : job.material.name.hi+" के लिए सत्यापन आवश्यक है। चालक "+job.driver.firstName + " "+job.driver.lastName+" है। वाहन संख्या "+job.vehicle.truckNumber+" है"    
                                 },
            "type"             : "order_verify_request",
            sendTo             : sendTo,
            "orderRequestId"   : job.id.toString(),
            "sound"            : "default"
        };        
    },
    startTripCustomer : function(job,sendTo,ln){
        job.customer  = job.customer || {};
        job.owner  = job.owner || {};
        job.driver  = job.driver || {};
        job.material = job.material || {};
        ln = ln || "en";
        return {
            title              : {
                                    "en" : "Order Dispatched",
                                    "hi" : "आर्डर निकल गया"
                                 },
            body               : {
                                    "en" : "Your order for "+job.material.name.en+" has been dispatched",
                                    "hi" : job.material.name.hi+" के लिए आपका आर्डर निकल गया"
                                 },
            "type"             : "order_dispatched_customer",
            sendTo             : sendTo,
            "orderRequestId"   : job.id.toString(),
            "sound"            : "default"
        };        
    },
    startTripOwner : function(job,sendTo,ln){
        job.customer  = job.customer || {};
        job.owner  = job.owner || {};
        job.driver  = job.driver || {};
        job.material = job.material || {};
        ln = ln || "en";
        return {
            title              : {
                                    "en" : "Order Dispatched",
                                    "hi" : "आर्डर निकल गया"
                                 },
            body               : {
                                    "en" : "Order for "+job.material.name.en+" has been dispatched",
                                    "hi" : job.material.name.hi+" का आर्डर निकल गया "
                                 },
            "type"             : "order_dispatched_owner",
            sendTo             : sendTo,
            "orderRequestId"   : job.id.toString(),
            "sound"            : "default"
        };        
    },
    orderVerifyDriver : function(job,sendTo,ln){
        job.customer  = job.customer || {};
        job.owner  = job.owner || {};
        job.driver  = job.driver || {};
        job.material = job.material || {};
        ln = ln || "en";
        return {
            title              : {
                                    "en" : "Order Verified",
                                    "hi" : "आर्डर सत्यापित गया"
                                 },
            body               : {
                                    "en" : "Order for "+job.material.name.en+" has been verified",
                                    "hi" : job.material.name.hi+" के लिए आर्डर सत्यापित किया गया है"
                                 },
            "type"             : "order_verify_driver",
            sendTo             : sendTo,
            "orderRequestId"   : job.id.toString(),
            "sound"            : "default"
        };        
    },
    orderVerifyOwner : function(job,sendTo,ln){
        job.customer  = job.customer || {};
        job.owner  = job.owner || {};
        job.driver  = job.driver || {};
        job.material = job.material || {};
        ln = ln || "en";
        return {
            title              : {
                                    "en" : "Order Verified",
                                    "hi" : "आर्डर सत्यापित गया"
                                 },
            body               : {
                                    "en" : "Order for "+job.material.name.en+" has been verified",
                                    "hi" : job.material.name.hi+" के लिए आर्डर सत्यापित किया गया है"
                                 },
            "type"             : "order_verify_owner",
            sendTo             : sendTo,
            "orderRequestId"   : job.id.toString(),
            "sound"            : "default"
        };        
    },
    orderDeliverCustomer : function(job,sendTo,ln){
        job.customer  = job.customer || {};
        job.owner  = job.owner || {};
        job.driver  = job.driver || {};
        job.material = job.material || {};
        ln = ln || "en";
        return {
            title              : {
                                   "en" : "Order delivered",
                                   "hi" : "आर्डर दिया गया"
                                 },
            body               : {
                                    "en" : "Order for "+job.material.name.en+" has been delivered",
                                    "hi" : job.material.name.hi+" के लिए आर्डर दिया गया है"
                                 },
            "type"             : "order_deliver_customer",
            sendTo             : sendTo,
            "orderRequestId"   : job.id.toString(),
            "sound"            : "default"
        };        
    },
    orderDeliverOwner : function(job,sendTo,ln){
        job.customer  = job.customer || {};
        job.owner  = job.owner || {};
        job.driver  = job.driver || {};
        job.material = job.material || {};
        ln = ln || "en";
        return {
            title              : {
                                    "en" : "Order delivered",
                                    "hi" : "आर्डर दिया गया"
                                 },
            body               : {
                                    "en" : "Order for "+job.material.name.en+" has been delivered",
                                    "hi" : job.material.name.hi+" के लिए आदेश दिया गया है"
                                 },
            "type"             : "order_deliver_customer",
            sendTo             : sendTo,
            "orderRequestId"   : job.id.toString(),
            "sound"            : "default"
        };        
    },
    productDeactivate : function(product,sendTo,ln){
        
        ln = ln || "en";
        return {
            title              : {
                                    "en" : "Prdouct deactivated",
                                    "hi" : "उत्पाद निष्क्रिय"
                                 },
            body               : {
                                    "en" : "Your product "+product.name + " is deactivated by Admin. Please contact to admin for re-activate.",
                                    "hi" : "आपका उत्पाद "+product.name+" व्यवस्थापक द्वारा निष्क्रिय किया गया है। पुनः सक्रिय करने के लिए व्यवस्थापक से संपर्क करें।"
                                 },
            "type"             : "product_deactivate",
            sendTo             : sendTo,
            "productId"        : product.id.toString(),
            "sound"            : "default"
        };        
    },
    productActivate : function(product,sendTo,ln){
        
        ln = ln || "en";
        return {
            title              : {
                                    "en" : "Prdouct activated",
                                    "hi" : "उत्पाद सक्रिय"
                                 },
            body               : {
                                    "en" : "Your product "+product.name + " is actviated by Admin.",
                                    "hi" : "आपका प्रोडक्ट "+product.name+" व्यवस्थापक द्वारा सक्रिय है"
                                 },
            "type"             : "product_deactivate",
            sendTo             : sendTo,
            "productId"        : product.id.toString(),
            "sound"            : "default"
        };        
    }                                                                                                                                                                              
};
