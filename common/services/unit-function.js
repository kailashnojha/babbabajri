'use strict';
/* jshint node: true ,esversion:6*/
module.exports ={
	getAllUnits : function(){
		return ["CUF", "TON", "BRICKS", "BAG","LITER"];
	},
    getAllUnitsByLn : function(ln){
        let obj = {
            "CUF": "घन फीट",
            "TON": "टन",
            "BRICKS": "ईंट",
            "BAG": "बैग/कट्टे",
            "LITER": "लीटर"
        };

        if(ln == "hi"){
            let obj2 = {};

            for(let x in obj){
                obj2[x.toLowerCase()]= obj[x];
            }

            return obj2;
        }else{
            let obj2 = {};
            for(let x in obj){
                obj2[x.toLowerCase()]= x;
            }
            return obj2;
        }

    },
    getUnitValueInLang : function(unit,ln){
        let obj = {
            "CUF": "घन फीट",
            "TON": "टन",
            "BRICKS": "ईंट",
            "BAG": "बैग/कट्टे",
            "LITER": "लीटर"
        };

        if(ln == "hi"){
            return obj[unit];
        }else{
            return unit;
        }
    },
	makeUnitKey : function(unit){
	    if(unit == "CUF"){
       		return "unitCUFmax";
        }else if(unit == "TON"){
        	return "unitTONmax";  
        }else if(unit == "BRICKS"){
        	return "unitBRICKmax";
        }else if(unit == "BAG"){
        	return "unitBAGmax";
        }else if(unit == "LITER"){
            return "unitLITERmax";
        }else{
        	return undefined;
        }
		// return "unit"+unit+"max";
	},
    validLoadValue : function(arr){
        arr.forEach(function(value){
            if(!value.loading && value.loading<0){
                return false;
            }else if(value.unloading && value.unloading < 0){
                return false;
            }else if(value.loadKM && value.loadKM < 0){
                return false;
            }else if(value.unloadKM && value.unloadKM < 0){
                return false;
            }else if(value.driverSalary && value.driverSalary < 0){
                return false;
            }else if(value.driverDailyExp && value.driverDailyExp < 0){
                return false;
            }else if(value.helper && value.helper < 0){
                return false;
            }else if(value.gst && value.gst < 0){
                return false;
            }else if(value.royalty && value.royalty < 0){
                return false;
            }else if(value.tollTax && value.tollTax < 0){
                return false;
            }else if(value.driverSaving && value.driverSaving < 0){
                return false;
            }else if(value.nightStay && value.nightStay < 0){
                return false;
            }else if(value.daily && value.daily < 0){
                return false;
            }else if(value.minCharge && value.minCharge < 0){
                return false;
            }else if(value.unitTONmax && value.unitTONmax < 0){
                return false;
            }else if(value.unitCUFmax && value.unitCUFmax < 0){
                return false;
            }else if(value.unitBRICKmax && value.unitBRICKmax < 0){
                return false;
            }else if(value.unitBAGmax && value.unitBAGmax < 0){
                return false;
            }else if(value.unitLITERmax && value.unitLITERmax < 0){
                return false;
            }   
        });
        return true;        
    },
    getSaving : function(unit,km,salaryValue,qty){
        if(km > 30){
            if(unit == "CUF"){
                // 25 cuf is eqaul to 1 ton 
                // 25 cuf is eual to 1000kg 
                // 1 cuf is equal to 40 kg
                return km * (40 * qty) * salaryValue;
            }else if(unit == "TON"){
                 // 1ton equal to 1000 kg
                return km * (1000 * qty) * salaryValue; 
            }else if(unit == "BRICKS"){
                // 1 bricks is equal to 2.67 kg
                return km * (2.67 * qty) * salaryValue;
            }else if(unit == "BAG"){
                // 1 bag is equal to 50 kg
                return km * (50 * qty) * salaryValue;
            }else{
                return 0;
            }
        }else{
            return 0;
        }    
    },
    getUnitToKGValue(unit,qty){
        if(unit == "CUF"){
            // 25 cuf is eqaul to 1 ton 
            // 25 cuf is eual to 1000kg 
            // 1 cuf is equal to 40 kg
            return 40 * qty;
        }else if(unit == "TON"){
             // 1ton equal to 1000 kg
             return 1000 * qty;
        }else if(unit == "BRICKS"){
            // 1 bricks is equal to 2.67 kg
            return 2.67 * qty;
        }else if(unit == "BAG"){
            // 1 bag is equal to 50 kg
            return 50 * qty;
        }else{
            return 0;
        }
    }


};