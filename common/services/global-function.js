'use strict';
/* jshint node: true ,esversion:6*/
module.exports ={
	getLnFromHeader : function(ctx){
		var req = ctx && ctx.req;
        var headers = req && req.headers;
        var query = req && req.query;
        var ln = headers.ln || query.ln || "en";
        ln = ln == "hi" ? "hi" : "en";
        return ln;
 	},
	getOTP : function(){
	    var val = Math.floor(1000 + Math.random() * 9000);
	    return val;
	},
	getLongOtp : function(){
		var val = Math.floor(100000 + Math.random() * 900000);
	    return val;
	},
	timeCoversion : function(startTime){
		// var startTime;
		var a = startTime.split(':');
		var minutes = (+a[0]) * 60 + (+a[1]);
		return minutes;
	},
	changeLN : function(value, ln){
	    if(value.product){
	      if(value.product.materialType){
	        value.product.materialType.name = value.product.materialType.name[ln];
	      }

	      if(value.product.material){
	        value.product.material.name = value.product.material.name[ln];
	      }
	    }

	    if(value.materialType){
	       value.materialType.name = value.materialType.name[ln]; 
	    }

	    if(value.material){
	      value.material.name = value.material.name[ln];
	    }

	    if(value.vehicle){
	      if(value.vehicle.vehicleType){
	        value.vehicle.vehicleType.name = value.vehicle.vehicleType.name[ln];
	        /*if(value.vehicle.vehicleType){
	        	value.vehicle.vehicleType.name = value.vehicle.vehicleType.name[ln];
	        }*/
	      }
	    }

	    if(value.vehicleType){
        	value.vehicleType.name = value.vehicleType.name[ln];
        }

	    return value;		
	}
};