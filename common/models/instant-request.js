'use strict';
/* jshint node: true ,esversion:6*/
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
module.exports = function(Instantrequest) {
    disableAllMethods(Instantrequest,["find"])

    Instantrequest.bookNow = function(req, driverId,cb){
        let userId = req.accessToken.userId;
        Instantrequest.app.models.OrderRequest.findbyId(driverId, function(err, orderInst){
            if(err)
                return cb(err,null);
            let data = {
                orderDate : new Date(),
                customerId : userId,
                driverId : driverId,
                orderId : orderInst.id,
                deliveryStatus : "Pending"
            };  
            Instantrequest.create(data, function(error, requestInst){
                if(error)
                    return cb(error, null)
                cb(null, {data : requestInst, msg : msg.bookNow})
            });
        });
    };
    Instantrequest.remoteMethod('bookNow', {
        accepts : [
            {arg : "req", type : "object", http : {source :"req"}},
            {arg : "driverId", type : "string", http: {source : "form"}},
        ],
        returns : [
            {arg : "success", type : "object"}
        ]
    });

};
