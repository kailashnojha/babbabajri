'use strict';
/* jshint node: true ,esversion:6*/
var shortid = require('shortid');
var msg = require("../messages/request-vehicle/request-vehicle-msg.json");
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;

module.exports = function(Requestvehicle) {
	disableAllMethods(Requestvehicle, ["find"]);

	Requestvehicle.request = function(req,res,cb){
		let creatorId = req.accessToken.userId;

		Requestvehicle.app.models.WebUser.findById(creatorId, function(errUser, creatorInst){
			if(errUser) return cb(errUser, null);

			if(creatorInst.realm === 'webuser'){
				Requestvehicle.findOne({where:{creatorId:creatorId}}, function(errReq, reqInst){
					if(errReq) return cb(errReq,null);
					if(reqInst) return cb(new Error("Already registered"),null);
					createRequest(creatorInst);
				});
			}else{
				createRequest(creatorInst);
			}	
		});

		function createRequest(creator){
		    Requestvehicle.app.models.Container.imageUpload(req, res, {container:"vehicle-request"}, function(err, success){
	            if(err) return cb(err, null);
				
				let data = JSON.parse(success.data);
				let files = success.files;
				
				let obj = data;
				obj.creatorId = creatorId;
				obj.createdAt = new Date();
				obj.updatedAt = new Date();
				obj.regToken = shortid.generate();

				if(files.signImg){
					obj.signImg = files.signImg[0].url;
				}			            

				if(files.vehicleImg){
					obj.vehicleImg = files.vehicleImg[0].url;
				}			            
	            
	            Requestvehicle.create(data,function(error,success){
	            	if(error) return cb(error,null);
	            	if(creator.realm== "webuser"){
	            		creator.vehicleDone = true;
	            		creator.save(function(){});
	            		Requestvehicle.app.models.Otp.formSubmissionMsg({mobile:obj.contactNo,token:obj.regToken},function(){});
	            	}
	            	cb(null,{data:success,msg:msg.request});
	            });	
	        });   
	    }    

	};

	Requestvehicle.remoteMethod("request",{
		accepts : [
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"res",type:"object",http:{source:"res"}}
		],
		returns : {arg:"success", type:"object"}
	});


	Requestvehicle.getRequestById = function(requestId, cb){
		Requestvehicle.findById(requestId,function(error,success){
			if(error) return cb(error,null);
			cb(null, {data:success});
		});
	};

	Requestvehicle.remoteMethod("getRequestById",{
		accepts :[
			{arg:"requestId",type:"string", required:true, http:{source:"query"}}
		],
		returns : {arg:"success",type:"object"},
		http:{verb:"get"}
	});

	Requestvehicle.getAllForms = function(skip, limit, creatorId, cb){
		skip = skip || 0;
		limit = limit || 10;

		let filter = {
			skip:skip,
			limit:limit,
			order:"createdAt DESC",
			include:"creator"
		};
		if(creatorId){
			filter.where = {
				creatorId : creatorId
			};
		}

		Requestvehicle.find(filter,function(error,success){
			if(error) return cb(error,null);
			Requestvehicle.count(filter.where,function(err,count){
				if(err) return cb(err,null);
				cb(null,{data:success, count:count, msg:msg.getAllForms});
			})
		})
	}

	Requestvehicle.remoteMethod("getAllForms",{
		accepts : [
			{arg:"skip",type:"number",http:{source:"query"}},
			{arg:"limit",type:"number",http:{source:"query"}},
			{arg:"creatorId",type:"string",http:{source:"query"}}
		],
		returns : {arg:"success", type:"object"},
		http : {verb:"get"} 
	})


	Requestvehicle.getFormById = function(formId, cb){
		Requestvehicle.findById(formId,{
			include:"creator"
		},function(error,formInst){
			if(error,null);
			cb(null,{data:formInst,msg:msg.getFormById});
		})
	}

	Requestvehicle.remoteMethod("getFormById",{
		accepts : [
			{arg:"formId", type:"string", http:{source:"query"}}
		],
		returns : {arg:"success",type:"object"},
		http : {verb:"get"}
	})


	

};
