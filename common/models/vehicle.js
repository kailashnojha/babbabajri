'use strict';
/* jshint node: true ,esversion:6*/
var msg = require("./../messages/vehicle/vehicle-msg.json");
var CustomError = require("./../../common/services/custom-error");
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var gf = require("./../services/global-function.js");
var changeLN = gf.changeLN;
module.exports = function(Vehicle) {

	disableAllMethods(Vehicle, ["find"]);
	
	Vehicle.validatesInclusionOf('approveStatus', { in: Vehicle.definition.properties.approveStatus.enum,message:"Invalid realm"});  

	Vehicle.addVehicle = function(req,res,cb){

		let ownerId = req.accessToken.userId;

		Vehicle.app.models.Container.imageUpload(req, res, { container: 'VehicleImg' }, function(err, success) {
	        if (err)
	          cb(err, null);
	        else if (success.data) {
	            success.data = JSON.parse(success.data);
	            createVehicle(success);
	        } else {
	            cb(new CustomError("data not found"), null);
	        }
	    });


		function createVehicle(obj){
			let approveStatus = "pending";
			var data = {
				approveStatus    : approveStatus,
				ownerId          : ownerId,
				vehicleTypeId    : obj.data.vehicleTypeId,
				tyres            : obj.data.tyres,
				truckNumber      : obj.data.truckNumber,
				registrationNo   : obj.data.registrationNo,
				createdAt        : new Date(),
				updatedAt        : new Date()
			};

			if (obj.files) {
				// if (obj.files.documents){
				// 	data.documents = [];
		        // 	obj.files.documents.forEach(function(value){
		        // 		data.documents.push(value.url);	
		        // 	})
				// }
				if (obj.files.pollutionCard)
				    data.pollutionCard = obj.files.pollutionCard[0].url;
				if (obj.files.RC)
				  	data.RC = obj.files.RC[0].url;
				if (obj.files.insurance)
					data.insurance = obj.files.insurance[0].url;
				if (obj.files.other)
				  data.other = obj.files.other[0].url;
	
	        }

	        Vehicle.app.models.People.findById(ownerId,function(errP,ownerInst){
	        	if(errP) return cb(errP,null);

		        Vehicle.create(data,function(error,success){
					if(error) return cb(error,null);
					cb(null,{data:success,msg:msg.addVehicle});
					Vehicle.app.models.Otp.vehicleReg({mobile:ownerInst.mobile},function(){});
				});			
	        });
		}
	};

	Vehicle.remoteMethod("addVehicle",{
		accepts : [
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"res",type:"object",http:{source:"res"}}
			
		],
		returns : {arg:"success",type:"object"}
	});

	Vehicle.getVehicleById = function(req,vehicleId,cb){
		let ln = req.accessToken.ln || "en";
		Vehicle.findById(vehicleId,function(error,success){
			if(error) return cb(error,null);
			/*if(success.vehicleType){
				success.vehicleType.name = success.vehicleType.name[ln];
			}*/
			cb(null,{data:success,msg:msg.getVehicles});
		});
	};

	Vehicle.remoteMethod("getVehicleById",{
		accepts : [
			{arg:"req", type:"object", http:{source:"req"}},
			{arg:"vehicleId", type:"string",required:true,http:{source:"query"}}
		],
		returns : {arg:"success",type:"object"},
		http:{verb:"get"}
	});

	Vehicle.approve = function(vehicleId, status, cb){
		Vehicle.findById(vehicleId,function(error,vehicleInst){
			if(error) return cb(error,null);
			
			vehicleInst.approveStatus = status;
			vehicleInst.updatedAt = new Date();
			vehicleInst.save(function(error,success){
				if(error) return cb(error,null);
				cb(null,{data:success,msg:msg.approve});
			});

		});
	};

	Vehicle.remoteMethod("approve",{
		accepts :[
			{arg:"vehicleId",type:"string",http:{source:"form"}},
			{arg:"status",type:"string",http:{source:"form"}}
		],
		returns : {arg:"success", type:"object"}
	});

	Vehicle.getMyVehicles = function(req,cb){
		let ln = req.accessToken.ln || "en";
		let ownerId = req.accessToken.userId;
		var filter = {"order":"createdAt DESC",where : {ownerId : ownerId}};
		filter.include = [{
			relation : "vehicleType",
			scope:{fields: ['name']}
		}];
		Vehicle.find(filter,function(error,success){
			if(error) return cb(error,null);

			let arr = [];
			success.forEach(function(vehicle){
				let value = vehicle.toJSON();
				arr.push(changeLN(value,ln));
			});

			cb(null,{data:arr, msg:msg.getVehicles});
		});
	};

	Vehicle.remoteMethod("getMyVehicles",{
		accepts : [
			{arg:"req", type:"object",http:{source:"req"}}
		],
		returns : {arg:"success",type:"object"},
		http:{verb:"get"}
	});

	Vehicle.getVehicles = function(req, cb){
		let ln = req.accessToken.ln;
		Vehicle.find(function(error,success){
			if(error) return cb(error,null);

			let arr = [];
			success.forEach(function(vehicle){
				let value = vehicle.toJSON();
				arr.push(changeLN(value,ln));
			});

			cb(null,{data:success,msg:msg.getVehicles});
		});
	};

	Vehicle.remoteMethod("getVehicles",{
		accepts : [
			{arg:"req", type:"object", http:{source:"req"}}
		],
		returns : {arg:"success",type:"object"},
		http:{verb:"get"}
	});

	Vehicle.getAllVehicle = function(req, type, skip, limit, searchStr, cb){
		let ln = req.accessToken.ln || "en";
		skip = skip || 0;
		limit = limit || 10;
		
		Vehicle.find({
			where   : {approveStatus:type},
			skip    : skip,
			limit   : limit, 
			order   : "createdAt DESC",
			include : [
				{
					relation:"owner"
				},
				{
					relation:"vehicleType",
					scope:{
						fields:["name"]
					}
				}
			]
		},function(error,vehicles){
			if(error) return cb(error,null);

			let arr = [];
			vehicles.forEach(function(vehicle){
				let value = vehicle.toJSON();
				arr.push(changeLN(value,ln));
			});

			cb(null,{data:vehicles,msg:msg.getAllVehicle});
		});
	};

	Vehicle.remoteMethod("getAllVehicle",{
		accepts : [
			{arg:"req", type:"object",http:{source:"req"}},	
			{arg:"type", type:"string",http:{source:"query"}},
			{arg:"skip", type:"number",http:{source:"query"}},
			{arg:"limit", type:"number",http:{source:"query"}},
			{arg:"searchStr", type:"string",http:{source:"query"}}
		],
		returns : {arg:"success", type:"object"},
		http:{verb:"get"}
	});




	Vehicle.assignDriver = function(req, driverId, vehicleId, cb){
		
		let ownerId = req.accessToken.userId;

		Vehicle.app.models.People.findOne({where:{ownerId:ownerId,id:driverId}},function(error,driverInst){
			if(error) return cb(error,null);
			if(driverInst) return cb(new CustomError("Unauthorized"),null);

			Vehicle.findById(vehicleId,function(err,vehicleInst){
				if(err) return cb(err,null);
				if(!vehicleInst) return cb(new CustomError("No vehicle found"),null);

				driverInst.save(function(errorA, success){
					if(errorA) return cb(errorA,null);
					cb(null,{data:success,msg:msg.assignDriver});
				});
				
			});
		});
	};

	Vehicle.remoteMethod("assignDriver",{
		accepts : [
			{arg:"req", type:"object", http: {source : "req"}},
			{arg:"driverId", type:"string", http: {source : "form"}},
			{arg:"vehicleId", type:"string", http: {source : "form"}},
		],
		returns : {arg:"success", type: "object"}
	});
	
};
