'use strict';
/* jshint node: true ,esversion:6*/

var msg = require("./../messages/material-request/material-request-msg.json");

var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
module.exports = function(Materialrequest) {

	disableAllMethods(Materialrequest, ["find"]);

	Materialrequest.createRequest = function(req, materialId, materialTypeId, qty, qtyUnit, providerId, cb){
		let customerId = req.accessToken.userId;
		let obj = {
			customerId, materialId, materialTypeId, qty, qtyUnit, providerId
		};
		// console.log("for last commit after this start phase 2");
		Materialrequest.create(obj,function(error,success){
			if(error) return cb(error,null);

			cb(null, {data:success, msg: msg.createRequest});
		});
	};

	Materialrequest.remoteMethod("createRequest",{
		accepts : [
			{arg:"req", type:"object", http:{source:"req"}},
			{arg:"materialId", type:"string", http:{source:"form"}},
			{arg:"materialTypeId", type:"string", http:{source:"form"}},
			{arg:"qty", type:"string", http:{source:"form"}},
			{arg:"qtyUnit", type:"string", http:{source:"form"}},
			{arg:"materi", type:"string", http:{source:"form"}},
		],
		returns : {arg:"success", type:"object"}
	});

	


};
