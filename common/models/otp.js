'use strict';
/* jshint node: true ,esversion:6*/
var globalFunction = require("../services/global-function");
var msg = require("../messages/otp/otp-msg.json");
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var gf = require("../services/global-function");
var CustomError = require("./../../common/services/custom-error");
const request = require("request");
var apiKey = "236386A6Hw6f4e5b937e22";
var senderId = "DVBABA";
module.exports = function(Otp) {
	
	disableAllMethods(Otp, ["find"]);

	// ************************************** All common SMS will come here ************************

	Otp.vehicleReg = function(smsData, cb){
		let msg = "Your vehicle number has successfully registered";
		smsData.mobile = "91"+smsData.mobile;
		var url = encodeURI(`http://smsp.myoperator.co/api/sendhttp.php?authkey=${apiKey}&mobiles=${smsData.mobile}&message=${msg}&sender=${senderId}&route=4&country=0`);		
		request.get(url,function(error, response, body){
				if(error)
					return cb(error,null);
				else
					cb(null,{success:body});
			}
		);
	};

	Otp.remoteMethod("vehicleReg",{
		accepts : [
		  	{"arg":"smsData","type":"object",http:{source:"body"}}
		],
		returns : {arg:"success",type:"object"}
	});

	// account approval by admin
	Otp.accApprByAdmin = function(smsData, cb){
		let msg = `Your account has ${smsData.status} successfully`;

		smsData.mobile = "91"+smsData.mobile;
		var url = encodeURI(`http://smsp.myoperator.co/api/sendhttp.php?authkey=${apiKey}&mobiles=${smsData.mobile}&message=${msg}&sender=${senderId}&route=4&country=0`);		
		request.get(url,function(error, response, body){
				if(error)
					return cb(error,null);
				else
					cb(null,{success:body});
			}
		);
	};

	Otp.remoteMethod("accApprByAdmin",{
		accepts : [
		  	{"arg":"smsData","type":"object",http:{source:"body"}}
		],
		returns : {arg:"success",type:"object"}
	});

	Otp.formSubmissionMsg = function(smsData, cb){
		
		let msg = `You have successfully registered. Your registration id is ${smsData.token}`;
		/*if(smsData.type == 'dealership'){
			msg = `You have successfully registered. Your registration id is ${}`;
		}else if(smsData.type == 'vehicle'){

		}else if(smsData.type == "employment"){

		}else{
			return;
		}*/

		smsData.mobile = "91"+smsData.mobile;
		var url = encodeURI(`http://smsp.myoperator.co/api/sendhttp.php?authkey=${apiKey}&mobiles=${smsData.mobile}&message=${msg}&sender=${senderId}&route=4&country=0`);		
		request.get(url,function(error, response, body){
				if(error)
					return cb(error,null);
				else
					cb(null,{success:body});
			}
		);
	};

	Otp.remoteMethod("accApprByAdmin",{
		accepts : [
		  	{"arg":"smsData","type":"object",http:{source:"body"}}
		],
		returns : {arg:"success",type:"object"}
	});



	// ************************************** All common SMS will come here ************************


	Otp.sendSMS = function(smsData, cb){
		let msg = "Use one time password(OTP) is "+smsData.otp;
		smsData.mobile = "91"+smsData.mobile;
		var url = encodeURI(`http://smsp.myoperator.co/api/sendhttp.php?authkey=${apiKey}&mobiles=${smsData.mobile}&message=${msg}&sender=${senderId}&route=4&country=0`);		
		request.get(url,function(error, response, body){
				if(error)
					return cb(error,null);
				else
					cb(null,{success:body});
			}
		);
		
	};

	Otp.remoteMethod("sendSMS",{
		accepts : [
		  	{"arg":"smsData","type":"object",http:{source:"body"}}
		],
		returns : {arg:"success",type:"object"}
	});

	Otp.checkResetOtp = function(peopleId, otp, ln, cb){
		Otp.app.models.People.findById(peopleId,function(errorP,peopleInst){
			if(errorP) return cb(errorP,null);
			if(!peopleInst) return cb(new CustomError("No user found"),null);
			if(!peopleInst.passwordOtp){
				let msg = ln == "hi" ? "पहले पासवर्ड रीसेट करने के लिए अनुरोध करें" : "Please request for reset password first";
				return cb(new CustomError(msg),null);
			} 
			var expiresAt = new Date(peopleInst.passwordOtp.expireAt);
			var currentDate = new Date();
			if(expiresAt > currentDate){
				if(peopleInst.passwordOtp.otp == otp)
					cb(null,{data:peopleInst,msg:msg.checkResetOtp});
				else{
					let msg = ln == "hi" ? "ओटीपी मेल नहीं खाता है" : "Otp doesn't match";
					cb(new CustomError(msg),null);
				} 
					
			}
			else{
				let msg = ln == "hi" ? "ओटीपी समय समाप्त हो गया है" : "Otp time has been Expired";
				cb(new CustomError(msg),null);
			} 
				
		});
	};

	Otp.remoteMethod("checkResetOtp",{
		accepts : [
			{arg:"peopleId", type:"string", required:true, http:{source:"query"}},
			{arg:"otp", type:"number", required:true, http:{source:"query"}},
			{arg:"ln",  type:"string", required:true, http:gf.getLnFromHeader}
		],
		returns : {arg:"success",type:"object"},
		// http:{verb:"get"}
	});



	Otp.verifyMobile = function(peopleId, otp, firebaseToken, ln, cb){
		Otp.app.models.People.findById(peopleId,function(errorP,peopleInst){
			if(errorP) return cb(errorP,null);
			
			if(!peopleInst){
				let msg = ln == "hi" ? "कोई उपयोगकर्ता नहीं मिला" : "No user found";
				return cb(new CustomError(msg),null);	
			} 
			peopleInst.signupOtp = peopleInst.signupOtp || {}; 
			let currentDate = new Date();
			if(peopleInst.signupOtp.expireAt > currentDate){
				if(peopleInst.signupOtp.otp == otp){
					peopleInst.signupOtp = {};
					peopleInst.mobileVerified = true;
					peopleInst.save(function(errorUP,updatedInst){
						if(errorUP) return cb(errorUP,null);
						updatedInst.createAccessToken(-1,{firebaseToken:firebaseToken}, function(err,token){
							if (err) return cb(err);
				            token.__data.user = updatedInst;
				            token.__data.access_token = token.id;
					        cb(err, {data:token,msg:msg.verifyMobile});    
						});
						// cb(null,{data:updatedInst,msg:msg.verifyMobile});
					});		
				}else{
					let msg = ln == "hi" ? "ओटीपी मेल नहीं खाता है" : "Otp doesn't match";
					cb(new CustomError(msg),null);	
				} 
					
			}else{
				let msg = ln == "hi" ? "ओटीपी समाप्त हो गया, कृपया ओटीपी फिर से भेजें" : "otp expired, please resend otp";
				cb(new CustomError(msg),null);
			} 
				

		});
	};

	Otp.remoteMethod("verifyMobile",{
		accepts : [
			{arg:"peopleId",type:"string",required:true,http:{source:"form"}},
			{arg:"otp",type:"number",required:true,http:{source:"form"}},
			{arg:"firebaseToken",type:"string",required:false,http:{source:"form"}},
			{arg:"ln",  type:"string", required:true, http:gf.getLnFromHeader}
		],
		returns : {arg:"success",type:"object"}
	});

	// Otp.checkOTP = function(userId,otp,obj,cb){
	Otp.checkOTP = function(obj,ln,cb){	
		Otp.app.models.User.findById(obj.userId,function(error,userInstance){
			if(error)
				cb(error,null);
			else{
				if(error) return cb(error,null);
				if(!userInstance){
					let msg = ln == "hi" ? "कोई उपयोगकर्ता नहीं मिला" : "No user found";
					return cb(CustomError(msg),null);
				} 

				if(userInstance.signupOtp.otp == otp)
					cb(null,{data:userInstance,msg:msg.checkOtp});
				else{
					let msg = ln == "hi" ? "ओटीपी मेल नहीं खाता है" : "Otp doesn't match";
					cb(new CustomError(msg),null);			
				}
			}	
		});
	};

	Otp.remoteMethod("checkOTP",{
		accepts : [
			{arg: "obj", type: 'object',http:{source:"body"}},
			{arg: "ln", type: 'string',http:gf.getLnFromHeader}
		],
		returns : {arg:'success',type: 'object'}
	});

	// Common Api's

	Otp.resendOtp = function(peopleId, type, ln, cb){
		type = type || "signup"; // type values : webuserSignup,webuserReset, signup, reset
		let UserModel;
		if(type == "signup" || type == "reset"){
			UserModel = Otp.app.models.People;
		}else{
			UserModel = Otp.app.models.WebUser;
			if(type == "webuserSignup"){
				type = "signup";
			}else{
				type = "reset";
			}
		}

		UserModel.findById(peopleId,function(err, peopleInstance){
			if(err)
				cb(err,null);
			else{
				if(!peopleInstance){
					let msg = ln == "hi" ? "कोई उपयोगकर्ता नहीं मिला" : "No user found";
					return cb(new CustomError(msg),null);
				} 

				if(type === "signup"){
					peopleInstance.signupOtp = {
						createdAt : new Date(),
						expireAt  : new Date(),
						otp       : gf.getOTP()
					};
					peopleInstance.signupOtp.expireAt.setMinutes(peopleInstance.signupOtp.expireAt.getMinutes()+5);
					
					if(peopleInstance.realm == "webuser" || peopleInstance.realm == "emitra")
						peopleInstance.passwordOtp.otp = gf.getLongOtp();

				}else{
					if(type === "reset"){
						peopleInstance.passwordOtp = {
							createdAt : new Date(),
							expireAt  : new Date(),
							otp       : gf.getOTP()
						};
						peopleInstance.passwordOtp.expireAt.setMinutes(peopleInstance.passwordOtp.expireAt.getMinutes()+5);

						if(peopleInstance.realm == "webuser" || peopleInstance.realm == "emitra")
							peopleInstance.passwordOtp.otp = gf.getLongOtp();

					}
					else{
						let msg = ln == "hi" ? "अमान्य प्रकार" : "Invalid type";
						return cb(new CustomError(msg),null);	
					} 
				}
				peopleInstance.save(function(err, peopleInst){
					if(err)
						cb(err,null);
					else{
						Otp.sendSMS({
                          otp:type === "reset"?peopleInstance.passwordOtp.otp:peopleInstance.signupOtp.otp,
                          mobile:peopleInst.mobile,
                          type:"mobileVerified"
                        },function(err,success){
                        	if(err)
                        		cb(err,null);
                        	else
                        		cb(null,{data:peopleInst,msg:msg.resendOtp});
                      	});
					}
				});
			}
		});
	};

	Otp.remoteMethod("resendOtp",{
		accepts : [
			{arg:'peopleId',type:"string",required:true},
			{arg:'type',type:"string"},
			{arg:'ln',type:"string", http : gf.getLnFromHeader}
		],
		returns : {arg:"success",type:"object"}
	});


	// ************************** Web User Api ****************************************

	Otp.verifyMobileWebUser = function(peopleId,otp,firebaseToken, ln,cb){
		Otp.app.models.WebUser.findById(peopleId,function(errorP,peopleInst){
			if(errorP) return cb(errorP,null);
			console.log(peopleInst);
			if(!peopleInst){
				let msg = ln == "hi" ? "कोई उपयोगकर्ता नहीं मिला" : "No user found";
				return cb(new CustomError(msg),null);
			} 
			peopleInst.signupOtp = peopleInst.signupOtp || {}; 
			let currentDate = new Date();
			if(peopleInst.signupOtp.expireAt > currentDate){
				if(peopleInst.signupOtp.otp == otp){
					peopleInst.signupOtp = {};
					peopleInst.mobileVerified = true;
					peopleInst.save(function(errorUP,updatedInst){
						if(errorUP) return cb(errorUP,null);
						updatedInst.createAccessToken(-1,{firebaseToken:firebaseToken}, function(err,token){
							if (err) return cb(err);
				            token.__data.user = updatedInst;
				            token.__data.access_token = token.id;
					        cb(err, {data:token,msg:msg.verifyMobile});    
						});
						// cb(null,{data:updatedInst,msg:msg.verifyMobile});
					});		
				}else{
					let msg = ln == "hi" ? "ओटीपी मेल नहीं खाता है" : "Otp doesn't match"; 
					cb(new CustomError(msg),null);
				} 
					
			}else {
				let msg = ln == "hi" ? "ओटीपी समाप्त हो गया, कृपया ओटीपी फिर से भेजें" : "otp expired, please resend otp"; 
				cb(new CustomError(msg),null);
			}

		});
	};

	Otp.remoteMethod("verifyMobileWebUser",{
		accepts : [
			{arg:"peopleId",type:"string",required:true,http:{source:"form"}},
			{arg:"otp",type:"number",required:true,http:{source:"form"}},
			{arg:"firebaseToken",type:"string",required:false,http:{source:"form"}},
			{arg:"ln",type:"string",http:gf.getLnFromHeader}

		],
		returns : {arg:"success",type:"object"}
	});


	Otp.checkResetOtpWebUser = function(peopleId, otp, ln, cb){
		Otp.app.models.WebUser.findById(peopleId,function(errorP,peopleInst){
			if(errorP) return cb(errorP,null);
			if(!peopleInst){
				let msg = ln == "hi"? "कोई उपयोगकर्ता नहीं मिला" : "No user found";
				return cb(new CustomError(msg),null);	
			} 
			if(!peopleInst.passwordOtp) return cb(new CustomError("Please request for reset password first"),null);
			var expiresAt = new Date(peopleInst.passwordOtp.expireAt);
			var currentDate = new Date();
			if(expiresAt > currentDate){
				if(peopleInst.passwordOtp.otp == otp)
					cb(null,{data:peopleInst,msg:msg.checkResetOtp});
				else{
					let msg = ln == "hi"? "ओटीपी मेल नहीं खाता है" : "Otp doesn't match";
					cb(new CustomError(),null);	
				} 
					
			}
			else{
				let msg = ln == "hi"? "ओटीपी समाप्त हो गया" : "Otp Expired";
				cb(new CustomError(msg),null);
			} 
				
		});
	};

	Otp.remoteMethod("checkResetOtpWebUser",{
		accepts : [
			{arg:"peopleId",type:"string",required:true,http:{source:"form"}},
			{arg:"otp",type:"number",required:true,http:{source:"form"}},
			{arg:"ln",type:"string",http:gf.getLnFromHeader}
		],
		returns : {arg:"success",type:"object"},
		// http:{verb:"get"}
	});


};
