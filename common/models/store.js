'use strict';
/* jshint node: true ,esversion:6*/
var CustomError = require("./../../common/services/custom-error");
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var msg = require("../messages/store/store-msg.json");
module.exports = function(Store) {
	disableAllMethods(Store, ["find"]);

	Store.createStore = function(req, address, materialId, materialTypeId, cb){
		let ownerId = req.accessToken.userId;
		let obj = {
			address, materialId, materialTypeId, ownerId
		};

		Store.create(obj, function(error,success){
			if(error) return cb(error,null);
			cb(null,{data:success,msg:msg.createStore});
		});
	};

	Store.remoteMethod("createStore",{
		accepts : [
			{arg:"req", type:"object", http:{source : "form"}},
			{arg:"address", type:"object", http:{source : "form"}},
			{arg:"materialId", type:"string", http:{source : "form"}},
			{arg:"materialTypeId", type:"string", http:{source : "form"}}
		],
		returns : {arg:"success", type: "object"}
	});

};
