'use strict';
/* jshint node: true ,esversion:6*/
var loopback = require('loopback');
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var gf = require("../services/global-function");
var msg = require("../messages/people/people-msg.json");
var path = require("path");
var Otp = require("../models/otp");
var CustomError = require("./../../common/services/custom-error");
const {OAuth2Client} = require('google-auth-library');
var clientId = "1054193910584-1k1op5lgski8m97fe88hi15vq0i8d9u6.apps.googleusercontent.com";
const client = new OAuth2Client(clientId);
var ObjectID = require('mongodb').ObjectID;
var unitFunction = require("./../services/unit-function.js");
var gf = require("./../services/global-function.js");
var changeLN = gf.changeLN;
var applyFilter = require('loopback-filters');
module.exports = function(People) {

  // Admin approval value :=> ["pending", "approved", "rejected"]

  disableAllMethods(People, ['find', "login", "logout", "confirm", "changePassword", "resetPassword", "setPassword"]);
  People.settings.rahOptions.roles = People.settings.rahOptions.roles || {};
  People.settings.rahOptions.roles.roles = People.settings.rahOptions.roles.roles || [];
  People.validatesPresenceOf('realm', { message: 'realm can not be blank' });
  People.validatesInclusionOf('adminApproval', { in: People.definition.properties.adminApproval.enum,message:"Invalid status"});  
  if(People.settings.rahOptions.roles.roles.length){
    People.validatesInclusionOf('realm', { in:  People.settings.rahOptions.roles.roles,message:"Invalid realm"});  
  }


  People.prototype.addFirebaseToken = function(token, ln, cb) {
    let self = this;
    if (typeof ln == 'function') {
      cb = ln;
      ln = "en";
    }
    cb(null, self);
  };


  People.blockUser = function(req, peopleId, ln, cb){

    let userId = req.accessToken.userId;
    People.findById(userId,function(errO, ownerInst){
      if(errO) return cb(errO,null);

      People.findById(peopleId,function(error, driverInst){
        if(error) return cb(error, null);

          if( (ownerInst.realm == "owner" && driverInst.ownerId.toString() == ownerInst.id.toString())  || ownerInst.realm == "admin" ){

            People.app.models.AccessToken.destroyAll({ id: driverInst.id }, function(err, success) {
              if (err)
                return cb(err, null);

              // driverInst.isBlock = false;
              if(driverInst.orderRequestId){
                let msg = ln == "hi" ? "नौकरी पर पहले से ही काम कर रहा है" : "Already working on a job"; 
                cb(new Error(msg),null);
              }else{
                if(driverInst.vehicleId){
                  People.app.models.Vehicle.findById(driverInst.vehicleId,function(errV, vehicleInst){
                    if(errV) return cb(errV,null);
                    // vehicleInst.unsetAttribute("driverId");
                    vehicleInst.driverId = null;
                    vehicleInst.save(function(errVU,vInst){
                      if(errVU) return cb(errVU,null);
                      BlockUpdate(ownerInst, driverInst);
                    });
                  });
                }else{
                  BlockUpdate(ownerInst, driverInst);
                }                  
              } 

            });  
          }
      });   
    });
    
    function BlockUpdate(ownerInst,driverInst){
      driverInst.isAvailable = false;
      driverInst.isBlock = !driverInst.isBlock;
      driverInst.unsetAttribute("vehicleId");
      if(driverInst.isBlock){
        driverInst.blockBy = ownerInst.realm;
      }else{
        driverInst.unsetAttribute("blockBy");
      }

      driverInst.save(function(errFinal, successFinal){
        if(errFinal) return cb(errFinal,null);
        cb(null, {data:successFinal,  msg:msg.blockUser});
      });
    }
  };

  People.remoteMethod("blockUser",{
    accepts : [
      {arg:"req", type:"object", http:{source:"req"}},
      {arg:"peopleId", type:"string", required:true, http:{source:"form"}},
      { arg: 'ln', type: 'string', http: gf.getLnFromHeader }
    ],
    returns : {arg:"success", type:"object"}
  });

  People.changeLanguage = function(ln, req, cb){
    req.accessToken.ln = ln == 'hi' ? 'hi' : 'en';
    req.accessToken.save(function(err,success){
      if(err)
        cb(err,null);
      else
        cb(null,{data:success,msg:msg.changeLanguageSuccess});
    });
  };

  People.remoteMethod("changeLanguage",{
    accepts:[
      {arg:"ln", type:"string", http:{source:"form"}},
      {arg:"req", type:"object", http:{source:"req"}},
    ],
    returns : {arg:"success", type:"object"}
  });

  People.updateAnyData = function(id,data,cb){

    People.findById(id,function(err,peopleInst){
      if(err) return cb(err,null);
      for(let x in data){
        peopleInst[x] = data[x];
      }

      peopleInst.save(function(error,updateData){
        if(error) return cb(error,null);
        cb(null,{data:updateData});
      });

    });
  };

  People.remoteMethod("updateAnyData",{
    accepts : [
      {arg:"id",type:"string",http:{source:"query"}},
      {arg:"data",type:"object",http:{source:"body"}}
    ],
    returns : {arg:"success", type:"object"}
  });

  People.updateProfile = function(req,res,cb){
    let peopleId = req.accessToken.userId;
  
    People.app.models.Container.imageUpload(req, res, { container: 'UserDocuments' }, function(err, success) {
      if (err)
        cb(err, null);
      else if (success.data) {
        success.data = JSON.parse(success.data);
        updatePro(success);
      } else {
        cb(new CustomError("data not found"), null);
      }
    });

    function updatePro(obj){

      var data = {
        firstName : obj.data.firstName,
        lastName  : obj.data.lastName,
        address : obj.data.address
      };

      People.findById(peopleId, function(err, peopleInst) {
        if (err) return cb(err, null);

        peopleInst.firstName = obj.data.firstName;
        peopleInst.lastName = obj.data.lastName;
        peopleInst.fullName = peopleInst.firstName + " "+peopleInst.lastName;
        peopleInst.mobile = obj.data.mobile;
        peopleInst.address = obj.data.address;
        peopleInst.profileImage = peopleInst.profileImage || "";

        if (obj.files) {
          if (obj.files.profileImage){
            peopleInst.profileImage = obj.files.profileImage[0].url;
          }
        }
      
        peopleInst.save(function(err, success) {
          if (err)
            cb(err, null);
          else
            cb(null,{data:success,msg:msg.editType});
        });

      });
    
    }
  };

  People.remoteMethod("updateProfile",{
    accepts : [
      {arg:"req",type:"object",http:{source:"req"}},
      {arg:"res",type:"object",http:{source:"res"}}
    ],
    returns : {arg:"success", type:"object"}
  });

  // Update name api

  People.updateName = function(req, firstName, lastName, cb){
    let peopleId = req.accessToken.userId;
    People.findById(peopleId,function(err, peopleInst){
      if(err) return cb(err,null);
      peopleInst.firstName = firstName;
      peopleInst.lastName = lastName;

      peopleInst.save(function(error, success){
        if(error) return cb(error,null);
        cb(null,{ data:success, msg:msg.updateName });
      });
    });
  };

  People.remoteMethod("updateName",{
    accepts : [
      {arg:"req",type:"object", http:{source:"req"}},
      {arg:"firstName",type:"string", http:{source:"form"}},
      {arg:"lastName",type:"string", http:{source:"form"}}
    ],
    returns : {arg:"success", type:"object"}
  });

  People.updateEmail = function(req, email, cb){
    let peopleId = req.accessToken.userId;
    People.findById(peopleId, function(err, peopleInst){
      if(err) return cb(err,null);
      if(peopleInst.email !== email)
        peopleInst.emailVerified = false;

      peopleInst.email = email;
      peopleInst.save(function(error,success){
        if(error) return cb(error,null);
        cb(null,{data:success, msg:msg.updateEmail});
      });
    });
  };

  People.afterRemote('updateEmail', function(context, instance, nextUser){
    var userInstance = instance.success.data;
    var options = {
      type      : 'email',
      to        : userInstance.email,
      from      : 'noreply@loopback.com',
      subject   : 'Thanks for registering.',
      redirect  : '/verified',
      user      : People,
      template  : path.resolve(__dirname, '../../server/views/verify.ejs'),
    };

    if (!userInstance.emailVerified && !!userInstance.email) {
      userInstance.verify(options, function(err, response, next) {
        if (err) return nextUser(err);
        nextUser();
      });
    } else {
      nextUser();
    }
  });


  People.remoteMethod("updateEmail",{
    accepts : [
      {arg:"req", type:"object", http:{source:"req"}},
      {arg:"email", type:"string", http:{source:"form"}}
    ],
    returns : {arg:"success", type:"object"}
  });

  People.contactUs = function(name, email, contactNumber, message, cb){
    let obj = {
      name : name,
      email : email,
      contactNumber : contactNumber,
      message : message
    };
    cb(null,{data:obj,msg:msg.contactUs});
  };

  People.remoteMethod("contactUs",{
    accepts : [
      {arg:"name", type:"string", http:{source:"form"}},
      {arg:"email", type:"string", http:{source:"form"}},
      {arg:"contactNumber", type:"string", http:{source:"form"}},
      {arg:"message", type:"string", http:{source:"form"}},
    ],
    returns : {arg:"sucess", type:"object"}
  });


  People.signup = function(firstName, lastName, address, realm, mobile, password, ln, cb){
    ln = ln == "hi" ? "hi" : "en";
    let data = {
      fullName   : firstName+" "+lastName,
      firstName  : firstName,
      lastName   : lastName,
      address    : address,
      realm      : realm,
      mobile     : mobile,
      password   : password,
      ownerId    : null
    };

    if(typeof data.address!= "object" || typeof data.address.location!= "object" ){
      let msg = ln == "hi" ? "गलत पता" : "Invalid address";
      return cb(new Error(msg),null);
    }

    data.createdAt = new Date();
    data.updatedAt = new Date();
    data.mobileVerified = false;
    if(data.realm == "user"){
      data.adminApproval = "approved";  
    }else{
      data.adminApproval = "pending";    
    }
    
    data.isSignupComplete = false;
    data.signupOtp = {
      createdAt  : new Date(),
      expireAt   : new Date(),
      otp        : gf.getOTP()
    };


    data.signupOtp.expireAt.setMinutes(data.signupOtp.expireAt.getMinutes() + 5);

    People.findOne({where:{mobile:mobile}},function(err,peopleInst){
      if(err) return cb(err,null);

      if(peopleInst){
        if(peopleInst.mobileVerified){
          let msg = ln == "hi" ? "मोबाइल नंबर पहले से मौजूद है" : "mobile number already exist";
          return cb(new Error(msg),null);
        }
        peopleInst.updateAttributes(data, function(error, peopleInst){
          if(error) return cb(error,null);
          cb(null, { data:peopleInst, msg:msg.signup });
        });
      }else{
        People.create(data, function(error,peopleInst){
          if(error) return cb(error,null);
          cb(null,{data:peopleInst,msg:msg.signup});
        });
      }
    });
  };

  People.remoteMethod('signup', {
    description: 'Signup users',
    accepts: [
      { arg: 'firstName', type: 'string', http: { source: 'form' } },
      { arg: 'lastName', type: 'string', http: { source: 'form' } },
      { arg: 'address', type: 'object', http: { source: 'form' } },
      { arg: 'realm', type: 'string', http: { source: 'form' } },
      { arg: 'mobile', type: 'string', http: { source: 'form' } },
      { arg: 'password', type: 'string', http: { source: 'form' } },
      { arg: 'ln', type: 'string', http: gf.getLnFromHeader }
    ],
    returns: { arg: 'success', type: 'object'},
    http: { verb: 'post' },
  });


  People.driverReg = function(req,res, firstName, lastName, mobile, password, address, ln, cb){
      ln = ln == "hi" ? "hi" : "en";
      People.app.models.Container.imageUpload(req, res, { container: 'DriverImg' }, function(err, success) {
          if (err)
            cb(err, null);
          else if (success.data) {
              let otherInfo = {
                "ownerId" : req.accessToken.userId
              };
              success.data = JSON.parse(success.data);
              // console.log(req.query);
              
              let driverInfo = {...success,...otherInfo};
              
              createType(driverInfo);
          } else {
              let msg = ln == "hi" ? "डेटा नहीं मिला" : "data not found";
              cb(new CustomError(msg), null);
          }
      });

      function createType(obj){
          var data = {
              firstName      : obj.data.firstName,
              lastName       : obj.data.lastName,
              mobile         : obj.data.mobile,
              password       : obj.data.password,
              realm          : "driver",
              adminApproval  : "pending",
              ownerId        : obj.ownerId,
              address        : obj.data.address,
              vehicleId      : null,
              isAvailable    : false,
              mobileVerified : true
          };
          // console.log("data=> ",data);
          if(typeof data.address!= "object" || typeof data.address.location!= "object" ){
            let msg = ln == "hi" ? "गलत पता" : "Invalid address"; 
            return cb(new Error(msg),null);
          }
          if (obj.files) {
            if (obj.files.driverLicense)
              data.driverLicense = obj.files.driverLicense[0].url;
            if (obj.files.policeVerify)
              data.policeVerify = obj.files.policeVerify[0].url;
            if (obj.files.aadharCard)
              data.aadharCard = obj.files.aadharCard[0].url;
            if (obj.files.image)
              data.image = obj.files.image[0].url;
            if (obj.files.profileImage)
              data.profileImage = obj.files.profileImage[0].url;
          }
          data.createdAt = new Date();
          People.create(data, function(err, success) {
            if (err) return cb(err, null);
            cb(null,{data:success,msg:msg.driverReg});
          });
      };
  };

  People.remoteMethod("driverReg",{
    accepts : [
      {arg:"req", type:"object",http:{source:"req"}},
      {arg : "res", type:"object", http:{source : "res"}},
      {arg:"firstName", type:"string",http:{source:"form"}},
      {arg:"lastName", type:"string",http:{source:"form"}},
      {arg:"mobile", type:"string",http:{source:"form"}},
      {arg:"password", type:"string",http:{source:"form"}},
      {arg:"address", type:"object",http:{source:"form"}},
      { arg: 'ln', type: 'string', http: gf.getLnFromHeader }
    ],
    returns : {arg:"success", type:"object"}
  });

  People.getAllUsers = function(status,type, searchStr, skip, limit, cb){
    type = type || "owner"; // [user, owner, driver]
    let filter = {
      where:{
        adminApproval:status,
        realm:type,
        searchStr:searchStr,
        mobileVerified:true
      }, 
      skip:skip, 
      limit:limit,
      order:"createdAt DESC"
    };
    People.find(filter, function(error,success){
      if(error) return cb(error,null);
      People.count(filter.where,function(err,count){
        if(err) return cb(err,null);
        cb(null,{data:success,totalCount:count,msg:msg.getAllUsers});
      });
      
    });
  };

  People.remoteMethod("getAllUsers",{
    accepts : [
      {arg:"status", type:"string",http:{source:"query"}},
      {arg:"type", type:"string",http:{source:"query"}},
      {arg:"searchStr", type:"string",http:{source:"query"}},
      {arg:"skip", type:"string",http:{source:"query"}},
      {arg:"limit", type:"string",http:{source:"query"}},
    ],
    returns : {arg:"success", type:"object"}
  });


  People.updateAllUser = function(cb){
    People.find({where:{}},function(error,success){
      success.forEach(function(value){
        value.fullName = value.firstName+" "+value.lastName;
        // value.geoPoint = new loopback.GeoPoint(value.address.location);
        value.save(function(){});
      });
      cb(null,success);
    });
  };

  People.remoteMethod("updateAllUser",{
    returns : {arg:"success",type:"object"}
  });


  People.approval = function(peopleId, status, cb){
    People.findById(peopleId,function(error, peopleInst){
      if(error) return cb(error,null);

      peopleInst.adminApproval = status;

      peopleInst.save(function(err, updateInst){
        if(error) return cb(err,null);
        cb(null,{data:updateInst, msg:msg.approval});
        People.app.models.Otp.accApprByAdmin({status:status,mobile:peopleInst.mobile},function(){});
      });

    });
  };

  People.remoteMethod("approval",{
    accepts : [
      {arg: "peopleId", type: "string", http : {source : "form"}},
      {arg: "status", type: "string", http : {source : "form"}}
    ],
    returns : {arg: "success", type: "object"}
  });

  ////password reset using otp---------------------///

  People.resetPassRequest = function(mobile, realm, ln, cb){
    ln = ln == "hi" ? "hi" : "en";
    if(realm != "user"){
      realm = undefined;
    }
    People.findOne({where:{mobile : mobile, realm: realm}}, function(err, peopleIns){
      if(err) return cb(err, null);
      if(!peopleIns){
        let msg = ln == "hi" ? "कोई उपयोगकर्ता नहीं मिला" : "No user found";
        return cb(new CustomError(msg), null); 
      }
        
       peopleIns.passwordOtp = {
              createdAt : new Date(),
              expireAt  : new Date(),
              otp       : gf.getOTP()
       };
       peopleIns.passwordOtp.expireAt.setMinutes(peopleIns.passwordOtp.expireAt.getMinutes() + 20);
       peopleIns.save(function(err, peopleInstance){
         if(err)
          cb(err, null);
         else{
          People.app.models.Otp.sendSMS({mobile : peopleIns.mobile, otp : peopleIns.passwordOtp.otp},function(){})
          cb(null,{data:peopleInstance,msg:msg.resetPassRequest});
        }
       });
       
    });
  };

  People.remoteMethod("resetPassRequest", {
    accepts : [
      {arg : "mobile", type : "string", http: {source : "form"}},
      {arg : "realm", type : "string", http: {source : "form"}},
      { arg: 'ln', type: 'string', http: gf.getLnFromHeader }
    ],
    returns : [
      {arg : "success", type : "object"}
    ]
  });

  People.resetPassword = function(id, otp, newPassword,ln, cb){
    ln = ln == "hi" ? "hi" : "en";
    People.findById(id, function(err, peopleIns){
      if(err){
        cb(err, null);
      }
      if(!peopleIns){
        let msg = ln == "hi" ? "कोई उपयोगकर्ता नहीं मिला" : "No User Found";
        return cb(new CustomError(msg), null);
      } 
      var expiersAt = new Date(peopleIns.passwordOtp.expireAt);
      var currentDate = new Date();
      var userOtp = peopleIns.passwordOtp.otp;
      if(expiersAt > currentDate){
        
        if(userOtp == otp){
          peopleIns.updateAttributes({password : newPassword}, function(err,peopleInst){
            
            if(err)
              cb(err, null)
            else{            
            cb(null,{data:peopleInst,msg:msg.resetPassword});
            }
          })
        }else{
          let msg = ln == "hi" ? "ओटीपी मेल नहीं खाता है" : "Otp does not match";
          return cb(new CustomError(msg), null);
        }
      }else{
        let msg = ln == "hi" ? "ओटीपी वैधता समाप्त हो गई" : "Otp validity expired";
        return cb(new CustomError(msg), null)
      }
    })
  };

  People.remoteMethod("resetPassword", {
    accepts : [
      {arg : "id", type : "string", http: {source : "form"}},
      {arg : "otp", type : "number", http: {source : "form"}},
      {arg : "newPassword", type : "string", http: {source : "form"}},
      { arg: 'ln', type: 'string', http: gf.getLnFromHeader }
    ],
    returns : [
      {arg : "success", type : "object"}
    ]
  });

  People.assignVehicle = function(vehicleId, driverId, ln, cb){
    ln = ln == "hi" ? "hi" : "en";
    People.app.models.Vehicle.findById(vehicleId, function(error, vehicleInst){
      if(error)
        return cb(error, null);

      if(!!vehicleInst.driverId){
        let msg = ln == "hi" ? "पहले से ही एक ड्राइवर को सौंपा गया है" : "Already assigned to a driver";
        return cb(new Error(msg),null);
      }

      if(vehicleInst.approveStatus === 'pending'){
        let msg = ln == "hi" ? "वाहन अनुमोदित नहीं है" : "Vehicle is not approved";
        return cb(new CustomError(msg), null);
      }else{
        People.findById(driverId, function(err, driverInst){
          if(err){
            return cb(err, null);
          }

          if(driverInst.adminApproval === 'pending'){
            let msg = ln == "hi" ? "चालक व्यवस्थापक द्वारा अनुमोदित नहीं है" : "Driver is not approved by admin";
            return cb(new CustomError(msg), null);
          }
          
          if(driverInst.isBlock){
            let msg = ln == "hi" ? "चालक खाता अवरुद्ध है" : "Driver account is blocked";
            return cb(new CustomError(msg), null);
          }

          driverInst.updateAttributes({vehicleId : vehicleInst.id, vehicleTypeId : vehicleInst.vehicleTypeId}, function(error, driverIns){
            if(error){
              return cb(error, null)
            }else{
              vehicleInst.updateAttributes({driverId : driverInst.id}, function(err, vehicleIns){
                if(err)
                  return cb(err, null);
                else{
                  return cb(null, {data : {vehicle : vehicleIns, driver : driverIns}, msg : msg.assignVehicle});
                }
              });
            }
          });
          
        });
      }
    });
  };

  People.remoteMethod('assignVehicle', {
    accepts : [
      {arg: "vehicleId", type: "string", http: {source : "form"}},
      {arg : "driverId", type: "string", http : {source : "form"}},
      { arg: 'ln', type: 'string', http: gf.getLnFromHeader }
    ],
    returns : [
      {arg : "success", type: "object"}
    ]
  });  

  People.getDriver = function(req, cb){
    var ownerId = req.accessToken.userId;
    People.find({"order":"createdAt DESC",where : {ownerId : ownerId, realm : 'driver'}}, function(err, drivers){
      if(err){
        return cb(err, null);
      }
      cb(null,{data : drivers , msg : msg.getDriver});
    });
  };

  People.remoteMethod('getDriver', {
    accepts : [
      {arg : "req", type : "object", http : {source : "req"}}
    ],
    returns : [
      {arg: "success", type: "object"}
    ],
    http : { verb : 'get'}
  });

  People.driverAvailability = function(req, res, cb){
      let ln = req.accessToken.ln == "hi" ? "hi" : "en";
      let driverId = req.accessToken.userId;

      People.app.models.Container.imageUpload(req, res, {container:"driver-bill-image"}, function(err, success){
          if(err) return cb(err, null);
          let data = JSON.parse(success.data);
          let files = success.files;

          People.findById(driverId, function(err, driverInst){
            if(err){
              return cb(err, null);
            }
            if(driverInst.adminApproval === 'pending'){
              let msg = ln == "hi" ? "चालक व्यवस्थापक द्वारा अनुमोदित नहीं है" : "Driver is not approved by admin";
              return cb(new CustomError(msg), null);
            }

            if(driverInst.isBlock){
              let msg = ln == "hi" ? "आपका खाता मालिक द्वारा निष्क्रिय कर दिया गया है" : "Your account is deactivate by owner";
              return cb(new CustomError(msg), null);
            }

            if(!driverInst.vehicleId){
              let msg = ln == "hi" ? "इस चालक को कोई वाहन सौंपा गया नहीं है" : "No vehicle assigned to this driver";
              return cb(new CustomError(msg), null);
            }

            if(driverInst.orderRequestId){
              let msg = ln == "hi" ? "पहले से ही एक नौकरी है" : "Already have a job";
              return cb(new Error(msg),null);
            }

            if(driverInst.materialId == undefined || driverInst.materialId.toString() != data.materialId || driverInst.orderQuantity != data.quantity || driverInst.unit != data.unit || driverInst.price != data.price){
              driverInst.loadingVehicleImg = "";
              driverInst.billImg           = "";
            }

            if(driverInst.materialTypeId && driverInst.materialTypeId.toString() != data.materialTypeId){
              driverInst.loadingVehicleImg = "";
              driverInst.billImg           = "";
            }



            driverInst.isAvailable       = data.isAvailable;
            driverInst.materialId        = data.materialId;
            driverInst.materialTypeId    = data.materialTypeId;
            driverInst.location          = data.location;
            driverInst.orderQuantity     = data.quantity;
            driverInst.unit              = data.unit;
            driverInst.unitPrice         = data.price;
            driverInst.price             = data.price;
            driverInst.subType           = data.subType;
            

            if(files.loadingVehicleImg){
              driverInst.loadingVehicleImg = files.loadingVehicleImg[0].url;
            }

            if(files.billImg){
              driverInst.billImg = files.billImg[0].url;
            }

            People.app.models.VehicleType.findById(driverInst.vehicleTypeId,function(errType,vehicleType){
              if(errType) return cb(errType,null);
              People.app.models.Vehicle.findById(driverInst.vehicleId,function(errVehicle,vehicleInst){
                if(errVehicle) return cb(errVehicle,null);
                let checkObj = checkVehicleCapacity(
                                    vehicleType,
                                    driverInst.orderQuantity,
                                    driverInst.unit,
                                    vehicleInst.tyres,
                                    ln
                                  );
                if(checkObj.isTrue){
                  driverInst.save(driverInst, function(error, driverInstance){
                    if(error){
                      return cb(error, null);
                    }else{
                      cb(null, {data : {driverInstance}, msg:msg.driverAvailability});
                    }  
                  });  
                }else{
                  cb(new Error(checkObj.msg),null);
                }  
              });
              
              
            });
          });      
      });  
  };

  function checkVehicleCapacity(vehicleType,qty,unit,tyres,ln){
    if(typeof tyres == "string"){
      tyres = parseInt(tyres);
    }

    let findValue;
    for(let i =0; i<vehicleType.loadValue.length;i++){
      if(vehicleType.loadValue[i].tyres == tyres){
        findValue = vehicleType.loadValue[i];
        break;
      }
    }


    let checkKey = unitFunction.makeUnitKey(unit);

    if(findValue){
      if(findValue[checkKey] >= qty){
        let msg = ln == "hi" ? "सब कुछ ठीक है" : "everything is fine";  
        return {
          msg:msg,
          isTrue: true
        };
      }else{
        let msg = ln == "hi" ? "अधिकतम "+findValue[checkKey]+" "+unitFunction.getUnitValueInLang(unit,ln)+" इस वाहन को आवंटित कर सकता है" : "Max "+findValue[checkKey]+" "+unit+" can assign this vehicle";
        return {
          msg:msg,
          isTrue:false
        };
      }
    }else{
      let msg = ln == "hi" ? "कुछ गलत हो गया" : "something went wrong";  
      return {
        msg:msg,
        isTrue: false
      };
    }
  }

  People.remoteMethod("driverAvailability",{
    accepts : [
      {arg:"req", type:"object", http:{source:"req"}},
      {arg:"res", type:"object", http:{source:"res"}}
    ],
    returns : {arg:"success", type:"object"}
  });

  People.getDriverAvailabilityData = function(req,cb){
    let driverId = req.accessToken.userId;
    People.findById(driverId,function(error,driverInst){
      if(error) return cb(error,null);
      let data = {};
      if(driverInst.materialId){
        data.isAvailable        = driverInst.isAvailable;
        data.materialId         = driverInst.materialId;
        data.materialTypeId     = driverInst.materialTypeId;
        data.location           = driverInst.location;
        data.orderQuantity      = driverInst.orderQuantity;
        data.unit               = driverInst.unit;
        data.price              = driverInst.price;
        data.loadingVehicleImg  = driverInst.loadingVehicleImg;
        data.billImg            = driverInst.billImg;
        data.subType            = driverInst.subType; 
      }
      cb(null,{data:data,msg:msg.getDriverAvailabilityData});
    });
  };

  People.remoteMethod("getDriverAvailabilityData",{
    accepts : {arg:"req", type:"object", http:{source:"req"}},
    returns : {arg:"success", type:"object"}
  });

  People.nearByAgent = function(location,cb){
    People.find({
        where:{
          "geoPoint":{near : location, maxDistance : 200, unit: 'kilometers'}
        }
    },function(error,success){
      if(error) return cb(error,null);
      cb(null,success);
    });
  };

  People.remoteMethod("nearByAgent",{
    accepts:[
      {arg:"location",type:"object",http:{source:"form"}}
    ],
    returns : {arg:"success", type:"object"}
  });

  // nearbuy drivers
  People.nearByDrivers = function(req, vehicleTypeId, location, materialId, cb){ 
    let ln = req.accessToken.ln || "en";
    var filter = {
      where :{
        and:[
          {or:[{orderRequestId:{exists:false}},{orderRequestId:null}]},
          {vehicleTypeId : vehicleTypeId},  
          {"materialId":materialId},
          {"isAvailable" : true}
        ]
      }
    };
    
    /*if(location){
        location = new loopback.GeoPoint(location);        
       filter.where.and.location = {near : location, maxDistance : 200, unit: 'kilometers'};
    }*/
    filter.include = [{
      relation : "material",
      scope : {
        fields : ["name"]
      }
    },{
      relation : "materialType",
      scope : {
        fields : ["name"]
      }
    },{
      relation : "vehicleType",
      scope : {
        fields : ["name","image","clickImage","driveImage"]
      }
    }];
    
    People.find(filter, function(err, drivers){
        if(err){
            return cb(err, null);
        }
       /* console.log("near by driver");
        console.log(drivers);
        console.log("near by driver");*/
        let arr = [];
        drivers.forEach(function(driver){
          let value = driver.toJSON();
          arr.push(changeLN(value,ln));  
        });

        if(location){
          arr = applyFilter(arr, {where: {location: {near : location, maxDistance : 200, unit: 'kilometers'}}});     
        }

        cb(null , {data:arr, msg:msg.nearByDrivers});
    });
  };

  People.remoteMethod('nearByDrivers', {
      accepts : [
          {arg : "req", type:"object", http:{source:"req"}},
          {arg : "vehicleTypeId", type:"string"},
          {arg : "location", type: "GeoPoint"},
          {arg : "materialId", type: "string"}
      ],
      returns :[
          {arg : "success", type :"object"}
      ],
      http:{verb:"get"}
  });

  People.driverForBooking = function(req,cb){
    let ownerId = req.accessToken.userId;
    var filter = {"order":"createdAt DESC",where : {ownerId : ownerId, adminApproval: "approved", vehicleId : {"neq" : null}}};

    // filter.where.vehicleId != null;
    console.log(filter);
    
    People.find(filter, function(err, drivers){
      if(err)
        return cb(err, null);
      cb(null, {data : drivers, msg: msg.driverForBooking});
    })
  };

  People.remoteMethod('driverForBooking', {
    accepts : [
      {arg : "req", type : "object", http : {source : "req"}}
    ],
    returns : [
      {arg : "success", type: "object"}
    ],
    http : {verb : "get"}
  });

  People.setLocation = function(req,location, cb){
    let userId = req.accessToken.userId;
    People.findById(userId, function(err, agentInst){
      if(err)
        return cb(err, null);
      if(agentInst.realm === "agent"){
        agentInst.location = location;
        agentInst.save(agentInst, function(error, agentInstance){
          if(error)
            return cb(error, null);
          cb(null, {data : agentInstance, msg : msg.setLocation});
        });
      }
    });
  };

  People.remoteMethod('setLocation', {
    accepts : [
      {arg : "req", type : "object", http : {source : "req"}},
      {arg : "location", type: "GeoPoint", http : {source : 'form'}}
    ],
    returns : [
      {arg: "success", type : "object"}
    ]
  });

  People.userById = function(req, cb){
    let userId = req.accessToken.userId;
    People.findById(userId, function(err, userInst){
      if(err)
        return cb(err, null);
      cb(null, {data : userInst, msg : msg.userById})
    });
  };

  People.remoteMethod('userById', {
    accepts : [
      {arg : "reg", type : "object", http : {source : "req"}}
    ],
    returns : [
      {arg : "success", type : "object"}
    ],
    http : {verb : "get"}
  });

  People.afterRemote('createEmitra', AfterSignup);
  People.afterRemote('driverReg', AfterSignup);
  People.afterRemote('signup', AfterSignup);

  function AfterSignup(context, instance, nextUser){
    var userInstance = instance.success.data;
    /*var options = {
      type: 'email',
      to: userInstance.email,
      from: 'noreply@loopback.com',
      subject: 'Thanks for registering.',
      redirect: '/verified',
      user: People,
      template: path.resolve(__dirname, '../../server/views/verify.ejs'),
    };*/

    if(userInstance.realm != 'driver' && typeof userInstance.signupOtp == 'object' && typeof userInstance.signupOtp.otp != undefined){
      People.app.models.Otp.sendSMS({
        otp:userInstance.signupOtp.otp,
        mobile:userInstance.mobile
      },function(){});
    }

    userInstance.addRole(userInstance.realm, function(error, success) {
      if(error) return nextUser(error);
      nextUser();
      /*if (!userInstance.emailVerified) {
        userInstance.verify(options, function(err, response, next) {
          if (err) return nextUser(err);
          nextUser();
        });
      } else {
        nextUser();
      }*/
    });
  }

  People.prototype.addRole = function(role, cb) {
    let self = this;
    People.app.models.Role.findOne({ where: { name: role } }, function(error, roleInst) {
      if (error)
        cb(error, null);
      else {
        if (roleInst) {
          function createRole() {
            People.app.models.RoleMapping.destroyAll({ principalId: self.id }, function(err, success) {
              if (err)
                return cb(err, null);

              roleInst.principals.create({
                principalType: People.app.models.RoleMapping.USER,
                principalId: self.id
              }, function(err, principal) {
                if (err) return cb(err, null);
                cb(null, principal);
                console.log("role created => ",roleInst.name);
              });
            });
          }
          createRole();
        } else {
          cb(new CustomError("No role found"), null);
        }
      }
    });
  };

  People.observe('before save', function updateLatLng(ctx, next) {

    if(ctx.instance && ctx.instance.address && typeof ctx.instance.address == 'object' && ctx.instance.address.location){
      ctx.instance.geoPoint = {lng:parseFloat(ctx.instance.address.location.lng),lat:parseFloat(ctx.instance.address.location.lat)};
      
      
    }
    else {
      if(ctx.data && ctx.data.address && typeof ctx.data.address == 'object' && ctx.data.address.location){
        ctx.data.geoPoint = {lng:parseFloat(ctx.data.address.location.lng),lat:parseFloat(ctx.data.address.location.lat)};
        
      }
    }

    if(ctx.instance && ctx.instance.firstName && ctx.instance.lastName){
      ctx.instance.fullName = ctx.instance.firstName+" "+ctx.instance.lastName;   
    }else{
      if(ctx.data && ctx.data.firstName && ctx.data.lastName){
        ctx.data.fullName = ctx.data.firstName+" "+ctx.data.lastName;   
      }
    }


    next();
  });



};
