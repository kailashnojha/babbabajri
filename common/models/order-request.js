'use strict';
/* jshint node: true ,esversion:6*/
var msg = require('../messages/order/order-msg.json');
var geoDist = require('geodist');
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var unitFunction = require("./../services/unit-function.js");
var gf = require("./../services/global-function.js");
var changeLN = gf.changeLN;
module.exports = function(Orderrequest) {

  disableAllMethods(Orderrequest, ["find"]);

  Orderrequest.placeOrder = function(
    req, vehicleTypeId, productId, deliveryDate, quantity, unit, address,totalTime,totalKM, ln ,cb
  ) {
    // add customerId
    let customerId = req.accessToken.userId;
    Orderrequest.app.models.Product.findById(productId, {
      include: ['material', 'materialType']
    }, function(err, productInst) {
      if (err)
        return cb(err, null);

      var productLocation = productInst.location;
      var userLocation = address.location;
      // var deliveryDistance = geoDist(productLocation, userLocation, { format: true, unit: 'km' });
      var deliveryDistance = totalKM;
      var data = {
        requestType       : "book_later",
        orderDate         : new Date(),
        subType           : productInst.subType, // subType added
        deliveryDate      : new Date(deliveryDate),
        orderQuantity     : quantity,
        vehicleTypeId     : vehicleTypeId,   // Vehicle type added
        productId         : productId,
        customerId        : customerId,
        materialId        : productInst.materialId,
        materialTypeId    : productInst.materialTypeId,
        ownerId           : productInst.ownerId,
        unit              : unit,
        unitPrice         : productInst.price,
        deliveryStatus    : "Pending",
        price             : productInst.price,
        pickupAddress     : productInst.address,
        bookingStatus     : "Not Confirmed",
        customerAddress   : address,
        deliveryDistance  : deliveryDistance
      };

      data.pickupAddress = productInst.address;
      data.pickupAddress.location =  productInst.location;

      Orderrequest.getPricingV2(
        data.materialId, quantity, unit, productInst.price, vehicleTypeId, totalTime, totalKM,ln,
      function(errPrice,priceInfo){
        if(errPrice) return cb(errPrice,null);
        data.priceInfo = priceInfo.data;
        data.price = data.priceInfo.totalPrice;

        Orderrequest.create(data, function(error, orderInst) {
          if (error) {
            return cb(error, null);
          } else {
            Orderrequest.app.models.Notification.orderPlace(orderInst.id, "book_later");
            cb(null, { data: { orderInst }, msg: msg.placeOrder });
          }
        });
      });
        

    });
  };

  Orderrequest.remoteMethod('placeOrder', {
    accepts: [
      { arg: "req", type: "object", http: { source: "req" } },
      { arg: "vehicleTypeId", type: "string",required:true, http: { source: "form" } },
      { arg: "productId", type: "string",required:true, http: { source: "form" } },
      { arg: "deliveryDate", type: "date",required:true, http: { source: "form" } },
      { arg: "quantity", type: "string",required:true, http: { source: "form" } },
      { arg: "unit", type: "string",required:true, http: { source: "form" } },
      { arg: "address", type: "object",required:true, http: { source: "form" } },
      { arg: "totalTime", type: "number", required:true, http: { source: "form" } },
      { arg: "totalKM", type: "number",required:true, http: { source: "form" } },
      { arg: "ln", type: "string",required:true, http: gf.getLnFromHeader },

    ],
    returns: [
      { arg: "success", type: "object" }
    ]
  });


  Orderrequest.getPricingV2 = function(materialId, quantity, unit, unitPrice, vehicleTypeId, totalTime, totalKM, ln, cb){
    
    Orderrequest.app.models.Material.findById(materialId, function(errM, materialInst){
        if(errM) return cb(errM,null);
        if(!materialInst){
          let msg = ln == "hi" ? "कोई सामग्री नहीं मिली" : "No material found";
          return cb(new Error(msg),null);
        } 

        let priceObj;
        let checkKey;
        let lastQty;
        let finalPrice = {};
        totalTime = totalTime || 0;
        let days = Math.round(totalTime/24);
        totalKM = totalKM || 0;

        Orderrequest.app.models.VehicleType.findById(vehicleTypeId,function(error, vehicleTypeInst){
          if(error) return cb(error,null);
          if(!vehicleTypeInst){
            let msg = ln == "hi" ? "कोई वाहन नहीं मिला" : "No vehicle found";
            return cb(new Error(msg),null);
          } 

          checkKey = unitFunction.makeUnitKey(unit);

          if(!checkKey) return cb(new Error("Invalid unit"),null);
          
          for( var i = 0; i < vehicleTypeInst.loadValue.length; i++ ){
            if( vehicleTypeInst.loadValue[i][checkKey] > quantity ){
              if(lastQty!= undefined && lastQty > vehicleTypeInst.loadValue[i][checkKey]){
                lastQty = vehicleTypeInst.loadValue[i][checkKey];
                priceObj = vehicleTypeInst.loadValue[i];
              }else{
                if(lastQty == undefined){
                  lastQty = vehicleTypeInst.loadValue[i][checkKey];
                  priceObj = vehicleTypeInst.loadValue[i];
                }
              }
            }
          }

          if(!priceObj){
            let msg = ln == "hi" ? "इस वाहन के लिए कोई कीमत नहीं मिली" : "No price found for this vehicle"; 
            return cb(new Error(msg), null);
          } 

          finalPrice = {
            price          : parseFloat((quantity * unitPrice).toFixed(2)),
            loading        : parseFloat(priceObj.loading.toFixed(2)),
            unloading      : parseFloat(priceObj.unloading.toFixed(2)),
            loadKM         : parseFloat((priceObj.loadKM * totalKM).toFixed(2)),
            unloadKM       : parseFloat((priceObj.unloadKM * totalKM).toFixed(2)),
            helper         : parseFloat(priceObj.helper.toFixed(2)),
            royalty        : parseFloat(materialInst.royalty.toFixed(2)),
            tollTax        : parseFloat(priceObj.tollTax.toFixed(2)),
            driverSalary   : parseFloat((priceObj.driverSalary * days).toFixed(2)),  
            // driverSaving   : parseFloat(priceObj.driverSaving.toFixed(2)),
            nightStay      : parseFloat((priceObj.nightStay * days).toFixed(2)),//
            minCharge      : parseFloat(priceObj.minCharge.toFixed(2)),
          };

          if(finalPrice.driverSalary == 0){
             finalPrice.driverSalary = parseFloat(priceObj.driverSalary.toFixed(2));
          }

          if(totalKM<40){
            finalPrice.driverDailyExp = 0;
          }

          let valueInKg = unitFunction.getUnitToKGValue(unit ,quantity);
          finalPrice.loading = parseFloat((finalPrice.loading * valueInKg * materialInst.loading).toFixed(2));
          finalPrice.unloading = parseFloat((finalPrice.unloading * valueInKg * materialInst.unloading).toFixed(2));


          let total = 0;
          for(let key in finalPrice){
            total += finalPrice[key];
          }

          if(totalKM <10){
            finalPrice.driverDailyExp = 0; 
          }else{
            let a = parseInt(totalKM/50);
            finalPrice.driverDailyExp = a == 0 ? 50 : (a+1) * totalKM;
          }
          
          finalPrice.driverSaving = unitFunction.getSaving(unit,totalKM,priceObj.driverSaving,quantity);
          finalPrice.driverSaving = parseFloat(finalPrice.driverSaving.toFixed(2));

          finalPrice.vehicleTypeId = vehicleTypeInst.id;
          finalPrice.tyres = priceObj.tyres;
          finalPrice.days = Math.ceil(days);
          finalPrice.totalKM = parseFloat(totalKM.toFixed(2));

          let taxValue = (total * materialInst.gst)/100;
          // finalPrice.gstTaxValue = parseFloat((total + taxValue).toFixed(2));
          finalPrice.gstTaxValue = parseFloat((taxValue).toFixed(2));
          
          finalPrice.totalPrice = parseFloat((total+finalPrice.driverSaving+finalPrice.driverDailyExp+finalPrice.gstTaxValue).toFixed(2));  
          /*let taxValue = (finalPrice.totalPrice * materialInst.gst)/100;
          finalPrice.gstTaxValue = parseFloat((finalPrice.totalPrice + taxValue).toFixed(2));
          
          finalPrice.totalPrice = parseFloat((total+finalPrice.driverSalary+finalPrice.driverDailyExp+finalPrice.gstTaxValue).toFixed(2));  */
          // console.log(finalPrice);
          return cb(null,{data:finalPrice,msg:msg.getPricing});
        });
    });  
  };

  Orderrequest.remoteMethod("getPricingV2",{
    accepts : [
      { arg:"materialId",type:"string", required:true},
      { arg:"quantity",type:"string", required:true},
      { arg:"unit",type:"string", required:true},
      { arg:"unitPrice",type:"number", required:true},
      { arg:"vehicleTypeId",type:"string", required:true},
      { arg:"totalTime",type:"number", required:true},
      { arg:"totalKM",type:"number", required:true},
      { arg:"ln",type:"string", required:true, http:gf.getLnFromHeader},
    ],
    returns : {arg:"success",type:"object"},
    http : {verb:"get"}
  });

  Orderrequest.getPricing = function(quantity, unit, unitPrice, vehicleTypeId, totalTime, totalKM, ln, cb){
    let priceObj;
    let checkKey;
    let lastQty;
    let finalPrice = {};
    totalTime = totalTime || 0;
    let days = parseInt(totalTime/24);
    totalKM = totalKM || 0;
    console.log("come in version 1 api");
    Orderrequest.app.models.VehicleType.findById(vehicleTypeId,function(error, vehicleTypeInst){
      if(error) return cb(error,null);
      if(!vehicleTypeInst){
        let msg = ln == "hi" ? "कोई वाहन नहीं मिला" : "No vehicle found";
        return cb(new Error(msg),null);
      } 

      checkKey = unitFunction.makeUnitKey(unit);

      if(!checkKey) return cb(new Error("Invalid unit"),null);
      
      for( var i = 0; i < vehicleTypeInst.loadValue.length; i++ ){
        if( vehicleTypeInst.loadValue[i][checkKey] > quantity ){
          if(lastQty!= undefined && lastQty > vehicleTypeInst.loadValue[i][checkKey]){
            lastQty = vehicleTypeInst.loadValue[i][checkKey];
            priceObj = vehicleTypeInst.loadValue[i];
          }else{
            if(lastQty == undefined){
              lastQty = vehicleTypeInst.loadValue[i][checkKey];
              priceObj = vehicleTypeInst.loadValue[i];
            }
          }
        }
      }

      if(!priceObj){ 
        let msg = ln == "hi" ? "इस वाहन के लिए कोई कीमत नहीं मिली" : "No price found for this vehicle";
        return cb(new Error(msg), null);
      }

      finalPrice = {
        price          : parseFloat((quantity * unitPrice).toFixed(2)),
        loading        : parseFloat(priceObj.loading.toFixed(2)),
        unloading      : parseFloat(priceObj.unloading.toFixed(2)),
        loadKM         : parseFloat((priceObj.loadKM * totalKM).toFixed(2)),
        unloadKM       : parseFloat((priceObj.unloadKM * totalKM).toFixed(2)),
        helper         : parseFloat(priceObj.helper.toFixed(2)),
        royalty        : parseFloat(priceObj.royalty.toFixed(2)),
        tollTax        : parseFloat(priceObj.tollTax.toFixed(2)),
        driverSalary   : parseFloat(priceObj.driverSalary.toFixed(2)),
        // driverSaving   : parseFloat(priceObj.driverSaving.toFixed(2)),
        nightStay      : parseFloat((priceObj.nightStay * days).toFixed(2)),//
        minCharge      : parseFloat(priceObj.minCharge.toFixed(2)),
      };

      if(totalKM<40){
        finalPrice.driverDailyExp = 0;
      }

      let total = 0;
      for(let key in finalPrice){
        total += finalPrice[key];
      }

      if(totalKM <10){
        finalPrice.driverDailyExp = 0; 
      }else{
        let a = parseInt(totalKM/50);
        finalPrice.driverDailyExp = a ==0 ? 50 : (a+1) * totalKM;
      }

      
      finalPrice.driverSaving = unitFunction.getSaving(unit,totalKM, parseFloat(priceObj.driverSaving.toFixed(2)),quantity);
      finalPrice.totalPrice = parseFloat((total+finalPrice.driverSaving+finalPrice.driverDailyExp).toFixed(2));
      finalPrice.vehicleTypeId = vehicleTypeInst.id;
      finalPrice.tyres = priceObj.tyres;
      finalPrice.days = Math.ceil(days);
      finalPrice.totalKM = parseFloat(totalKM.toFixed(2));

      return cb(null,{data:finalPrice,msg:msg.getPricing});
    });
  };

  Orderrequest.remoteMethod("getPricing",{
    accepts : [
      { arg:"quantity",type:"string", required:true},
      { arg:"unit",type:"string", required:true},
      { arg:"unitPrice",type:"number", required:true},
      { arg:"vehicleTypeId",type:"string", required:true},
      { arg:"totalTime",type:"number", required:true},
      { arg:"totalKM",type:"number", required:true},
      { arg:"ln",type:"string", required:true, http: gf.getLnFromHeader},
    ],
    returns : {arg:"success",type:"object"},
    http : {verb:"get"}
  });

  // owner get request list (order request not accepted)
  Orderrequest.requestList = function(req, cb) {
    let ln = req.accessToken.ln || "en";
    let ownerId = req.accessToken.userId;
    let filter = { 
        order:"orderDate DESC",
        where: { 
            and:[
                {ownerId: ownerId},
                {
                  or:[
                    {bookingStatus: "Not Confirmed"},
                      {
                        and:[
                          {driverId:{exists:false}},
                          {bookingStatus: "Confirm"}   
                        ]
                      }
                  ]
                }
            ]
             
        } 
    };
    filter.include = [{
      relation: "product",
      scope: {
        include: [{
            relation: "materialType",
            scope: { fields: ['name'] }
          },
          {
            relation: "material",
            scope: { fields: 'name' }
          }
        ]
      }
    },{
      relation: "customer",
      scope: {
        fields: ["firstName", "lastName", "fullName", "mobile", "address","profileImage"]
      }
    },{
      relation: "vehicleType",
      scope: {
        fields: ["name", "image", "clickImage", "driveImage"]
      }
    }];
    Orderrequest.find(filter, function(error, requests) {
      if (error) return cb(error, null);

      let arr = [];
      requests.forEach(function(request){
        let value = request.toJSON();
        
        arr.push(changeLN(value,ln));

      });
      cb(null, { data: arr, msg: msg.requestList });
    });
  };
  Orderrequest.remoteMethod('requestList', {
    accepts: [
      { arg: "req", type: "object", http: { source: "req" } }
    ],
    returns: [
      { arg: "success", type: "object" }
    ],
    http: { verb: "get" }
  });
  //ask kailash sir ----------about method

  // If driver get request then this function will confirm the status
  // IF owner call this function meaning owner assigning 
  Orderrequest.acceptRejectRequest = function(driverId, requestId, status, ln, cb) {
    Orderrequest.findById(requestId, function(error, bookingInst) {
      if (error) {
        return cb(error, null);
      }

      // if(!bookingInst) return cb(new )

      Orderrequest.app.models.People.findById(driverId, function(err, driverInst) {
        if (err)
          return cb(err, null);


        // console.log("is it coming status : ",status);
        bookingInst.bookingStatus = status;
        if (status === "Confirm") {

          if(driverInst.orderRequestId){
            let msg = ln == "hi" ? "चालक के पास पहले से ही नौकरी है" : "Driver already have job";
            return cb(new Error(msg),null);
          } 

          driverInst.orderRequestId = bookingInst.id;
          let productLocation = bookingInst.pickupAddress.location;
          let filter = { where: { realm: "agent", "adminApproval": "approved" } };
          filter.where.geoPoint = { near: productLocation, maxDistance: 50, unit: 'kilometers' };
          Orderrequest.app.models.People.findOne(filter, function(errorAgent, agentInst) {
            if (errorAgent)
              return cb(errorAgent, null);
            if (!agentInst) {
              bookingInst.agentId = null;
            } else {
              bookingInst.agentId = agentInst.id;
              bookingInst.agentVerified = "pending";
            }
            bookingInst.driverId = driverInst.id;
            bookingInst.vehicleId = driverInst.vehicleId;

            bookingInst.save(function(err, updateInst) {
              if (error) return cb(err, null);
              driverInst.save(function(errorDriver, updatedDriverInst) {
                if (errorDriver) return cb(err, null);

                Orderrequest.app.models.Notification.confirmOrder(updateInst.id);
                
                if(updateInst.requestType !== 'book_now')
                  Orderrequest.app.models.Notification.driverAssign(updateInst.id);
                  
                cb(null, { data: { updateInst, updatedDriverInst }, msg: msg.acceptRejectRequest });
              });

            });
          });
        } else {
          bookingInst.deliveryStatus = "Cancel";
          bookingInst.save(function(err, updateInst) {
            if (error) return cb(err, null);
            // console.log(updateInst)
            Orderrequest.app.models.Notification.orderCancel(updateInst.id);
            cb(null, { data: updateInst, msg: msg.acceptRejectRequest });
          });
        }
      });

    });
  };

  Orderrequest.remoteMethod('acceptRejectRequest', {
    accepts: [
      { arg: "driverId", type: "string", http: { source: "form" } },
      { arg: "requestId", type: "string", http: { source: "form" } },
      { arg: "status", type: "string", http: { source: "form" } },
      { arg: "ln", type: "string", http: gf.getLnFromHeader }
    ],
    returns: [
      { arg: "success", type: "object" }
    ]
  });

  // Get all current job customer

  Orderrequest.getCustCurrentJob = function(req,cb){
    let ln = req.accessToken.ln || "en";
    let customerId = req.accessToken.userId;
    let filter = {where:{customerId:customerId,deliveryStatus:"start"}};
    filter.include = [{
        relation: "product",
        scope: {
          include: [{
              relation: "material",
              scope: {
                fields: ["name"]
              }
            },
            {
              relation: "materialType",
              scope: {
                fields: ["name"]
              }
            }
          ]
        }
      },
      {
        relation: "customer",
        scope: {
          fields: ["firstName", "lastName", "fullName", "mobile", "address","profileImage"]
        }
      },
      {
        relation: "driver",
        scope: {
          fields: ["firstName", "lastName", "fullName", "mobile", "address","rating","profileImage"]
        }
      },
      {
        relation: "material",
        scope: {
          fields: ["name"]
        }
      },
      {
        relation: "materialType",
        scope: {
          fields: ["name"]
        }
      }, {
        relation: "vehicle",
        scope: {
          include: {
            relation : "vehicleType",
            scope: {
              fields: ["name", "image", "clickImage", "driveImage"]
            }
          }
        }
      },
      {
        relation: "rating"
      }

    ];

    Orderrequest.find(filter,function(error, success){
      if(error) return cb(error,null);

      let arr = [];//success.toJSON();
      success.forEach(function(request){
        let value = request.toJSON();
        arr.push(changeLN(value,ln));
      });
      cb(null,{data:arr,msg:msg.getCustCurrentJob});
    });
  };

  Orderrequest.remoteMethod("getCustCurrentJob",{
    accepts : [
      {arg:"req", type:"object",http:{source:"req"}}
    ],
    returns : {arg:"success",type:"object"},
    http : {verb:"get"}
  });

  // Get all for customer end here


  Orderrequest.confirmOrder = function(requestId, status, cb) {
    Orderrequest.findById(requestId, function(err, bookingInst) {
      if (err) return cb(err, null);
      bookingInst.bookingStatus = status;
      bookingInst.save(function(error, updateInst) {
        if (error) return cb(err, null);
        if(status == "Confirm")
          Orderrequest.app.models.Notification.confirmOrder(updateInst.id);
        else
          Orderrequest.app.models.Notification.orderCancel(updateInst.id);

        cb(null, { data: updateInst, msg: msg.confirmOrder });
      });
    });
  };

  Orderrequest.remoteMethod('confirmOrder', {
    accepts: [
      { arg: "requestId", type: "string", http: { source: "form" } },
      { arg: "status", type: "string", http: { source: "form" } }
    ],
    returns: [
      { arg: "success", type: "object" }
    ]
  });

  //Error here----------------------------------------------------------
  Orderrequest.orderList = function(req, cb) {
    let ln = req.accessToken.ln || "en";
    let ownerId = req.accessToken.userId;
    let currentDeliveryFilter = { "order":"orderDate DESC", where: { deliveryStatus: {neq:"end"}, driverId:{exists:true} ,bookingStatus: { nin: ["Not Confirmed","Cancel"] }, ownerId: ownerId } };
    currentDeliveryFilter.include = [{
        relation: "product"
      },
      {
        relation: "vehicle",
        scope: {
          include: {
            relation: "vehicleType",
            scope: {
              fields: ["name", "image", "clickImage", "driveImage"]
            }
          }
        }
      },
      {
      	relation: "rating"
      }
    ];
    let pastDeliveryFilter = { where: { deliveryStatus: "end", ownerId: ownerId },"order":"orderDate DESC" };
    pastDeliveryFilter.include = [{
        relation: "product"
      },
      {
        relation: "vehicle",
        scope: {
          include: {
            relation: "vehicleType",
            scope: {
              fields: ["name", "image", "clickImage", "driveImage"]
            }
          }
        }
      },
      {
      	relation: "rating"
      }
    ];
    Orderrequest.find(currentDeliveryFilter, function(err, currentDelivery) {
      if (err)
        return cb(err, null);

      let currArr = []; //currentDelivery.toJSON();
      currentDelivery.forEach(function(req1){
        let value = req1.toJSON();
        currArr.push(changeLN(value,ln));
      });

      Orderrequest.find(pastDeliveryFilter, function(error, pastDelivery) {
        if (error)
          return cb(error, null);

        let passArr = []; //pastDelivery.toJSON();
        pastDelivery.forEach(function(req2){
          let value = req2.toJSON();
          
          passArr.push(changeLN(value,ln));
        });
        cb(null, { data: { currentDelivery: currArr, pastDelivery: passArr }, msg: msg.orderList });
      });
    });
  };

  Orderrequest.remoteMethod('orderList', {
    accepts: [
      { arg: "req", type: "object", http: { source: "req" } }
    ],
    returns: [
      { arg: "success", type: "object" }
    ],
    http: { verb: "get" }
  });

  Orderrequest.agentNotificationList = function(req, status, cb) {
    let ln = req.accessToken.ln  || "en";
    let agentId = req.accessToken.userId;

    let filter = {};

    filter = { where: { agentId: agentId } ,"order":"orderDate DESC"};
    filter.include = [{
        relation: "product",
        scope: {
          include: [{
              relation: "material",
              scope: {
                fields: ["name"]
              }
            },
            {
              relation: "materialType",
              scope: {
                fields: ["name"]
              }
            }
          ]
        }
      },
      {
        relation: "customer",
        scope: {
          fields: ["firstName", "lastName", "fullName", "mobile", "address","profileImage"]
        }
      },
      {
        relation: "driver",
        scope: {
          fields: ["firstName", "lastName", "fullName", "mobile", "address","rating","profileImage"]
        }
      },
      {
        relation: "material",
        scope: {
          fields: ["name"]
        }
      },
      {
        relation: "materialType",
        scope: {
          fields: ["name"]
        }
      }, {
        relation: "vehicle",
        scope: {
          include: {
            relation : "vehicleType",
            scope: {
              fields: ["name", "image", "clickImage", "driveImage"]
            }
          }
        }
      },
      {
      	relation: "rating"
      }

    ];


    if (status === 'pending') {
      filter.where.agentVerified = "pending";
    } else {
      filter.where.agentVerified = { neq: "pending" };
    }
    Orderrequest.find(filter, function(err, notifications) {
      if (err)
        return cb(err, null);

      let arr = []; //notifications.toJSON();
      notifications.forEach(function(request){
        let value = request.toJSON();
        arr.push(changeLN(value,ln));
      });

      cb(null, { data: arr, msg: msg.agentNotificationList });
    });
  };

  Orderrequest.remoteMethod('agentNotificationList', {
    accepts: [
      { arg: "req", type: "object", http: { source: "req" } },
      { arg: "status", type: "string", http: { source: "query" } }
    ],
    returns: [
      { arg: "success", type: "object" }
    ],
    http: { verb: "get" }
  });

  Orderrequest.driverOrderList = function(req, cb) {

    let ln = req.accessToken.ln || "en";
    let userId = req.accessToken.userId;
    let filter = {"order":"orderDate DESC", where: { deliveryStatus: {inq:["Pending","start"]}, bookingStatus:{neq:"Cancel"}, driverId: userId } };
    filter.include = [{
        relation: "product",
        scope: {
          include: [{
              relation: "material",
              scope: {
                fields: ["name"]
              }
            },
            {
              relation: "materialType",
              scope: {
                fields: ["name"]
              }
            }
          ]
        }
      },
      {
        relation: "customer",
        scope: {
          fields: ["firstName", "lastName", "fullName", "mobile", "address","profileImage"]
        }
      },
      {
        relation: "material",
        scope: {
          fields: ["name"]
        }
      },
      {
        relation: "materialType",
        scope: {
          fields: ["name"]
        }
      },
      {
      	relation: "rating"
      }

    ];
    Orderrequest.find(filter, function(err, notifications) {
      if (err)
        return cb(err, null);

      let arr = []; //notifications.toJSON();
      notifications.forEach(function(request){
        let value = request.toJSON();
        arr.push(changeLN(value,ln));
      });


      cb(null, { data: arr, msg: msg.driverOrderList });
    });
  };

  Orderrequest.remoteMethod('driverOrderList', {
    accepts: [
      { arg: "req", type: "object", http: { source: "req" } }
    ],
    returns: [
      { arg: "success", type: "object" }
    ],
    http: { verb: "get" }
  });

  Orderrequest.bookNow = function(req, driverId, pickupAddress, customerAddress, totalTime, totalKM, ln, cb) {
    let userId = req.accessToken.userId;
    Orderrequest.app.models.People.findById(driverId, function(err, driverInst) {
      if (err)
        return cb(err, null);

      if(typeof pickupAddress.location != 'object' || typeof pickupAddress.location.lat != 'number' && typeof pickupAddress.location.lng != 'number'){
        let msg = ln == "hi" ? "अमान्य पिक अप पता" : "Invalid pickup address";
        return cb(new Error(msg),null);
      }
      if(typeof customerAddress.location != 'object' || typeof customerAddress.location.lat != 'number' && typeof customerAddress.location.lng != 'number'){
        let msg = ln == "hi" ? "अवैध ड्रॉप स्थान" : "Invalid drop address";
        return cb(new Error(msg),null);
      }

      // var deliveryDistance = geoDist(pickupAddress.location, customerAddress.location, { format: true, unit: 'km' });
      var deliveryDistance = totalKM;
      var data = {
        requestType       : "book_now",
        orderDate         : new Date(),
        deliveryDate      : new Date(),
        customerId        : userId,
        driverId          : driverInst.id,
        ownerId           : driverInst.id,
        vehicleId         : driverInst.vehicleId,
        pickupAddress     : pickupAddress,
        deliveryDistance  : deliveryDistance,
        customerAddress   : customerAddress,
        vehicleTypeId     : driverInst.vehicleTypeId,
        materialId        : driverInst.materialId,
        materialTypeId    : driverInst.materialTypeId,
        deliveryStatus    : "Pending",
        bookingStatus     : "Not Confirmed",
        orderQuantity     : driverInst.orderQuantity,
        unit              : driverInst.unit,
        price             : driverInst.price,
        unitPrice         : driverInst.price,
        subType           : driverInst.subType
      };

      Orderrequest.getPricingV2(
        data.materialId, driverInst.orderQuantity, driverInst.unit, driverInst.price, driverInst.vehicleTypeId, totalTime, totalKM,ln,
      function(errPrice,priceInfo){
        if(errPrice) return cb(errPrice,null);
        console.log("priceInfo => ",priceInfo);
        data.priceInfo = priceInfo.data;
        data.price = data.priceInfo.totalPrice;

        Orderrequest.create(data, function(error, requestInst) {
          if (error)
            return cb(error, null);

          Orderrequest.app.models.Notification.orderPlace(requestInst.id, "book_now");
          cb(null, { data: requestInst, msg: msg.bookNow });
        });
      });

    });
  };

  Orderrequest.remoteMethod('bookNow', {
    accepts: [
      { arg: "req", type: "object", required:true, http: { source: "req" } },
      { arg: "driverId", type: "string", required:true, http: { source: "form" } },
      { arg: "pickupAddress", type: "object", required:true, http: { source: "form" } },
      { arg: "customerAddress", type: "object", required:true, http: { source: "form" } },
      { arg: "totalTime", type: "number", required:true, http: { source: "form" } },
      { arg: "totalKM", type: "number", required:true, http: { source: "form" } },
      { arg: "ln", type: "string", required:true, http: gf.getLnFromHeader },
    ],
    returns: [
      { arg: "success", type: "object" }
    ]
  });

  Orderrequest.customerOrderList = function(req, cb) {
    let ln = req.accessToken.ln;
    let userId = req.accessToken.userId;

    let filter = { where: { customerId: userId },"order":"orderDate DESC" };
    filter.include = [{
        relation: "product",
        scope: {
          include: [{
              relation: "material",
              scope: {
                fields: ["name"]
              }
            },
            {
              relation: "materialType",
              scope: {
                fields: ["name"]
              }
            }
          ]
        }
      },
      {
        relation: "customer",
        scope: {
          fields: ["firstName", "lastName", "fullName", "mobile", "address","profileImage"]
        }
      },
      {
        relation: "driver",
        scope: {
          fields: ["firstName", "lastName", "fullName", "mobile", "address","rating","profileImage"]
        }
      },
      {
        relation: "material",
        scope: {
          fields: ["name"]
        }
      },
      {
        relation: "materialType",
        scope: {
          fields: ["name"]
        }
      }, {
        relation: "vehicle",
        scope: {
          include: {
            relation:"vehicleType",
            scope: {
              fields: ["name", "image", "clickImage", "driveImage"]
            }
          }
        }
      },
      {
      	relation: "rating"
      }

    ];


    Orderrequest.find(filter, function(err, customerOrders) {
      if (err)
        return cb(err, null);

      let arr = [];

      customerOrders.forEach(function(request){
        let value = request.toJSON();
        arr.push(changeLN(value,ln));
      });

      cb(null, { data: arr, msg: msg.customerOrderList });
    });
  };

  Orderrequest.remoteMethod('customerOrderList', {
    accepts: [
      { arg: "req", type: "object", http: { source: "req" } }
    ],
    returns: [
      { arg: "success", type: "object" }
    ],
    http: { verb: "get" }
  });

  Orderrequest.tripStatus = function(tripStatus, requestId, cb) {

    if(["start","end"].indexOf(tripStatus)==-1)
      return cb(new Error("Invalid status"),null); 

    Orderrequest.findById(requestId, function(err, orderInst) {
      if (err)
        return cb(err, null);


      Orderrequest.app.models.People.findById(orderInst.driverId, function(error, driverInst) {
        if (error)
          return cb(error, null);


        if (tripStatus === 'start') {
          orderInst.deliveryStatus = "start";
          driverInst.onTrip = true;
          driverInst.isAvailable = false;
          orderInst.save(function(errorOrder, updatedOrderInst) {
            if (errorOrder)
              return cb(error, null);
            driverInst.save(function(errorDriver, updatedDriverInst) {
              if (errorDriver)
                return cb(error, null);
              cb(null, { data: { updatedOrderInst :updatedDriverInst }, msg: msg.tripStart });
              // Notification
              Orderrequest.app.models.Notification.startTrip(updatedOrderInst.id);
            });
          });
        }
        if (tripStatus === 'end') {
          orderInst.deliveryStatus = "end";
          driverInst.onTrip = false;
          driverInst.isAvailable = false;
          driverInst.orderRequestId = null;
          orderInst.save(function(errorOrder, updatedOrderInst) {
            if (errorOrder)
              return cb(error, null);
            driverInst.save(function(errorDriver, updatedDriverInst) {
              if (errorDriver)
                return cb(error, null);
              cb(null, { data: { updatedOrderInst: updatedDriverInst }, msg: msg.tripEnd });
              Orderrequest.app.models.Notification.orderDeliver(updatedOrderInst.id);
            });
          });
        }

      });
    });
  };

  Orderrequest.remoteMethod('tripStatus', {
    accepts: [
      { arg: "tripStatus", type: "string", http: { source: "form" } },
      { arg: "requestId", type: "string", http: { source: "form" } }
    ],
    returns: [
      { arg: "success", type: "object" }
    ]
  });

  Orderrequest.driverTripHistory = function(req, cb) {
    let ln = req.accessToken.ln;
    let driverId = req.accessToken.userId;
    let filter = { where: { driverId: driverId, deliveryStatus: "end" } ,"order":"orderDate DESC"}
    /* filter.include = {
         relation : "customer",
         scope : {
             fields : ["fullName", "mobile", "address"]
         }
     }*/

    filter.include = [{
        relation: "product",
        scope: {
          include: [{
              relation: "material",
              scope: {
                fields: ["name"]
              }
            },
            {
              relation: "materialType",
              scope: {
                fields: ["name"]
              }
            }
          ]
        }
      },
      {
        relation: "customer",
        scope: {
          fields: ["firstName", "lastName", "fullName", "mobile", "address","profileImage"]
        }
      },
      {
        relation: "material",
        scope: {
          fields: ["name"]
        }
      },
      {
        relation: "materialType",
        scope: {
          fields: ["name"]
        }
      }

    ];

    Orderrequest.find(filter, function(error, tripHistory) {
      if (error)
        return cb(error, null);

      let arr = [];

      tripHistory.forEach(function(request){
        let value = request.toJSON();
        arr.push(changeLN(value,ln));
      });

      cb(null, { data: arr, msg: msg.driverTripHistory });
    });
  };

  Orderrequest.remoteMethod('driverTripHistory', {
    accepts: [
      { arg: "req", type: "object", http: { source: "req" } }
    ],
    returns: [
      { arg: "success", type: "object" }
    ],
    http: { verb: "get" }
  });

  Orderrequest.orderVerifyByAgent = function(requestId, status, feedback, req, cb) {
    Orderrequest.findById(requestId, function(err, orderInst) {
      if (err)
        return cb(err, null);
      orderInst.agentVerified = status;
      orderInst.agentFeedback = feedback;
      orderInst.save(function(error, updatedOrderInst) {
        if (error)
          return cb(error, null);
        cb(null, { data: updatedOrderInst, msg: msg.orderVerifyByAgent });
        Orderrequest.app.models.Notification.orderVerify(orderInst.id);
      });
    });
  };
  
  Orderrequest.remoteMethod('orderVerifyByAgent', {
    accepts: [
      { arg: "requestId", type: "string", http: { source: "form" } },
      { arg: "status", type: "string", http: { source: "form" } },
      { arg: "feedback", type: "string", http: { source: "form" } },
      { arg: "req", type: "object", http: { source: "req" } }
    ],
    returns: [
      { arg: "success", type: "object" }
    ]
  });

  Orderrequest.getAllOwnerOrderByAdmin = function(peopleId, skip, limit, cb){
    let ln = "en";
    let ownerId = peopleId;
    let currentDeliveryFilter = { "order":"orderDate DESC",skip:skip,limit:limit, where: { driverId:{exists:true} ,bookingStatus: { neq: "Not Confirmed" }, ownerId: ownerId } };
    currentDeliveryFilter.include = [{
        relation: "product"
      },
      {
        relation: "vehicle",
        scope: {
          include: {
            relation: "vehicleType",
            scope: {
              fields: ["name", "image", "clickImage", "driveImage"]
            }
          }
        }
      },
      {
        relation: "customer",
        scope: {
          fields: ["firstName", "lastName", "fullName", "mobile", "address","profileImage"]
        }
      },
      {
        relation: "agent",
        scope: {
          fields: ["firstName", "lastName", "fullName", "mobile", "address","profileImage"]
        }
      },
      {
        relation: "driver",
        scope: {
          fields: ["firstName", "lastName", "fullName", "mobile", "address","rating","profileImage"]
        }
      },
      {
        relation: "material",
        scope: {
          fields: ["name"]
        }
      },
      {
        relation: "materialType",
        scope: {
          fields: ["name"]
        }
      }
    ];
    
    Orderrequest.app.models.People.findById(peopleId,function(errP, peopleInst){
      if(errP) return cb(errP,null);
      Orderrequest.find(currentDeliveryFilter, function(err, currentDelivery) {
        if (err)
          return cb(err, null);
        Orderrequest.count(currentDeliveryFilter.where,function(errCount,count){
          if(errCount) return cb(errCount,null);
          let currArr = []; //currentDelivery.toJSON();
          currentDelivery.forEach(function(req1){
            let value = req1.toJSON();
            currArr.push(changeLN(value,ln));
          });
          cb(null, { data: currArr,count:count,userData:peopleInst, msg: msg.getAllOwnerOrderByAdmin });
          
        });
      });
    });

  };

  Orderrequest.remoteMethod("getAllOwnerOrderByAdmin",{
    accepts : [
      {arg:"peopleId", type:"string", required:true, http:{source:"query"}},
      {arg:"skip", type:"string", required:true, http:{source:"query"}},
      {arg:"limit", type:"string", required:true, http:{source:"query"}}
    ],
    returns : {arg:"success", type:"object"},
    http:{verb:"get"}
  });


};
