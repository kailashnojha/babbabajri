'use strict';
/* jshint node: true ,esversion:6*/
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var msg = require("../messages/rating/rating-msg.json");

module.exports = function(Rating) {
	disableAllMethods(Rating,["find"]);

	Rating.giveRating = function(req, orderRequestId, rating, cb){
		let customerId = req.accessToken.userId;
		if(rating<0 && rating>5) return cb(new Error("Invalid rating"),null);

		Rating.findOne({where:{orderRequestId:orderRequestId}},function(err, ratingInst){
			if(err) return cb(err,null);
			if(ratingInst){
				cb(null,{data:ratingInst,msg:msg.giveRating});
			}else{
				Rating.app.models.OrderRequest.findById(orderRequestId,function(error,orderRequestInst){
					if(err) return cb(error,null);

					if(!orderRequestInst) return cb(new Error("No order found"),null);

					Rating.app.models.People.findById(orderRequestInst.driverId,function(errP, driverInst){
						if(errP) return cb(errP,null);

						if(!driverInst.rating){
							driverInst.rating = {
								totalUsers  : 0,
								totalRating : 0,
								avgRating   : 0
							};
						}

						driverInst.rating.totalUsers++;
						driverInst.rating.totalRating += rating;
						driverInst.rating.totalRating = parseFloat(driverInst.rating.totalRating.toFixed(2));
						driverInst.rating.avgRating = parseFloat((driverInst.rating.totalRating/driverInst.rating.totalUsers).toFixed(2)); 
						// driverInst.save(function)
						let ratingData = {
							customerId      : customerId,
							driverId        : driverInst.id,
							rating          : rating,
							orderRequestId  : orderRequestId,
							createdAt       : new Date()
						};
						orderRequestInst.isRatingDone = true;
						orderRequestInst.save(function(errOrder,succOrder){
							if(errOrder) return cb(errOrder,null);
							Rating.create(ratingData,function(errR,ratingInst){
								if(errR) return cb(errR,null);
								driverInst.save(function(){});
								cb(null,{data:ratingInst,msg:msg.giveRating});
							});	
						});
					});
				});
			}

		});
	};

	Rating.remoteMethod("giveRating",{
		accepts : [
			{arg:"req", type:"object", http:{source:"req"}},
			{arg:"orderRequestId", type:"string", http:{source:"form"}},
			{arg:"rating", type:"number",required:true, http:{source:"form"}}
		],
		returns : {arg:"success",type:"object"}
	});

};
