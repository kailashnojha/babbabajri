'use strict';
/* jshint node: true ,esversion:6*/
var msg = require("./../messages/material/material-msg.json");
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;

module.exports = function(Material) {

	disableAllMethods(Material, ["find"]);
	Material.validatesPresenceOf('name', { message: 'name can not be blank' });
	Material.validatesPresenceOf('createdAt', { message: 'createdAt can not be blank' });
	Material.validatesPresenceOf('updatedAt', { message: 'updatedAt can not be blank' });

	Material.getMaterials = function(req, cb){
		let ln = req.accessToken.ln || "en";
		Material.find({
			where:{
				deleted:{neq:true}
			},
			include:{
				relation:"materialTypes",
				scope:{
					where:{
						deleted:{neq:true}
					}
				}
			}
		},function(error,success){
			if(error) return cb(error,null);
			let arr = [];

			success.forEach(function(value){
				let mat = value.toJSON();
				mat.name = mat.name[ln];
				if(mat.materialTypes && mat.materialTypes.length){
					mat.materialTypes.forEach((value)=>{
						value.name = value.name[ln];
					});
					arr.push(mat);
				}
			});

			cb(null,{data:arr,msg:msg.getMaterials});
		});
	};

	Material.remoteMethod("getMaterials",{
		accepts : [
			{arg:"req", type:"object", http:{source:"req"}}
		],
		returns : {arg:"success",type:"object"},
		http:{verb:"get"}
	});

	Material.getMaterialsByAdmin = function(cb){
		Material.find({
			where:{
				deleted:{neq:true}
			},
			include:{
				relation:"materialTypes",
				scope:{
					where:{
						deleted:{neq:true}
					}
				}
			}
		},function(error,success){
			if(error) return cb(error,null);
			cb(null,{data:success,msg:msg.getMaterials});
		});
	};

	Material.remoteMethod("getMaterialsByAdmin",{
		accepts : [
			// {arg:"req", type:"object", http:{source:"req"}}
		],
		returns : {arg:"success",type:"object"},
		http:{verb:"get"}
	});



	Material.addMaterial = function(name, units, loading, unloading, gst, royalty, subTypes, cb){
		Material.create({
			name       : {
				en: name.en.trim(),
				hi: name.hi.trim()
			},
			units      : units,
			loading    : loading,
			unloading  : unloading,
			gst        : gst,
			royalti    : royalty,
			subTypes   : subTypes,
			createdAt  : new Date(),
			updatedAt  : new Date(),
			deleted    : false
		},function(error,success){
			if(error) return cb(error,null);
			cb(null,{data:success, msg:msg.addMaterial});
		});
	};

	Material.remoteMethod("addMaterial",{
		accepts : [
			{arg:"name", type:"object", http:{source:"form"}},
			{arg:"units", type:"array", http:{source:"form"}},
			{arg:"loading", type:"number", http:{source:"form"}},
			{arg:"unloading", type:"number", http:{source:"form"}},
			{arg:"gst", type:"number", http:{source:"form"}},
			{arg:"royalty", type:"number", http:{source:"form"}},
			{arg:"subTypes", type:"array", http:{source:"form"}}
		],
		returns : {arg:"success", type:"object"}
	});

	Material.editMaterial = function(materialId, name, units, loading, unloading, gst, royalty, subTypes, cb){
		Material.findById(materialId,function(error,materialInst){
			if(error) return cb(error,null);
			materialInst.updateAttributes({
				name       : {
					en: name.en.trim(),
					hi: name.hi.trim()
				},
				units      : units,
				loading    : loading,
				unloading  : unloading,
				gst        : gst,
				royalti    : royalty,
				subTypes   : subTypes,
				updatedAt  : new Date()
			},function(err,data){
				if(err) return cb(err,null);
				cb(null, { data:data, msg:msg.editMaterial });
			}); 
		});
	};

	Material.remoteMethod("editMaterial",{
		accepts : [
			{arg:"materialId",type:"string",http:{source:"form"}},
			{arg:"name",type:"object",http:{source:"form"}},
			{arg:"units",type:"array",http:{source:"form"}},
			{arg:"loading", type:"number", http:{source:"form"}},
			{arg:"unloading", type:"number", http:{source:"form"}},
			{arg:"gst", type:"number", http:{source:"form"}},
			{arg:"royalty", type:"number", http:{source:"form"}},
			{arg:"subTypes", type:"array", http:{source:"form"}}
		],
		returns : {arg:"success",type:"object"}
	});

	Material.getSingleMaterial = function(req, materialId, cb){
		let ln = req.accessToken.ln || "en";
		Material.findById(materialId,{
			include:{
				relation:"materialTypes",
				scope:{
					where:{
						deleted:{neq:true}
					}
				}
			},
			
		},function(error,materialInst){
			if(error) return cb(error,null);
			let mat = materialInst.toJSON();
			mat.name = mat.name[ln];
			mat.materialTypes.forEach(function(value){
				value.name = value.name[ln];
			});

			cb(null,{data:materialInst, msg : msg.getSingleMaterial});
		});
	};

	Material.remoteMethod("getSingleMaterial",{
		accepts : [
			{arg:"req", type:"object", http:{source:"req"}},
			{arg:"materialId", type:"string",required:true, http:{source:"query"}}
		],
		returns : {arg:"success", type:"object"},
		http: {verb:"get"}
	});

	Material.delMaterial = function(materialId, cb){
		Material.findById(materialId,function(error,matInst){
			if(error) return cb(error,null);
			if(!matInst) return cb(new Error("No material found"),null);

			Material.app.models.MaterialType.count({materialId:matInst.id,deleted:{neq:true}},function(errC,count){
				if(errC) return cb(errC,null);
				if(count>0){
					cb(new Error("Keeps material types"),null);
				}else{
					matInst.deleted = true;
					matInst.save(function(err, success){
						if(err) return cb(err,null);
						cb(null,{data:success, msg:msg.delMaterial});
					});		
					
				}				
			});
			
		});
	};

	Material.remoteMethod("delMaterial",{
		accepts : [
			{arg:"materialId",type:"string",required:true, http:{source:"query"}}
		],
		returns : {arg:"success", type:"object"},
		http:{verb:"del"}
	});


	Material.delMatType = function(materialTypeId,cb){
		Material.app.models.MaterialType.findById(materialTypeId,function(error, matTypeInst){
			if(error) return cb(error,null);
			if(!matTypeInst) return cb(new Error("No material Type found"),null);

			Material.app.models.Product.count({deleted:{neq:true},materialTypeId:matTypeInst.id},function(err,count){
				if(err) return cb(err,null);
				if(count>0){
					let msg = `${count} product already exists realted to this category. You can not delete this.`;
					cb(new Error(msg),null);
				}else{
					matTypeInst.deleted = true;
					matTypeInst.save(function(errP,success){
						if(errP) return cb(errP,null);
						cb(null, {data:success, msg:msg.delMatType});
					});
				}
			});

		});
	};

	Material.remoteMethod("delMatType",{
		accepts : [
			{arg:"materialTypeId", type:"string", http:{source:"query"}}
		],
		returns : {arg:"success", type:"object"},
		http:{verb:"del"}
	});

	Material.addMaterialType = function(req, res, cb){
		Material.app.models.Container.imageUpload(req, res, {container:"material-type"}, function(err, success){
			if(err) return cb(err, null);
			let data = JSON.parse(success.data);
			let files = success.files;
			// console.log("name console => ",data.name);
			
			let obj = {
				name       : {
					en : data.name.en.trim(),
					hi : data.name.hi.trim()	
				},
				updatedAt  : new Date()
			};

			// console.log("update obj => ", obj);

			if(files.image){
				obj.image = files.image[0].url;
			}
			// console.log(files);

			Material.findById(data.materialId, function(error, materialInst){
				if(error) return cb(error,null);
				if(!materialInst) return cb(new Error("No material found"),null);
				obj.materialId = materialInst.id;
				obj.subTypes = data.subTypes || [];
				if(!data.materialTypeId){
					obj.createdAt = new Date();
					if(!obj.image || obj.image == "")
						return cb(new Error("No image found"),null);

					obj.deleted = false;
					Material.app.models.MaterialType.create(obj,function(err,typeInst){
						if(error) return cb(error,null);
						cb(null, {data:typeInst, msg:msg.addMaterialType});
					});
				}else{
					Material.app.models.MaterialType.findById(data.materialTypeId, function(err,materialTypeInst){
						if(err) return cb(err,null);
						if(!materialTypeInst) cb(new Error("No material type found"), null);
						materialTypeInst.updateAttributes(obj,function(errU,successU){
							if(errU) return cb(errU,null);
							cb(null,{data:successU, msg:msg.editMaterialType});
						});
					});
				}
			});

			// console.log(success);
			// cb(null, success);
		});
	};

	Material.remoteMethod("addMaterialType",{
		accepts : [
			{arg:"req", type:"object", http:{source:"req"}},
			{arg:"res", type:"object", http:{source:"res"}}
		],
		returns : {arg:"success", type:"object"}

	});




};
