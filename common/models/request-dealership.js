'use strict';
/* jshint node: true ,esversion:6*/
var shortid = require('shortid');
var msg = require("../messages/request-dealer/request-dealer-msg.json");
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;

module.exports = function(Requestdealership) {

	disableAllMethods(Requestdealership, ["find"]);

	Requestdealership.request = function(req,res,cb){
		let creatorId = req.accessToken.userId;

		Requestdealership.app.models.WebUser.findById(creatorId, function(errUser, creatorInst){
			if(errUser) return cb(errUser, null);

			if(creatorInst.realm === 'webuser'){
				Requestdealership.findOne({where:{creatorId:creatorId}}, function(errReq, reqInst){
					if(errReq) return cb(errReq,null);
					if(reqInst) return cb(new Error("Already registered"),null);
					createRequest(creatorInst);
				});
			}else{
				createRequest(creatorInst);
			}	
			    
		});

		function createRequest(creator){
	        Requestdealership.app.models.Container.imageUpload(req, res, {container:"dealership-request"}, function(err, success){
	            if(err) return cb(err, null);

				let data = JSON.parse(success.data);
				let files = success.files;
				
				let obj = data;
				obj.creatorId = creatorId;
				obj.createdAt = new Date();
				obj.updatedAt = new Date();
				obj.regToken = shortid.generate();
				if(files.shopImg){
					obj.shopImg = files.shopImg[0].url;
				}			            
	            // console.log("dealer file=> ",files);
	            // console.log("dealer data", obj);
	            Requestdealership.create(data, function(error,success){
	            	if(error) return cb(error, null);
	            	if(creator.realm== "webuser"){
	            		creator.dealershipDone = true;
	            		creator.save(function(){});
	            		Requestdealership.app.models.Otp.formSubmissionMsg({mobile:obj.contactNo,token:obj.regToken},function(){});
	            	}
	            	cb(null,{data:success,msg:msg.request});
	            });	

	        });   			
		}
	};

	Requestdealership.remoteMethod("request",{
		accepts : [
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"res",type:"object",http:{source:"res"}}
		],
		returns : {arg:"success", type:"object"}
	});


	Requestdealership.getRequestById = function(requestId, cb){
		Requestdealership.findById(requestId,function(error,success){
			if(error) return cb(error,null);
			cb(null, {data:success});
		});
	};

	Requestdealership.remoteMethod("getRequestById",{
		accepts :[
			{arg:"requestId",type:"string", required:true, http:{source:"query"}}
		],
		returns : {arg:"success",type:"object"},
		http:{verb:"get"}
	});

	Requestdealership.getAllForms = function(skip, limit,creatorId, cb){
		skip = skip || 0;
		limit = limit || 10;
		let filter = {
			skip : skip,
			limit : limit,
			order:"createdAt DESC",
			include:"creator"
		};

		if(creatorId){
			filter.where = {
				creatorId:creatorId
			};
		}

		// {skip:skip,limit:limit,order:"createdAt DESC",include:"creator"}
		Requestdealership.find(filter,function(error,success){
			if(error) return cb(error,null);
			Requestdealership.count(filter.where,function(err,count){
				if(err) return cb(err,null);
				cb(null,{data:success,count:count,msg:msg.getAllForms});	
			})
		})
	}

	Requestdealership.remoteMethod("getAllForms",{
		accepts : [
			{arg:"skip",type:"number",http:{source:"query"}},
			{arg:"limit",type:"number",http:{source:"query"}},
			{arg:"creatorId",type:"string",http:{source:"query"}}
		],
		returns : {arg:"success", type:"object"},
		http : {verb:"get"} 
	})


	Requestdealership.getFormById = function(formId, cb){
		Requestdealership.findById(formId,{
			include:"creator"
		},function(error,formInst){
			if(error,null);
			cb(null,{data:formInst,msg:msg.getFormById});
		})
	}

	Requestdealership.remoteMethod("getFormById",{
		accepts : [
			{arg:"formId", type:"string", http:{source:"query"}}
		],
		returns : {arg:"success",type:"object"},
		http : {verb:"get"}
	})


};
