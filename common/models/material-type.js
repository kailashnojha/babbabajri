'use strict';
/* jshint node: true ,esversion:6*/
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
module.exports = function(Materialtype) {
	disableAllMethods(Materialtype, ["find"]);
};