'use strict';
/* jshint node: true ,esversion:6*/
var msg = require("./../messages/material/material-msg.json");
var uf = require("./../services/unit-function.js");
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;

module.exports = function(Subtype) {
	disableAllMethods(Subtype, ["find"]);

	Subtype.addOrUpdate = function(name, subTypeId, cb){
		name.en = (name.en.trim()).toLowerCase();
		name.hi = name.hi.trim();
				
		if(subTypeId){
			Subtype.findById(subTypeId,function(error,subTypeInst){
				if(error) return cb(error,null);
				if(!subTypeInst) return cb(new Error("No sub type found"),null);
				subTypeInst.updateAttributes({
					name : name,
					updatedAt : new Date()
				},function(err,success){
					if(err) return cb(err,null);
					cb(null, {data : success, msg: msg.addOrUpdate});
				});

			});
		}else{
			Subtype.create({
				name       : name,
				deleted    : false,
				createdAt  : new Date(),
				updatedAt  : new Date()
			},function(error,success){
				if(error) return cb(error,null);
				cb(null, {data:success, msg:msg.addOrUpdate});
			});
		}
	};

	Subtype.remoteMethod("addOrUpdate",{
		accepts : [
			{arg:"name", type:"object", http:{source:"form"}},
			{arg:"subTypeId", type:"string", http:{source:"form"}}
		],
		returns : {arg:"success", type:"object"}
	});	

	Subtype.getSubTypeByAdmin = function(cb){
		Subtype.find({where:{deleted:false},order:"createdAt DESC"},function(err, success){
			if(err) return cb(err,null);
			Subtype.count(function(error,count){
				if(error) return cb(error,null);
				cb(null, {data:success,count:count, msg: msg.getSubTypeByAdmin});
			});
		});	
	};

	Subtype.remoteMethod("getSubTypeByAdmin",{
		accepts : [],
		returns : {arg:"success", type:"object"},
		http : {verb:"get"}
	});

	Subtype.getSubTypesObj = function(req, cb){
		let ln = req.accessToken.ln || "en";
		Subtype.find({order:"createdAt DESC"},function(err, success){
			if(err) return cb(err,null);
			let obj = {};
			success.forEach(function(value){
				if(ln == "hi")
					obj[value.name.en] = value.name.hi;
				else
					obj[value.name.en] = value.name.en;
			});
			let unitValue = uf.getAllUnitsByLn(ln); 
			for(let x in unitValue){
				obj[x] = unitValue[x];
			}
			cb(null, {data:obj, msg:msg.getSubTypesObj});
		});
	};

	Subtype.remoteMethod("getSubTypesObj",{
		accepts : [
			{arg:"req",type:"object", http:{source:"req"}}
		],
		returns : {arg:"success", type:"object"},
		http : {verb:"get"}
	});

};
