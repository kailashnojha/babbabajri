'use strict';
/* jshint node: true ,esversion:6*/
var admin = require("firebase-admin");
var serviceAccount = require("../../server/bababajri-firebase.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://babbabazri-72e67.firebaseio.com'
  // databaseURL: 'https://bababajri-1346a.firebaseio.com'
});

var firebaseDatabase = admin.database();

var ref = firebaseDatabase.ref("/Driver");

var notyMsg = require("./../services/notifications-msg");
var msg = require("./../messages/notification/notification-msg.json");

var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;

var jobInclude = [
        {
            relation:"product",
            scope:{
                include:[
                    {
                        relation:"material",
                        scope:{
                            fields:["name"]
                        }
                    },
                    {
                        relation:"materialType",
                        scope:{
                            fields:["name"]
                        }
                    }
                ]
            }
        },
        {
            relation:"driver",
            scope:{
                fields:["firstName","lastName","fullName","mobile","address"]
            }
        },
        {
            relation:"customer",
            scope:{
                fields:["firstName","lastName","fullName","mobile","address"]
            }
        },
        {
            relation:"owner",
            scope:{
                fields:["firstName","lastName","fullName","mobile","address"]
            }
        },        
        {
            relation:"agent",
            scope:{
                fields:["firstName","lastName","fullName","mobile","address"]
            }
        },                
        {
            relation:"material",
            scope:{
                fields:["name"]
            }
        },
        {
            relation:"materialType",
            scope:{
                fields:["name"]
            }
        },
        {
	        relation: "vehicle",
	        scope: {
	          include: {
	            relation: "vehicleType"
	          }
	        }
	    }
    ];

module.exports = function(Notification) {

	disableAllMethods(Notification, ['find']);
	Notification.testMsg = function(cb){
		var registrationToken = "d5E3vypY-w8:APA91bHvhCmzj0t_yaX2bz46KmWHDi-WxFhPqKfSIRGcKfb4lBEfHAxir4fo0lccKcvtI59NgRfhePDBXTpV9DqDEqCR6j5oroTbas5iVlVpXnkb99BqtHogbYoz_-4F4LXX7jrJ7XMcdhw9Y7V9j8lNtpciSyPi7A";
		var message = {
		   notification: {
		    title: '$GOOG up 1.43% on the day',
		    body: '$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.'
		  },
		  data: {
		    score: '850',
		    time: '2:45'
		  },
		  token: registrationToken
		};

		admin.messaging().send(message)
		  .then((response) => {
		    console.log('Successfully sent message:', response);
		  })
		  .catch((error) => {
		    console.log('Error sending message:', error);
		  });
		console.log("working");
		cb();
	};

	Notification.remoteMethod("testMsg",{

	});

    Notification.orderPlace = function( orderRequestId, orderType ) {
    	Notification.app.models.OrderRequest.findById(orderRequestId,{
    		include:jobInclude
    	},function(error,orderRequestInstance){
    		console.log(error);
    		if(error) return;
    		let orderRequestInst = orderRequestInstance.toJSON();
    		// Notification to owner or driver for new job alert
    		let sendTo = orderType == "book_now" ? "driver" : "owner";
		    let obj = notyMsg.orderPlace(orderRequestInst,sendTo);
		    var payload = {
		        data: obj,
		        notification: obj
		    };
		    sendNotification(
		    	orderRequestInst.ownerId,
		    	payload,
		    	{
			    	senderId       : orderRequestInst.customerId,
			    	receiverId     : orderRequestInst.ownerId,
			    	orderRequestId : orderRequestInst.id,
			    	type           : "order_place",
			    	sendTo         : sendTo
		    	}
		    );
    	});
    };


    Notification.confirmOrder = function(orderRequestId){
    	Notification.app.models.OrderRequest.findById(orderRequestId,{
    		include:jobInclude
    	},function(error,orderRequestInstance){
    		if(error) return;


    		let orderRequestInst = orderRequestInstance.toJSON();
    		// Notification to owner or driver for new job alert
    		
    		if(orderRequestInst.requestType == "book_now" || (orderRequestInst.requestType == "book_later" && !orderRequestInst.driverId)){
	    		let sendTo = "customer";
			    let obj = notyMsg.confirmOrder(orderRequestInst,sendTo);
			    var payload = {
			        data: obj,
			        notification: obj
			    };
			    sendNotification(
			    	orderRequestInst.customerId,
			    	payload,
			    	{
				    	senderId       : orderRequestInst.ownerId,
				    	receiverId     : orderRequestInst.customerId,
				    	orderRequestId : orderRequestInst.id,
				    	type           : "order_confirm",
				    	sendTo         : sendTo
			    	}
			    );
    		}

		    if(orderRequestInst.agentId && orderRequestInst.vehicleId && orderRequestInst.driverId){
				let sendToAgent = "agent";
			    let objAgent = notyMsg.agentVerifyRequest(orderRequestInst,sendToAgent);
			    var payloadAgent = {
			        data: objAgent,
			        notification: objAgent
			    };
			    sendNotification(
			    	orderRequestInst.agentId,
			    	payloadAgent,
			    	{
				    	senderId       : orderRequestInst.ownerId,
				    	receiverId     : orderRequestInst.agentId,
				    	orderRequestId : orderRequestInst.id,
				    	type           : "order_verify_request",
				    	sendTo         : sendToAgent
			    	}
			    );		    	
		    }

    	});
    };

    Notification.driverAssign = function(orderRequestId){
    	Notification.app.models.OrderRequest.findById(orderRequestId,{
    		include:jobInclude
    	},function(error,orderRequestInstance){
    		if(error) return;
    		let orderRequestInst = orderRequestInstance.toJSON();
    		// Notification to owner or driver for new job alert
    		let sendTo = "driver";
		    let obj = notyMsg.driverAssign(orderRequestInst,sendTo);
		    var payload = {
		        data: obj,
		        notification: obj
		    };
		    sendNotification(
		    	orderRequestInst.driverId,
		    	payload,
		    	{
			    	senderId       : orderRequestInst.ownerId,
			    	receiverId     : orderRequestInst.driverId,
			    	orderRequestId : orderRequestInst.id,
			    	type           : "driver_assign",
			    	sendTo         : sendTo
		    	}
		    );

    	});
    };    

    Notification.orderCancel = function(orderRequestId){
    	Notification.app.models.OrderRequest.findById(orderRequestId,{
    		include:jobInclude
    	},function(error,orderRequestInstance){
    		if(error) return;
    		let orderRequestInst = orderRequestInstance.toJSON();
    		// Notification to owner or driver for new job alert
    		let sendTo = "customer";
		    let obj = notyMsg.orderCancel(orderRequestInst,sendTo);
		    var payload = {
		        data: obj,
		        notification: obj
		    };
		    sendNotification(
		    	orderRequestInst.customerId,
		    	payload,
		    	{
			    	senderId       : orderRequestInst.ownerId,
			    	receiverId     : orderRequestInst.customerId,
			    	orderRequestId : orderRequestInst.id,
			    	type           : "order_cancelled",
			    	sendTo         : sendTo
		    	}
		    );

    	});
    };    


    Notification.startTrip = function(orderRequestId){
    	Notification.app.models.OrderRequest.findById(orderRequestId,{
    		include:jobInclude
    	},function(error,orderRequestInstance){
    		if(error) return;

    		let orderRequestInst = orderRequestInstance.toJSON();
    		// Notification to owner or driver for new job alert
    		let sendTo = "customer";
		    let obj = notyMsg.startTripCustomer(orderRequestInst,sendTo);
		    var payload = {
		        data: obj,
		        notification: obj
		    };
		    sendNotification(
		    	orderRequestInst.customerId,
		    	payload,
		    	{
			    	senderId       : orderRequestInst.ownerId,
			    	receiverId     : orderRequestInst.customerId,
			    	orderRequestId : orderRequestInst.id,
			    	type           : "order_dispatched_customer",
			    	sendTo         : sendTo
		    	}
		    );

		    if(orderRequestInst.requestType != "book_now"){
		    	let sendToOwner = "owner";
			    let objOwner = notyMsg.startTripOwner(orderRequestInst,sendToOwner);
			    var payloadOwner = {
			        data: objOwner,
			        notification: objOwner
			    };
			    sendNotification(
			    	orderRequestInst.ownerId,
			    	payloadOwner,
			    	{
				    	senderId       : orderRequestInst.driverId,
				    	receiverId     : orderRequestInst.ownerId,
				    	orderRequestId : orderRequestInst.id,
				    	type           : "order_dispatched_owner",
				    	sendTo         : sendToOwner
			    	}
			    );		    		
		    }

		});    
    };


    // Notification.

    Notification.orderVerify = function(orderRequestId){
    	Notification.app.models.OrderRequest.findById(orderRequestId,{
    		include:jobInclude
    	},function(error,orderRequestInstance){
    		if(error) return;
    		let orderRequestInst = orderRequestInstance.toJSON();
    		// Notification to owner or driver for new job alert
    		let sendTo = "driver";
		    let obj = notyMsg.orderVerifyDriver(orderRequestInst,sendTo);
		    var payload = {
		        data: obj,
		        notification: obj
		    };
		    sendNotification(
		    	orderRequestInst.driverId,
		    	payload,
		    	{
			    	senderId       : orderRequestInst.agentId,
			    	receiverId     : orderRequestInst.driverId,
			    	orderRequestId : orderRequestInst.id,
			    	type           : "order_verify_driver",
			    	sendTo         : sendTo
		    	}
		    );

		    if(orderRequestInst.requestType != "book_now"){
				let sendToOwner = "owner";
			    let objOwner = notyMsg.orderVerifyOwner(orderRequestInst,sendToOwner);
			    var payloadOwner = {
			        data: objOwner,
			        notification: objOwner
			    };
			    sendNotification(
			    	orderRequestInst.ownerId,
			    	payloadOwner,
			    	{
				    	senderId       : orderRequestInst.agentId,
				    	receiverId     : orderRequestInst.ownerId,
				    	orderRequestId : orderRequestInst.id,
				    	type           : "order_verify_owner",
				    	sendTo         : sendToOwner
			    	}
			    );		    	
		    }

    	});
    };


    Notification.orderDeliver = function(orderRequestId){
    	Notification.app.models.OrderRequest.findById(orderRequestId,{
    		include:jobInclude
    	},function(error,orderRequestInstance){
    		if(error) return;
    		let orderRequestInst = orderRequestInstance.toJSON();
    		// Notification to owner or driver for new job alert
    		let sendTo = "customer";
		    let obj = notyMsg.orderDeliverCustomer(orderRequestInst,sendTo);
		    var payload = {
		        data: obj,
		        notification: obj
		    };
		    sendNotification(
		    	orderRequestInst.customerId,
		    	payload,
		    	{
			    	senderId       : orderRequestInst.driver,
			    	receiverId     : orderRequestInst.customerId,
			    	orderRequestId : orderRequestInst.id,
			    	type           : "order_deliver_customer",
			    	sendTo         : sendTo
		    	}
		    );

		    if(orderRequestInst.requestType != "book_now"){
				let sendToOwner = "owner";
			    let objOwner = notyMsg.orderDeliverOwner(orderRequestInst,sendToOwner);
			    var payloadOwner = {
			        data: objOwner,
			        notification: objOwner
			    };
			    sendNotification(
			    	orderRequestInst.ownerId,
			    	payloadOwner,
			    	{
				    	senderId       : orderRequestInst.driver,
				    	receiverId     : orderRequestInst.ownerId,
				    	orderRequestId : orderRequestInst.id,
				    	type           : "order_deliver_owner",
				    	sendTo         : sendToOwner
			    	}
			    );		    	
		    }

    	});
    };

    Notification.productDeactive = function(senderId,productId){

    	Notification.app.models.Product.findById(productId,function(error,productInst){
    		if(error) return;
    		// let orderRequestInst = orderRequestInstance.toJSON();
    		// Notification to owner or driver for new job alert

    		let sendTo = "owner";
		    let obj = notyMsg.productDeactivate(productInst,sendTo);
		    var payload = {
		        data: obj,
		        notification: obj
		    };
		    sendNotification(
		    	productInst.ownerId,
		    	payload,
		    	{
			    	senderId       : senderId,
			    	receiverId     : productInst.ownerId,
			    	productId      : productInst.id,
			    	type           : "product_deactivate",
			    	sendTo         : sendTo
		    	}
		    );
    	});
    };

    Notification.productActive = function(senderId,productId){

    	Notification.app.models.Product.findById(productId,function(error,productInst){
    		if(error) return;
    		// let orderRequestInst = orderRequestInstance.toJSON();
    		// Notification to owner or driver for new job alert
    		let sendTo = "owner";
		    let obj = notyMsg.productActivate(productInst,sendTo);
		    var payload = {
		        data: obj,
		        notification: obj
		    };
		    sendNotification(
		    	productInst.ownerId,
		    	payload,
		    	{
			    	senderId       : senderId,
			    	receiverId     : productInst.ownerId,
			    	productId      : productInst.id,
			    	type           : "product_activate",
			    	sendTo         : sendTo
		    	}
		    );
    	});
    };

    // All Notification type

	/*
		order_place
		order_confirm
		order_verify_request
		driver_assign
		order_cancelled
		order_dispatched_customer
		order_dispatched_owner
		order_verify_driver
		order_verify_owner
		order_deliver_customer
		order_deliver_owner
	*/


    Notification.getNotificationList = function(req,cb){
    	let peopleId = req.accessToken.userId;
    	let ln = req.headers.ln;
    	ln = ln == "hi" ? "hi" : "en";
    	Notification.app.models.People.findById(peopleId,function(err,userInst){
    		if(err) return cb(err,null);
    		let badge = userInst.newNotifications || 0;
	    	Notification.find({
	    		where:{
	    			receiverId:peopleId
	    		},
	    		include:[{
	    			relation:"orderRequest",
	    			scope:{
	    				include:jobInclude
	    			}
	    		},{
	    			relation:"product"
	    		}],
	    		order:"createdAt DESC"
	    	},function(error,success){
	    		if(error) return cb(error,null);

	    		let notyArr = [];
	    		success.forEach(function(value){
	    			let data = value.toJSON();
		
					if(data.type == 'order_place'){
						notyArr.push(getSingleValueOfNoty(notyMsg.orderPlace(data.orderRequest,data.sendTo),ln));
					}else if(data.type == 'order_confirm'){
						notyArr.push(getSingleValueOfNoty(notyMsg.confirmOrder(data.orderRequest,data.sendTo),ln));
					}else if(data.type == 'driver_assign'){
						notyArr.push(getSingleValueOfNoty(notyMsg.driverAssign(data.orderRequest,data.sendTo),ln));
					}else if(data.type == 'order_verify_request'){
						notyArr.push(getSingleValueOfNoty(notyMsg.agentVerifyRequest(data.orderRequest,data.sendTo),ln));
					}else if(data.type == 'order_cancelled'){
						notyArr.push(getSingleValueOfNoty(notyMsg.orderCancel(data.orderRequest,data.sendTo),ln));
					}else if(data.type == 'order_dispatched_customer'){
						notyArr.push(getSingleValueOfNoty(notyMsg.startTripCustomer(data.orderRequest,data.sendTo),ln));
					}else if(data.type == 'order_dispatched_owner'){
						notyArr.push(getSingleValueOfNoty(notyMsg.startTripOwner(data.orderRequest,data.sendTo),ln));
					}else if(data.type == 'order_verify_driver'){
						notyArr.push(getSingleValueOfNoty(notyMsg.orderVerifyDriver(data.orderRequest,data.sendTo),ln));
					}else if(data.type == 'order_verify_owner'){
						notyArr.push(getSingleValueOfNoty(notyMsg.orderVerifyOwner(data.orderRequest,data.sendTo),ln));
					}else if(data.type == 'order_deliver_customer'){
						notyArr.push(getSingleValueOfNoty(notyMsg.orderDeliverCustomer(data.orderRequest,data.sendTo),ln));
					}else if(data.type == 'order_deliver_owner'){
						notyArr.push(getSingleValueOfNoty(notyMsg.orderDeliverOwner(data.orderRequest,data.sendTo),ln));	
					}else if(data.type == 'product_deactivate'){
						notyArr.push(getSingleValueOfNoty(notyMsg.productDeactivate(data.product,data.sendTo),ln));	
					}else if(data.type == 'product_activate'){
						notyArr.push(getSingleValueOfNoty(notyMsg.productActivate(data.product,data.sendTo),ln));	
					}	
					notyArr[notyArr.length-1].createdAt = data.createdAt;
	    		});

	    		cb(null,{data:notyArr,badge:badge,msg:msg.getNotificationList});
	    	});
		});	
    };

    function getSingleValueOfNoty(obj,ln){
    	obj.title = obj.title[ln];
    	obj.body = obj.body[ln];
    	return obj;
    }

    Notification.remoteMethod("getNotificationList",{
    	accepts : [
    		{arg:"req",type:"object",http:{source:"req"}}
    	],
    	returns : {arg:"success",type:"object"},
    	http:{verb:"get"}
    });


    Notification.readAllNotifications = function(req,cb){
    	let peopleId = req.accessToken.userId;

    	Notification.app.models.People.findById(peopleId,function(error,peopleInst){
    		if(error) return cb(error,null);
    		peopleInst.newNotifications = 0;
    		peopleInst.save(function(err,user){
    			if(err) return cb(err,null);
    			cb(null,{data:user,msg:msg.readAllNotifications});	
    		})
    		
    	});
    };

    Notification.remoteMethod("readAllNotifications",{
    	accepts : {arg:"req",type:"object",http:{source:"req"}},
    	returns : {arg:"success",type:"object"}
    });

    Notification.getBadgeCount = function(req,cb){
    	let peopleId = req.accessToken.userId;
    	Notification.app.models.People.findById(peopleId,function(error,peopleInst){
    		if(error) return cb(error,null);
    		peopleInst.newNotifications = peopleInst.newNotifications || 0;
    		cb(null,{data:{badgeCount:peopleInst.newNotifications},msg:msg.getBadgeCount});
    	});
    };

    Notification.remoteMethod("getBadgeCount",{
    	accepts:{arg:"req",type:"object",http:{source:"req"}},
    	returns : {arg:"success",type:"object"},
    	http:{verb:"get"}
    });


  	function sendNotification(peopleId,payload,saveNotyObj){

  		saveNotification(saveNotyObj);

	    Notification.app.models.People.findById(peopleId,{
	    	include: {
        		relation: "accessTokens"
      		}
      	}, function(error, peopleInstance) {
	        if (error) return;
	        let peopleInst = peopleInstance.toJSON();
	       	let firebaseTokens = [];
	       	// console.log(peopleInst);
	       	peopleInst.newNotifications = peopleInst.newNotifications || 0;
	       	payload.notification.badge = peopleInst.newNotifications.toString();
	       	payload.data.badge = peopleInst.newNotifications.toString();
	       	let ln; 
	       	peopleInst.accessTokens.forEach(function(token){
	       		if(token.firebaseToken){
	       			ln = token.ln; 
	       			firebaseTokens.push(token.firebaseToken);		
	       		}
	       	});

	       	ln = ln == "hi" ? "hi" : "en";
	       	if(ln == "hi"){
	       		payload.data.title = payload.data.title.hi || "";
		       	payload.data.body = payload.data.body.hi || "";
		   	}else{
	       		payload.data.title = payload.data.title.en || "";
		       	payload.data.body = payload.data.body.en || "";
		   	}
	       	
	        console.log(payload);	
	        updateNotyCount(peopleInstance);

	        if(!firebaseTokens.length)
	        	return;

       	  	admin.messaging().sendToDevice(firebaseTokens,payload)
		    .then(function(response) {
		        console.log(response.results);
		    })
		    .catch(function(error) {
		        console.log(error);
		    });
		});       

  	}

  	function saveNotification(obj){
  		obj.createdAt = new Date();
  		obj.updatedAt = new Date();
        Notification.create(obj,function(){});
  	}

    function updateNotyCount(peopleInst){
		if(!peopleInst.newNotifications) 
		   	peopleInst.newNotifications = 0;
		peopleInst.newNotifications += 1;
		peopleInst.save(function(){});	 	 
	}


   // On firebase data change update user data

    ref.on("child_changed", function(snapshot) {
	    let key = snapshot.key;
	    let value = snapshot.val();
	    console.log(value);
	    Notification.app.models.People.findById(key,function(error,driverInst){
	    	if(driverInst){
	    		driverInst.location = {
	    			lat : value.lat,
	    			lng:value.lng
	    		};

	    		driverInst.save(function(){});
	    	}	

	    });


	}, function (errorObject) {});



};
