'use strict';
/* jshint node: true ,esversion:6*/
var msg = require("./../messages/VehicleType/vehicle-type-msg.json");
var CustomError = require("./../../common/services/custom-error");
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var unitFunction = require("./../services/unit-function.js");
module.exports = function(Vehicletype) {

	disableAllMethods(Vehicletype, ["find"]);
 
	Vehicletype.addType = function(req, res, cb){
		Vehicletype.app.models.Container.imageUpload(req, res, { container: 'VehicleTypeImg' }, function(err, success) {
	        if (err)
	          cb(err, null);
	        else if (success.data) {
	            success.data = JSON.parse(success.data);
	            createType(success);
	        } else {
	            cb(new CustomError("data not found"), null);
	        }
	    });

		function createType(obj){
			let vehicleTypeId = req.query.vehicleTypeId;
			
		    var data = {
		        name      : obj.data.name,
		        updatedAt : new Date()
		    };
  
	        if (obj.files) {
		        if (obj.files.image)
		          data.image = obj.files.image[0].url;

		      	if(obj.files.clickImage)
		      		data.clickImage = obj.files.clickImage[0].url;
		      	
		      	if(obj.files.driveImage)
		      		data.driveImage = obj.files.driveImage[0].url;
	        }

	        if(!obj.data.loadValue || !obj.data.loadValue.length){
	        	return cb(new Error("Please provide minimum 1 load value"),null);
	        }else if(!unitFunction.validLoadValue(obj.data.loadValue)){
	        	return cb(new Error("value not right"),null);
	        }
	        data.loadValue = obj.data.loadValue;
		    if(!vehicleTypeId){

		    	if(!data.image){
		    		return cb(new Error("No image found"),null);
		    	}

		    	data.createdAt = new Date();
		        Vehicletype.create(data, function(err, success) {
			        if (err) return cb(err, null);
			        cb(null,{data:success,msg:msg.addType});
		        });
		    }
		    else{
		        Vehicletype.findById(vehicleTypeId, function(err, vehicleTypeInst) {
		   		    if (err) return cb(err, null);
		   		    delete data.name;
		        	vehicleTypeInst.updateAttributes(data, function(err, success) {
				        if (err)
				          cb(err, null);
				        else
				           cb(null,{data:success,msg:msg.editType});
				    });
			    });
		    }
		}
	};

	Vehicletype.remoteMethod("addType",{
		accepts : [
			{arg : "req", type:"object", http:{source : "req"}},
			{arg : "res", type:"object", http:{source : "res"}}
		],
		returns : {arg: "success", type: "object"}
	});


	Vehicletype.getTypeById = function(req,vehicleTypeId, cb){
		let ln = req.accessToken.ln || "en";
		Vehicletype.findById(vehicleTypeId,function(error,vehicleTypeInst){
			if(error) return cb(error,null);

			vehicleTypeInst.name = vehicleTypeInst.name[ln];

			cb(null, {data:vehicleTypeInst, msg:msg.getTypeById});
		});
	};

	Vehicletype.remoteMethod("getTypeById",{
		accepts : [
			{arg:"req", type:"object", http:{source: "req"}},
			{arg:"vehicleTypeId",type:"string",required:true, http:{source: "query"}}
		],
		returns : {arg:"success", type:"object"},
		http:{verb:"get"}
	});


	Vehicletype.editType = function(req, res,cb){
		if(!req.query.vehicleTypeId) return cb(new CustomError("No id found"),null);
		Vehicletype.addType(req,res,cb);
	};

	Vehicletype.remoteMethod("editType",{
		accepts : [
			{arg:"req", type:"object", http:{source:"req"}},
			{arg:"res", type:"object", http:{source:"res"}}
		],
		returns : {arg:"success",type:"object"}
	});

	Vehicletype.getTypes = function(req, cb){
		let ln = req.accessToken.ln || "en";
		Vehicletype.find(function(error,success){
			if(error) return cb(error,null);

			success.forEach(function(value){
				value.name = value.name[ln];
			});

			cb(null,{ data:success, msg:msg.getTypes });
		});
	};

	Vehicletype.remoteMethod("getTypes",{
		accepts :[
			{arg:"req", type:"object", http:{source:"req"}}
		],
		returns : {arg:"success",type:"object"},
		http:{verb:"get"}
	});

	Vehicletype.getTypesByEveryone = function(ln, cb){
		ln = ln == "hi" ? "hi" : "en";	
		Vehicletype.find(function(error,success){
			if(error) return cb(error,null);

			success.forEach(function(value){
				value.name = value.name[ln];
			});

			cb(null,{ data:success, msg:msg.getTypes });
		});
	};

	Vehicletype.remoteMethod("getTypesByEveryone",{
		accepts :[
			{arg:"ln", type:"string", http:{source:"query"}}
		],
		returns : {arg:"success",type:"object"},
		http:{verb:"get"}
	});


	Vehicletype.getByUnitQty = function(req, quantity, unit, cb){
		let ln = req.accessToken.ln || "en";
		Vehicletype.find(function( error, vehicleTypes ){
			if(error) return cb(error,null);

			let makeUnit  = unitFunction.makeUnitKey(unit);
			let vehicleList = [];
			console.log(makeUnit);
			vehicleTypes.forEach(function(value){
				let isAvailable  = false;
				value.loadValue.forEach(function(loadValue){
					loadValue[makeUnit] = loadValue[makeUnit] || 0;
					if(quantity < loadValue[makeUnit]){
						isAvailable = true;
					}
				});


				if(isAvailable){
					value.name = value.name[ln];
					vehicleList.push(value);	
				}

			});


			cb(null, { data:vehicleList, msg:msg.getByUnitQty });
		});
	};

	Vehicletype.remoteMethod("getByUnitQty",{
		accepts : [
			{arg:"req", type:"object", http:{source:"req"}},
			{arg:"quantity", type:"number", required:true, http:{source:"query"}},
			{arg:"unit", type:"string", required:true, http:{source:"query"}},
		],
		returns : {arg:"success",type:"object"},
		http: {verb:"get"}
	});

	Vehicletype.addImage = function(vehicleTypeId,url, cb){

		Vehicletype.findById(vehicleTypeId,function(error,vehicleType){
			if(error) return cb(error,null);
			if(!vehicleType) return cb(new Error("No vehicle type"), null);
			vehicleType.driveImage = url;
			vehicleType.save(function(err,data){
				if(err) return cb(err,null);
				cb(null,{data:data,msg:msg.addImage});
			});
			
		});
	};

	Vehicletype.remoteMethod("addImage",{
		accepts : [
			{arg:"vehicleTypeId", type:"string", http:{source:"form"}},
			{arg:"url", type:"string", http:{source:"form"}},
		],
		returns : {arg:"success", type:"object"}
	});

};
