'use strict';
/* jshint node: true ,esversion:6*/
var msg = require("../messages/request-employment/request-employment-msg.json");
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var shortid = require('shortid');
module.exports = function(Requestemployment) {

	disableAllMethods(Requestemployment, ["find"]);

	Requestemployment.request = function(req,res,cb){
		let creatorId = req.accessToken.userId;
		Requestemployment.app.models.WebUser.findById(creatorId, function(errUser, creatorInst){
			if(errUser) return cb(errUser, null);

			if(creatorInst.realm === 'webuser'){
				Requestemployment.findOne({where:{creatorId:creatorId}}, function(errReq, reqInst){
					if(errReq) return cb(errReq,null);
					if(reqInst) return cb(new Error("Already registered"),null);
					createRequest(creatorInst);
				});
			}else{
				createRequest(creatorInst);
			}	
			    
		});

		function createRequest(creator){
			Requestemployment.app.models.Container.imageUpload(req, res, {container:"employment-request"}, function(err, success){
	            if(err) return cb(err, null);
				
				let data = JSON.parse(success.data);
				let files = success.files;
				
				let obj = data;
				obj.creatorId = creatorId;
				obj.createdAt = new Date();
				obj.updatedAt = new Date();
				obj.regToken = shortid.generate();
				if(files.signImg){
					obj.signImg = files.signImg[0].url;
				}			            

				if(files.profilePic){
					obj.profilePic = files.profilePic[0].url;
				}			            
	            
	            Requestemployment.create(data,function(error,success){
	            	if(error) return cb(error,null);
	            	if(creator.realm== "webuser"){
	            		creator.employmentDone = true;
	            		creator.save(function(){});
	            	}
	            	Requestemployment.app.models.Otp.formSubmissionMsg({mobile:obj.contactNo,token:obj.regToken},function(){});
	            	cb(null,{data:success,msg:msg.request});

	            });	
	        });   
		}
	    
	};

	Requestemployment.remoteMethod("request",{
		accepts : [
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"res",type:"object",http:{source:"res"}}
		],
		returns : {arg:"success", type:"object"}
	});

	Requestemployment.getRequestById = function(requestId, cb){
		Requestemployment.findById(requestId,function(error,success){
			if(error) return cb(error,null);
			cb(null, {data:success});
		});
	};

	Requestemployment.remoteMethod("getRequestById",{
		accepts :[
			{arg:"requestId",type:"string", required:true, http:{source:"query"}}
		],
		returns : {arg:"success",type:"object"},
		http:{verb:"get"}
	});

	Requestemployment.getAllForms = function(skip, limit, creatorId, cb){
		skip = skip || 0;
		limit = limit || 10;
		let filter = {
			skip:skip,
			limit:limit,
			order:"createdAt DESC",
			include:"creator"
		};
		filter.where = {
			creatorId : creatorId
		};


		Requestemployment.find(filter,function(error,success){
			if(error) return cb(error,null);
			Requestemployment.count(filter.where,function(err, count){
				if(err) return cb(err,null);
				cb(null,{data:success, count:count, msg:msg.getAllForms});	
			});
		});
	};

	Requestemployment.remoteMethod("getAllForms",{
		accepts : [
			{arg:"skip",type:"number",http:{source:"query"}},
			{arg:"limit",type:"number",http:{source:"query"}},
			{arg:"creatorId",type:"string",http:{source:"query"}}
		],
		returns : {arg:"success", type:"object"},
		http : {verb:"get"} 
	});


	Requestemployment.getFormById = function(formId, cb){
		Requestemployment.findById(formId,{
			include:"creator"
		},function(error,formInst){
			if(error,null);
			cb(null,{data:formInst,msg:msg.getFormById});
		})
	};

	Requestemployment.remoteMethod("getFormById",{
		accepts : [
			{arg:"formId", type:"string", http:{source:"query"}}
		],
		returns : {arg:"success",type:"object"},
		http : {verb:"get"}
	});

	

};
