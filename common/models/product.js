'use strict';
/* jshint node: true ,esversion:6*/   
var loopback = require('loopback');
var msg = require("../messages/product/product-msg.json");
var CustomError = require("./../../common/services/custom-error");
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var gf = require("./../services/global-function.js");
var changeLN = gf.changeLN;
var applyFilter = require('loopback-filters');
module.exports = function(Product) {
    disableAllMethods(Product, ['find']);

    Product.addProduct = function(req, res, cb){
        let ownerId = req.accessToken.userId;
        Product.app.models.Container.imageUpload(req, res, {container:"product-image"}, function(err, success){
            if(err) return cb(err, null);
            let data = JSON.parse(success.data);
            let files = success.files;

            let productData = {
                ownerId        : ownerId,
                materialId     : data.materialId,
                name           : data.name,
                materialTypeId : data.materialTypeId,
                location       : data.location,
                address        : data.address,
                unit           : data.unit,
                subType        : data.subType, 
                price          : data.price,
                createdAt      : new Date(),
                updatedAt      : new Date(),
                deleted        : false
            };
            
            if(files.image){
                productData.image = files.image[0].url;
            }

            Product.create(productData, function(error, productInst){
                if(error){
                    return cb(error, null);
                }else{
                    cb(null, {data : productInst, msg:msg.addProduct});
                }
            });            

        });    
    };

    Product.remoteMethod("addProduct",{
        accepts : [
            {arg:"req", type:"object", http:{source:"req"}},
            {arg:"res", type:"object", http:{source:"res"}}
        ],
        returns : {arg:"success", type:"object"}
    });

    Product.actDeactProductByAdmin = function(req, productId, status, cb){

        if(!["active","deactive"].includes(status)) return cb(new Error("Invalid status"),null);

        let peopleId = req.accessToken.userId;
        Product.findById(productId, function(errorP, productInst){
            if(errorP) return cb(errorP,null);
            if(!productInst) return cb(new Error("No product found"),null);
            productInst.status = status;
            // productInst.deletedById = peopleId;
            productInst.save(function(error, success){
                if(error) return cb(error, null);
                if(success.status == "active")
                    Product.app.models.Notification.productActive(peopleId, success.id);
                else
                    Product.app.models.Notification.productDeactive(peopleId, success.id);

                cb(null, {data:success, msg: msg.deleteProdByAdmin});
            });
        });
    };

    Product.remoteMethod("actDeactProductByAdmin",{
        accepts : [
            {arg:"req", type:"object", http: {source:"req"}},
            {arg:"productId", type:"string",required:true, http: {source:"form"}},
            {arg:"status", type:"string", required:true, http: {source:"form"}}
        ],
        returns : {arg:"success", type:"object"},
        http : {verb : "post"}
    });

    Product.updateProduct = function(req, productId, price, cb){
        let peopleId = req.accessToken.userId;
        Product.findById(productId, function(error, productInst){
            if(error) return cb(error,null);
            if(productInst.ownerId.toString() != peopleId.toString()) 
                return cb(new Error("Unauthorized person"),null);
            if(price<0) return cb(new Error("Invalid price"),null);
            if(!productInst) return cb(new Error("No product found"),null);

            productInst.price = price;
            productInst.save(function(errorU, success){
                if(errorU) return cb(errorU, null);
                cb(null, {data:success, msg:msg.updateProduct});
            });
        });
    };

    Product.remoteMethod("updateProduct",{
        accepts : [
            {arg:"req", type:"object", http:{source:"req"}},
            {arg:"productId", type:"string", required:true, http:{source:"form"}},
            {arg:"price", type:"number", required:true, http:{source:"form"}}
        ],
        returns : {arg:"success", type:"object"}
    });


    Product.deleteProd = function(req, productId, cb){
        let ownerId = req.accessToken.userId;
        Product.findById(productId,function(err,productInst){
            if(err) return cb(err,null);
            if(!productInst) return cb(new Error("No product found"),null);
            productInst.deleted = true;
            productInst.save(function(error,success){
                if(error) return cb(error,null);
                cb(null,{data:success,msg:msg.deleteProd});
            });
        });
    };

    Product.remoteMethod("deleteProd",{
        accepts : [
            {arg:"req", type:"object", http:{source:"req"}},
            {arg:"productId", type:"string", http:{source:"form"}},
        ],
        returns : {arg:"success",type:"object"}
    });

    Product.getMyProduct = function(req,cb){
        /*console.log("header finding");
        console.log(req.headers);*/
        let ln = req.accessToken.ln || "en";
        let peopleId = req.accessToken.userId;
        let filter = {
            "order":"createdAt DESC",
            where:{ownerId:peopleId,deleted:{neq:true}},
            include:[
                {
                    relation : "materialType",

                },
                {
                    relation : "material",
                    scope:{
                        fields:{name:true,createdAt:true,updatedAt:true}
                    }
                }
            ]
        };
        Product.find(filter,function(error,myProducts){
            if(error) return cb(error,null);

            let arr = [];
            myProducts.forEach(function(pro){
                let value = pro.toJSON();

                arr.push(changeLN(value,ln));
            });

            cb(null,{data:arr,msg:msg.getMyProduct});
        });
    };

    Product.remoteMethod("getMyProduct",{
        accepts : [
            { arg:"req", type:"object", http:{source:"req"} }
        ],
        returns : {arg:"success", type:"object"},
        http : {verb:"get"}
    });

    Product.getOwnerProductByAdmin = function(peopleId,cb){
        /*console.log("header finding");
        console.log(req.headers);*/
        // console.log(peopleId);
        let ln =  "en";
        let filter = {
            "order":"createdAt DESC",
            where:{ownerId:peopleId,deleted:{neq:true}},
            include:[
                {
                    relation : "materialType",
                },
                {
                    relation : "material",
                    scope:{
                        fields:{name:true,createdAt:true,updatedAt:true}
                    }
                }
            ]
        };
        Product.find(filter,function(error,myProducts){
            if(error) return cb(error,null);

            let arr = [];
            myProducts.forEach(function(pro){
                let value = pro.toJSON();

                arr.push(changeLN(value,ln));
            });

            cb(null,{data:arr,msg:msg.getOwnerProductByAdmin});
        });
    };

    Product.remoteMethod("getOwnerProductByAdmin",{
        accepts : [
            {arg:"peopleId", type:"string", required:true, http:{source:"query"}}
        ],
        returns : {arg:"success", type:"object"},
        http : {verb:"get"}
    });


    Product.getProduct = function(req, materialId, location, materialTypeId, cb){ 
        let ln = req.accessToken.ln || "en";
        var filter = {
            "order":"createdAt DESC",
            where : {
                materialId : materialId, 
                materialTypeId:materialTypeId, 
                deleted:{neq:true},
                status:{neq:"deactive"}
            }
        };
        
        /*if(location){
            // location = new loopback.GeoPoint(location);        
            filter.where.location = {near : location, maxDistance : 200, unit: 'kilometers'};
        }*/
        filter.include = [{
            relation:"material",
            scope:{fields: ['name',"units"]}
        },{
            relation:"materialType",
            scope:{fields: ['name']}
        }];
        
        Product.find(filter, function(err, products){
            if(err){
                return cb(err, null);
            }
                
            let arr = [];
            products.forEach(function(pro){
                let value = pro.toJSON();

                // if(typeof pro.toJSON == "function"){
                //     value = pro.toJSON();
                // }else{
                //     value = pro;
                // }    
                arr.push(changeLN(value,ln));
                
            });
            
            if(location){
               arr = applyFilter(arr, {where: {location: {near : location, maxDistance : 200, unit: 'kilometers'}}});     
            }

            cb(null , {data:arr, msg:msg.getProduct});
        });
    };

    Product.remoteMethod('getProduct', {
        accepts : [
            {arg : "req", type:"object", http:{source:"req"}},
            {arg : "materialId", type:"string"},
            {arg : "location", type: "GeoPoint"},
            {arg : "materialTypeId", type: "string"}
        ],
        returns :[
            {arg : "success", type :"object"}
        ],
        http:{verb:"get"}
    });

    Product.trendingProduct = function(req,cb){
        let ln = req.accessToken.ln || "en";
        Product.app.models.MaterialType.find({
            order : 'timesSold DESC',
            where:{deleted:{neq:true}},
            include:"material"
        }, function(error, productMaterial){
            if(error){
                return cb(error, null);
            }else{
                let arr = [];

                productMaterial.forEach(function(value){
                    let matType = value.toJSON();
                    if(matType.material.deleted != true){
                        matType.name = matType.name[ln];
                        arr.push(changeLN(matType,ln));
                    }
                });

                cb(null, {data : arr, msg: msg.trendingProduct});
            }
        });
    };

    Product.remoteMethod('trendingProduct', {
        accepts : [
            {arg:"req", type:"object", http:{source:"req"}}
        ],
        returns : [
            {arg : "success", type : "object"}
        ],
        http : {verb : "get"}
    });

};
