'use strict';
/* jshint node: true ,esversion:6*/
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
module.exports = function(Setting) {

    disableAllMethods(Setting, ["find"]);

	Setting.updateCount = function(cb){
		Setting.findOne({where:{type:"counter"}},function(err,success){
			if(err) return cb(err,null);

			if(!success){
				Setting.create({
					type:"counter",
					count:1,
				},function(error,counter){
					if(error) return cb(error,null);
					cb(null, {data:counter});
				});
			}else{
				success.count += 1;
				success.save(function(error,counter){
					if(error) return cb(error,null);
					cb(null, {data:counter});
				})
			}
		});	
	};

	Setting.remoteMethod("updateCount",{
		returns : {arg:"success", type:"object"}
	});

};
