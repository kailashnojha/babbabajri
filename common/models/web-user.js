'use strict';
/* jshint node: true ,esversion:6*/

var async = require("async");
var msg = require("../messages/web-user/web-user-msg.json");
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var CustomError = require("./../../common/services/custom-error");
var path = require("path");
var gf = require("../services/global-function");
module.exports = function(Webuser) {
	disableAllMethods(Webuser, ['find', "login", "logout", "confirm", "changePassword", "resetPassword", "setPassword"]);

	Webuser.validatesUniquenessOf('email', {
        message: 'email already exists'
    });

	Webuser.prototype.addFirebaseToken = function(token, ln, cb) {
	    let self = this;
	    if (typeof ln == 'function') {
	      cb = ln;
	      ln = "en";
	    }
	    cb(null, self);
	};

	Webuser.signup = function(req, res, cb){
		let ln = req.params.ln == "hi" ? "hi" : "en";
		Webuser.app.models.Container.imageUpload(req, res, { container: 'UserDocuments' }, function(err, success) {
	        if (err)
	        	cb(err, null);
	        else if (success.data) {
	        	success.data = JSON.parse(success.data);

				let obj = {
					realm              : success.data.realm,
					firstName          : success.data.firstName,
					lastName           : success.data.lastName,
					email              : success.data.email,
					mobile             : success.data.mobile,
					shopAddress        : success.data.shopAddress,
					permanentAddress   : success.data.permanentAddress,
					password           : success.data.password,
					createdAt          : new Date(),
					updatedAt          : new Date(),
					emailVerified      : false,
					mobileVerified     : false,
					employmentDone     : false,
					vehicleDone        : false,
					dealershipDone     : false
				};

				// console.log(success);
				if(obj.realm == "emitra")
					obj.adminApproval = "pending";
				else
					obj.adminApproval = "approved";

			    obj.signupOtp = {
			      createdAt  : new Date(),
			      expireAt   : new Date(),
			      otp        : gf.getLongOtp()
			    };


			    obj.signupOtp.expireAt.setMinutes(obj.signupOtp.expireAt.getMinutes() + 5);


				if (success.files) {
		            if (success.files.adharImg){
			            obj.adharImg = success.files.adharImg[0].url;
			            /*obj.files.profileImage.forEach(function(value){
			              peopleInst.profileImage.push(value.url);
			            });*/
		            }
		        }

				Webuser.create(obj,function(error,success){
					if(error) return cb(error,null);
					cb(null,{data:success,msg:msg.signup});
				});
			}
		});		
	};

	function AfterSignup(context, instance, nextUser){
	    var userInstance = instance.success.data;
	    var options = {
	      type: 'email',
	      to: userInstance.email,
	      from: 'noreply@loopback.com',
	      subject: 'Thanks for registering.',
	      redirect: '/verified',
	      user: Webuser,
	      template: path.resolve(__dirname, '../../server/views/verify.ejs'),
	    };

	    if(typeof userInstance.signupOtp == 'object' && typeof userInstance.signupOtp.otp != undefined){
	      Webuser.app.models.Otp.sendSMS({
	        otp:userInstance.signupOtp.otp,
	        mobile:userInstance.mobile
	      },function(){});
	    }

	    userInstance.addRole(userInstance.realm, function(error, success) {
	      if(error) return nextUser(error);
	      // nextUser();
	      /*if (!userInstance.emailVerified) {
	        userInstance.verify(options, function(err, response, next) {
	          if (err) return nextUser(err);
	          nextUser();
	        });
	      } else {*/
	        nextUser();
	      // }
	    });
	}

	Webuser.remoteMethod("signup",{
		accepts : [
			{arg:"req", type:"object",http:{source:"req"}},
			{arg:"res", type:"object",http:{source:"res"}},
			/*{arg:"realm", type:"string",http:{source:"form"}},
			{arg:"firstName", type:"string",http:{source:"form"}},
			{arg:"lastName", type:"string",http:{source:"form"}},
			{arg:"email", type:"string",http:{source:"form"}},
			{arg:"mobile", type:"string",http:{source:"form"}},
			{arg:"shopAddress", type:"object",http:{source:"form"}},
			{arg:"permanentAddress", type:"object",http:{source:"form"}},
			{arg:"password", type:"string",http:{source:"form"}}*/
		],
		returns : {arg:"success", type:"object"}
	});
	Webuser.afterRemote('signup', AfterSignup);

	Webuser.getMyInfo = function(req, cb){
		let webUserId = req.accessToken.userId;

		Webuser.findById(webUserId,function(err,success){
			if(err) return cb(err,null);
			cb(null,{data:success,msg:msg.getMyInfo});
		});
	};

	Webuser.remoteMethod("getMyInfo",{
		accepts : [
			{arg:"req", type:"object", http:{source:"req"}}
		],
		returns : {arg:"success", type:"object"}
	});

	Webuser.getAllEmitra = function(cb){
		Webuser.find({where:{realm:"emitra"},order:"createdAt DESC"},function(error, success){
			if(error) return cb(error,null);
			cb(null, {data:success,msg:msg.getAllEmitra});
		});
	};

	Webuser.remoteMethod("getAllEmitra",{
		// accepts : []
		returns : {arg:"success", type:"object"},
		http:{verb:"get"}
	});


	Webuser.prototype.addRole = function(role, cb) {
	    let self = this;
	    Webuser.app.models.Role.findOne({ where: { name: role } }, function(error, roleInst) {
	      if (error)
	        cb(error, null);
	      else {
	        if (roleInst) {
	          function createRole() {
	            Webuser.app.models.RoleMapping.destroyAll({ principalId: self.id }, function(err, success) {
	              if (err)
	                return cb(err, null);

	              roleInst.principals.create({
	                principalType: Webuser.app.models.RoleMapping.USER,
	                principalId: self.id
	              }, function(err, principal) {
	                if (err) return cb(err, null);
	                cb(null, principal);
	                console.log("role created => ",roleInst.name);
	              });
	            });
	          }
	          createRole();
	        } else {
	          cb(new CustomError("No role found"), null);
	        }
	      }
	    });
	};

	Webuser.getAllRequests = function(req, cb){
		let userId = req.accessToken.userId;

		let tasks = [];

		tasks.push(function(cbLocal){
			Webuser.app.models.RequestDealership.find({where:{creatorId:userId}},cbLocal);	
		});
		tasks.push(function(cbLocal){
			Webuser.app.models.RequestEmployment.find({where:{creatorId:userId}},cbLocal);	
		});
		tasks.push(function(cbLocal){
			Webuser.app.models.RequestVehicle.find({where:{creatorId:userId}},cbLocal);	
		});

		async.parallel(tasks,function(error,result){
   	        if(error) return cb(error,null);

	        let data = {
		        dealerships : result[0],
		        employments : result[1],
		        vehicles    :result[2]
	        };

        	//return cb(null,result);
        	return cb(null,{data:data});
    	}); 
	};

	Webuser.remoteMethod("getAllRequests",{
		accepts : [
			{arg:"req", type:"object", http:{source:"req"}}
		],
		returns : {arg:"success", type:"object"},
		http:{verb:"get"}
	});


	Webuser.getAllUsers = function(status, type, searchStr, skip, limit, cb){
	    type = type || "owner"; // [user, owner, driver]
	    let filter = {
	      where:{
	        realm         : type,
	        searchStr     : searchStr,
	        adminApproval : status
	      }, 
	      skip      : skip, 
	      limit     : limit
	    };
	    Webuser.find(filter, function(error,success){
	      if(error) return cb(error,null);
	      Webuser.count(filter.where,function(err,count){
	      	if(err) return cb(err,null);
	      	cb(null,{data:success,count:count,msg:msg.getAllUsers});
	      });
	      
	    });

	};

    Webuser.remoteMethod("getAllUsers",{
	    accepts : [
	      {arg:"status", type:"string",http:{source:"query"}},
	      {arg:"type", type:"string",http:{source:"query"}},
	      {arg:"searchStr", type:"string",http:{source:"query"}},
	      {arg:"skip", type:"string",http:{source:"query"}},
	      {arg:"limit", type:"string",http:{source:"query"}},
	    ],
	    returns : {arg:"success", type:"object"},
	    http:{verb:"get"}
	});


    Webuser.activeDeactive = function(peopleId, cb){
    	Webuser.findById(peopleId, function(err, peopleInst){
    		if(err) return cb(err,null);
    		Webuser.app.models.AccessToken.destroyAll({ id: peopleId }, function(errT, success) {
    			if(errT) return cb(errT,null); 	
    			peopleInst.isDeactivate = !peopleInst.isDeactivate;
    			peopleInst.save(function(errP, updatedInst){
    				if(errP) return cb(errP,null);
    				cb(null, {data:updatedInst, msg: msg.activeDeactive});
    			});
    		});
    	});
    };

    Webuser.remoteMethod("activeDeactive",{
    	accepts : [
    		{arg: "peopleId", type: "string",required:true, http:{source:"form"}}
    	],
    	returns : {arg:"success", type: "object"}
    });


    Webuser.approval = function(peopleId, status, cb){
	    Webuser.findById(peopleId,function(error, peopleInst){
	      if(error) return cb(error,null);

	      peopleInst.adminApproval = status;

	      peopleInst.save(function(err, updateInst){
	        if(error) return cb(err,null);
	        cb(null,{data:updateInst, msg:msg.approval});
	        Webuser.app.models.Otp.accApprByAdmin({status:status,mobile:peopleInst.mobile},function(){});

	      });

	    });
    };

    Webuser.remoteMethod("approval",{
	    accepts : [
	      {arg: "peopleId", type: "string", http : {source : "form"}},
	      {arg: "status", type: "string", http : {source : "form"}}
	    ],
	    returns : {arg: "success", type: "object"}
    });


 	Webuser.resetPassRequest = function(mobile,cb){
	    Webuser.findOne({where:{mobile : mobile}}, function(err, peopleIns){
	      if(err)
	        cb(err, null);
	      if(!peopleIns)
	        return cb(new CustomError("No user found"), null);
	       peopleIns.passwordOtp = {
	              createdAt : new Date(),
	              expireAt  : new Date(),
	              otp       : gf.getLongOtp()
	       };
	       peopleIns.passwordOtp.expireAt.setMinutes(peopleIns.passwordOtp.expireAt.getMinutes() + 20);
	       peopleIns.save(function(err, peopleInstance){
	         if(err)
	          cb(err, null);
	         else{
	          Webuser.app.models.Otp.sendSMS({mobile : peopleIns.mobile, otp : peopleIns.passwordOtp.otp},function(){})
	          cb(null,{data:peopleInstance,msg:msg.resetPassRequest});
	        }
	       });
	       
	    });
  	};

 	Webuser.remoteMethod("resetPassRequest", {
	    accepts : [
	      {arg : "mobile", type : "string",required:true, http: {source : "form"}}
	    ],
	    returns : [
	      {arg : "success", type : "object"}
	    ]
  	});

  	Webuser.resetPassword = function(id, otp, newPassword,cb){
  		/*console.log("******************************");
  		console.log(id);
  		console.log(otp);
  		console.log(newPassword);
  		console.log("******************************");*/

	    Webuser.findById(id, function(err, peopleIns){
	      if(err){
	        cb(err, null);
	      }
	      if(!peopleIns){
	        return cb(new CustomError("No User Found"), null);
	      } 
	      var expiersAt = new Date(peopleIns.passwordOtp.expireAt);
	      var currentDate = new Date();
	      var userOtp = peopleIns.passwordOtp.otp;
	      if(expiersAt > currentDate){
	        
	        if(userOtp == otp){
	          peopleIns.updateAttributes({password : newPassword}, function(err,peopleInst){
	            
	            if(err)
	              cb(err, null);
	            else{            
	            cb(null,{data:peopleInst,msg:msg.resetPassword});
	            }
	          });
	        }else{
	          return cb(new CustomError("Otp not match"), null);
	        }
	      }else
	        return cb(new CustomError("Otp Expired"), null)
	    });
  	};

  	Webuser.remoteMethod("resetPassword", {
	    accepts : [
	      {arg : "id", type : "string", http: {source : "form"}},
	      {arg : "otp", type : "number", http: {source : "form"}},
	      {arg : "newPassword", type : "string", http: {source : "form"}}
	    ],
	    returns : [
	      {arg : "success", type : "object"}
	    ]
  	});


};
